var ZuckoApi;
jQuery(document).ready(function($){
    if($('.tabs')){
        $('.tabs').tabs({
            hide: {effect:'fade',duration:200},
            show: {effect:'fade',duration:200}
        });
    }
    if($.fn.placeholder){
        $("input[placeholder], textarea[placeholder]").placeholder();
    }
    if($('.rating')){
        $('.rating').raty({
            score: function() {
                return $(this).attr('data-score');
            }
        });
    }
    if($('.moment-date')){
        moment_init('.moment-date');
        if($('.moment-date.realtime')){   
            setInterval(function(){
                moment_init('.moment-date.realtime');
            },30000);
        }
    }
    /*
    var test = "<div><div data-bind='Hello {{name}}'></div><span data-class='{{name}}'>This is test</span></div>";
    alert($(test).cloneWithData({name: "mahfuz"}).html());
    */
    function moment_init(elm){
        $(elm).each(function(i,elm){
            var mom;
            var time = $(elm).attr('data-time');
            var format = $(elm).attr('data-format');
            if(time){
                mom = moment.unix(time);
            }else{
                if($(elm).attr('data-date')){
                    mom = moment($(elm).attr('data-date'))
                }else{
                    mom = moment();
                }
            }
            $(elm).html(mom.format(format));
        });
    }
    ZuckoApi = function($path, $data){
        var url = base_url+"/ajax/"+$path;
        this.call = function($requestType){
            var df = jQuery.Deferred();
            $.ajax({
                url: url,
                type: $requestType,
                data: $data,
                dataType: 'json',
                success: function(res, status, jqX){
                    if(res.state=="ok"){
                        df.resolve(res);
                    }else{
                        df.reject(res);
                    }
                },
                error: function(jqX, error_code, error_thrown){
                    console.log(jqX);
                    var res = {state: 'error', 'error_code': error_code, 'error_message': error_code};
                    if(jqX.responseJSON){
                        res.error_message = jqX.responseJSON.error.message;
                    }
                    df.reject(res);
                },
                complete: function(jqX, status_code){
                    if(df.state()=="pending"){
                        var res = {state:'error','error_code': 666, 'error_message': 'Unknown Error'};
                        df.reject(res);
                    }
                }
            });
return df.promise();
}
}
});
(function($){
    $.fn.render = function(data){
        var $this = $(this);
        parse($this);
        function parse(elm){
            elm = $(elm);
            //console.log(elm);
            var children = elm.children();
            //console.log(children);
            //console.log(1);
            if(children.length>0){
                children.each(function(i, child){
                    parse(child);
                });
            }
            var tpl = elm.attr('data-bind');
            if(tpl){
                elm.html(toText(tpl));
            }
            if(elm.attr('data-src')){
                elm.attr('src',toText(elm.attr('data-src')));
            }
            if(elm.attr('data-href')){
                elm.attr('href',toText(elm.attr('data-href')));
            }
            if(elm.attr('data-class')){
                elm.attr('class',toText(elm.attr('data-class')));
            }
        };
        function toText(tpl){
            Mustache.parse(tpl);
            return Mustache.render(tpl, data);
        };
    };
})(jQuery);
(function($){
    $.fn.cloneWithData = function(data){
        var $this = $(this);
        var clone = $this.clone(true);
        $(clone).render(data);
        console.log(clone);
        return clone;
    };
})(jQuery);
var User;
var Review;
var Vote;
var EventModel;
$(function(){
    User = Backbone.Model.extend({
        initialize: function(){

        },
        urlRoot: function(){
            return base_url + "/api/users";
        }
    });
    Review = Backbone.Model.extend({
        urlRoot: function(){
            return base_url +"/api/reviews";
        }
    });
    EventModel = Backbone.Model.extend({
        urlRoot: function(){
            return base_url+"/api/events";
        }
    });
    Vote = Backbone.Model.extend({
        urlRoot: function(){
            return base_url + "/api/reviews/"+this.get('target_id')+"/votes";
        }
    });
    current_user = new User(current_user);
});
var CounterView;
$(function(){
    CounterView = Backbone.View.extend({
        value: 0,
        initialize: function(elm){
            this.setElement(elm);
            this.value = this.$el.html();
            this.$el.addClass("counter");
            this.render();
        },
        render: function(){
            var tpl = _.template(this.template);
            this.$el.html(tpl(this));
        },
        template: '<span class="text"><%= value %></span><span class="caret"><s></s><i></i></span>'
    });
    $('.backbone-counter').each(function(){
        new CounterView($(this));
    });
});
var ReviewView;
var EventView;
var SliderView;
$(function(){
    SliderView = Backbone.View.extend({
        events: {

        },
        slider: null,
        initialize: function(elm){
            this.setElement(elm);
            
        },
        render: function(){
            //this.slider.reloadSlider();
            this.slider = this.$(".wrapper ul").bxSlider({
                auto: true,
                pause: 3000,
                autoHover: true,
                infiniteLoop: true,
                speed: 400,
                slideWidth: 150,
                slideSelector: 'li.slide',
                minSlides: 2,
                maxSlides: 4,
                moveSlides: 4,
                slideMargin: 4,
                easing: 'easeOutBounce',
                pager: false,
                controls: true,
                nextSelector: '.right-arrow',
                prevSelector: '.left-arrow',
                nextText: '<span style="display:inline-block;width:20px;height:20px;background:none;cursor:pointer"></span>',
                prevText: '<span style="display:inline-block;width:20px;height:20px;background:none;cursor:pointer"></span>'
            });
            this.$('.wrapper').magnificPopup({
                type: 'image',
                gallery: {
                    enabled: true
                },
                delegate: 'ul li img'
            });
        },
        pause: function(){
            console.log('dddd');
            this.slider.destroySlider();
        }
    });
EventView = Backbone.View.extend({
    events: {
        "click .vote-interested" : "voteInterested",
        "click .vote-interested.voted" : "unVoteInterested",
        "click .show_more_btn": "showEventDetails",
        "click .show_less_btn": "hideEventDetails"
    },
    eventModel: null,
    slider: null,
    interestedCounter: null,
    sliderView: null,
    initialize: function(elm){
        this.setElement(elm);
        this.eventModel = new EventModel(JSON.parse(this.$el.attr("data-event")));
        this.listenTo(this.eventModel, "change", this.render);
        this.interestedCounter = new CounterView(this.$(".stats-interested"));
        if(this.$('.slider').length){
            this.sliderView = new SliderView(this.$('.slider'));
            if(this.$el.attr("data-fullpage")!=undefined){
                this.sliderView.render();
            }
        }
        this.render();
    },
    render: function(){
        if(this.eventModel.get('voted_interested')===true){
            this.$('.vote-interested').addClass('voted');
        }else{
            this.$('.vote-interested').removeClass('voted');
        }
        console.log(this);
        this.interestedCounter.value = this.eventModel.get('interested_count');
        this.interestedCounter.render();
    },
    showEventDetails: function(e){
        e.preventDefault();
        var $this = this;
        $this.$('.small-desc').fadeOut('200', function() {
            $this.$('.event_info .event-details.full').fadeIn('400', function() {
                if($this.sliderView!==null){
                    $this.sliderView.render();
                }
            });
        });
    },
    hideEventDetails: function(e){
        e.preventDefault();
        var $this = this;
        $this.$('.event-details.full').fadeOut('200', function() {
            $this.$('.event_info .small-desc').fadeIn('400', function() {
                if($this.sliderView!=null){
                    $this.sliderView.pause();
                }
            });
        });
    },
    voteInterested: function(){
        if(current_user.get("id")==undefined){
            return;
        }
        if(this.eventModel.get('voted_interested')===true){
            return;
        }
        this.eventModel.set('voted_interested',true);
        this.eventModel.set('interested_count',parseInt(this.eventModel.get('interested_count'))+1);
        $.ajax({
            url: base_url+'/ajax/event/interested',
            type: 'post',
            data: {'event_id':this.eventModel.get('id')}
        });
    },
    unVoteInterested: function(){
        if(this.eventModel.get('voted_interested')===false){
            return;
        }
        this.eventModel.set('voted_interested',false);
        this.eventModel.set('interested_count',parseInt(this.eventModel.get('interested_count'))-1);
        $.ajax({
            url: base_url+'/ajax/event/interested/delete',
            type: 'post',
            data: {'event_id':this.eventModel.get('id')}
        });
    }
});
$(".backbone-event-view").each(function(){
    new EventView($(this));
});
ReviewView = Backbone.View.extend({
    events:{
        "click .vote-button.useful" : "voteUseful",
        "click .vote-button.funny" : "voteFunny",
        "click a.like-btn.likeable" : "voteLike",
        "click a.like-btn.unlikeable" : "unVoteLike"
    },
    likesCounter: null,
    initialize: function(elm){
        this.setElement(elm);
        this.review = new Review(JSON.parse(this.$el.attr('data-review')));
        console.log(this.review.get('id'));
        this.listenTo(this.review, "change", this.render);
        this.likesCounter = new CounterView(this.$('.stats-likes'));
        this.render();
    },
    render: function(){
        console.log("redering");
        if(this.review.get('voted_useful')===true){
            this.$('.vote-button.useful').addClass('disabled');
        }else{
            this.$('.vote-button.useful').removeClass('disabled');
        }
        if(this.review.get('voted_funny')===true){
            this.$('.vote-button.funny').addClass('disabled');
        }else{
            this.$('.vote-button.funny').removeClass('disabled');
        }
        if(this.review.get('voted_likes')===true){
            this.$('.like-btn').removeClass('likeable').addClass('active unlikeable');
        }else{
            this.$('.like-btn').removeClass('active unlikeable').addClass('likeable');
        }
        var useful_count = parseInt(this.review.get('useful_count'));
        var funny_count = parseInt(this.review.get('funny_count'));
        var total = useful_count + funny_count;
        if(total>0){
            this.$('.stats-useful').html(Math.floor((useful_count/total)*100)+"%");
            this.$('.stats-funny').html(Math.floor((funny_count/total)*100)+"%");
        }
        this.likesCounter.value = this.review.get('likes_count');
        this.likesCounter.render();
    },
    unVoteLike: function(){
        console.log("unliking");
        if(this.review.get('voted_likes')===false){
            return;
        }
        this.review.set('voted_likes',false);
        this.review.set('likes_count',parseInt(this.review.get('likes_count'))-1);
        $.ajax({
            url: base_url+'/api/reviews/'+this.review.get('id')+'/votes/delete/'+current_user.get('id')+'/likes',
            type: 'post'
        });
    },
    voteLike: function(){
        console.log("HEE");
        if(this.review.get('voted_likes')===true){
            return;
        }
            //this.review.set('voted_likes',true);
            //this.review.set('likes_count',parseInt(this.review.get('likes_count')+1));
            var vote = new Vote({
                "type": "likes",
                "target_id":this.review.get('id'),
                "user_id": current_user.get('id'),
            });
            vote.save(null, {wait: true});
            vote.once("change", function(){
                this.review.fetch();
            }, this);
        },
        voteUseful: function(){
            if(this.review.get('voted_useful')===true){
                return;
            }
            this.review.set('voted_useful',true);
            var vote = new Vote({
                "type": "useful",
                "target_id":this.review.get('id'),
                "user_id": current_user.get('id'),
            });
            vote.save(null, {wait: true});
            vote.once("change", function(){
                this.review.fetch();
            }, this);
        },
        voteFunny: function(){
            if(this.review.get('voted_funny')===true){
                return;
            }
            this.review.set('voted_funny', true);
            var vote = new Vote({
                "type": "funny",
                "target_id":this.review.get('id'),
                "user_id": current_user.get('id'),
            });
            vote.save(null, {wait: true});
            vote.once("change", function(){
                this.review.fetch({error: function(d,e){console.log(e)}});
            }, this);
        },
        addComment: function(){

        },
    });
$('.backbone-review-view').each(function(){
    new ReviewView($(this));
});
var StatusUpdaterView = Backbone.View.extend({
    el : $('.mood-status'),
    events :{
        "click .edit_icon" : "showEdit",
        "click .status-text" : "showEdit",
        "click .update-status-submit" : "submitEdit",
        "keydown .update-status-input" : "preventNewLine",
        "focusout .update-status-input" : "submitEdit"
    },
    enabled: function(){
        if(this.$el.attr('data-enabled')=="yes")
            return true;
        else
            return false;
    },
    initialize: function(user){
        if(!this.enabled()){
            this.$('.edit_icon').hide();
        }
        this.user = user;
        this.listenTo(this.user, "change:about", this.render);
        this.listenTo(this.user, "change:about", this.saveData);
    },
    showEdit: function(){
        if(!this.enabled()){
            return;
        }
        this.$('.status-text').hide();
        this.$('.update-status-area').show();
        this.$('.update-status-input').val(this.user.get("about"));
        this.$('.update-status-input').focus();
    },
    submitEdit: function(){
        this.$('.status-text').show();
        this.$('.update-status-area').hide();
        if(this.$('.update-status-input').val()!=""){
            this.user.set("about",this.$('.update-status-input').val());
        }
    },
    saveData: function(){
        this.user.save();
    },
    render: function(){
        if(this.user.has("about")&&this.user.get("about")!=""){
            this.$('.status-text medium').html('"'+this.user.get("about")+'"');
            this.$('.update-status-input').val(this.user.get("about"));
        }
    },
    preventNewLine: function(e){
        if(e.keyCode==13){
            e.preventDefault();
            this.submitEdit();
        }
    }
});
new StatusUpdaterView(current_user);
});

