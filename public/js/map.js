(function($){

	function initialize() {
		var pos = $('#map-canvas').attr('data');
		var lat = -25.363882;
		var lng = 131.044922;
		if(pos!=null&&pos!=""){
			pos = pos.split(",");
			lat = pos[0];
			lng = pos[1];
		}
		var mapOptions = {
			zoom: 10,
			center: new google.maps.LatLng(lat, lng),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};

		map = new google.maps.Map(document.getElementById('map-canvas'),
			mapOptions);
		geocoder = new google.maps.Geocoder();
		user_marker = new google.maps.Marker({
			position: map.getCenter(),
			map: map,
			draggable: true,
			title: 'You Are Here'
		});
		google.maps.event.addListener(user_marker, 'position_changed', function(){
			var pos = user_marker.getPosition();
			$('input[name="latitude"]').val(pos.lat());
			$('input[name="longitude"]').val(pos.lng());
		});
		google.maps.event.addListener(map, 'bounds_changed', function(){

		});
		google.maps.event.addListener(map, 'zoom_changed', function(){

		});
		$('input[name="address"], input[name="city"], input[name="zip_code"],select[name="country_id"],select[name="state_id"]').change(function(){
			var address = $('input[name="address"]').val();
			var city = $('input[name="city"]').val();
			var zip = $('input[name="zip_code"]').val();
			var state = $('select[name="state_id"]').find(':selected').text();
			var country = $('select[name="country_id"]').find(':selected').text();
			convertAddress(address, city, zip, state, country);
		});
	}
	function convertAddress(address, city, state, zip, country){
		address = address+", "+city+", "+state+", "+zip+", "+country;
		console.log(address);
		geocoder.geocode( { 'address': address}, receiveConvert);
		function receiveConvert(res, status){
			console.log(status);
			if(status=="ZERO_RESULTS"){
				return convertAddress('',city,'','','');
			}
			console.log(res[0].geometry.bounds.toUrlValue());
			var pos2 = new google.maps.LatLng(res[0].geometry.location.mb, res[0].geometry.location.nb);
			map.fitBounds(res[0].geometry.bounds);
			user_marker.setPosition(res[0].geometry.location);
		}
	}
	var autoSearch = false;
	var map;
	var user_marker;
	var business_marker = new Array();
	var business_infowindow;
	var user_icon = base_url+'/img/User-icon.png';
	var business_icon = base_url+'/img/business-icon.png';
	var business_data = new Array();
	var geocoder;
	google.maps.event.addDomListener(window, 'load', initialize);
})(jQuery);