'use strict';
module.exports = function(grunt){
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		watch: {
			js: {
				files: ['public/js/*.js'],
				tasks: ['uglify']
			},
			css: {
				files: ['public/css/*.css','public/admin/css/*.css'],
				tasks: ['cssmin']
			}
		},
		uglify: {
			all:{
				files:{
					'public/js/build/jquery.min.js': [
						'bower_components/jquery/dist/jquery.js'
					],
					'public/js/build/common.min.js': [
						'bower_components/jquery-ui/ui/core.js',
						'bower_components/jquery-ui/ui/effect.js',
						'bower_components/jquery-ui/ui/effect-fade.js',
						'bower_components/jquery-ui/ui/widget.js',
						'bower_components/jquery-ui/ui/tabs.js',
						'bower_components/underscore/underscore.js',
						'bower_components/backbone/backbone.js',
						'public/js/jquery.raty.min.js',
						'public/js/moment.js',
						'public/js/mustache.js'
					],
					'public/js/build/all.min.js': [
						'public/js/build/common.min.js',
						'public/js/main.js',
						'public/js/jquery.bxslider.js',
						'public/js/jquery.easing.1.3.js',
						'public/js/jquery.magnific-popup.js',
						'public/js/jquery.mCustomScrollbar.concat.min.js',
						'bower_components/jquery-ui/ui/datepicker.js'
					]
				},
				options: {
					sourceMap: true
				}
			}
		},
		cssmin: {
			combine:{
				files:{
					'public/css/all.min.css': [
						'public/css/normalize.css',
						'public/css/main.css',
						'public/css/style2.css',
						'public/css/jquery.bxslider.css',
						'public/css/jquery.ui.datepicker.css',
						'public/css/jquery.mCustomScrollbar.css',
						'public/css/magnific-popup.css'
					],
					'public/admin/css/all.min.css':[
						'public/admin/css/bootstrap.min.css',
						'public/admin/css/bootstrap-responsive.min.css',
						'public/admin/css/font-awesome.css',
						'public/admin/css/style.css',
						'public/admin/css/pages/signin.css',
						'public/admin/css/pages/dashboard.css'
					]
				}
			}
		}
	});
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.registerTask('default',['uglify','cssmin','watch']);
};