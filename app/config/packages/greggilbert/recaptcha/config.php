<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| API Keys
	|--------------------------------------------------------------------------
	|
	| Set the public and private API keys as provided by reCAPTCHA.
	|
	*/
	'public_key'	=> '6Le1z_kSAAAAAKrCwpog5PeVJWntwSTOQEW6E7bU',
	'private_key'	=> '6Le1z_kSAAAAAFOmONSpBXQsryGjUM_ZofPekYBe',
	
	/*
	|--------------------------------------------------------------------------
	| Template
	|--------------------------------------------------------------------------
	|
	| Set a template to use if you don't want to use the standard one.
	|
	*/
	'template'		=> '',
	'options' => array(
		'theme' => 'white',
		'privacy' => 'false'
		)
	
);