<?php 
return [
	"Add Events" => "Add Events",
	"add_form1"	=> "Hi Everybody",
	"add_form2" => "On this page, you will be able to add an event, allowing all people around to know exactly whats going on in Tahiti and all the islands of the Fenua.",
	"add_form3" => "Fill in the forms below, add a picture or not and let's \"add an event\"!",
	"Create Event" => "Create Event",
	"title" => "title",
	"Location" => "Location",
	"Video Url" => "Video Url",
	"All fields required" => "All fields required",
	"Add Event" => "Add Event",
	"Upload Pictures" => "Upload Pictures",
	"When" => "When",
	"City" => "City",
	"Website Link" => "Website Link",
	"Browse" => "Browse",
	"Description" => "Description",
];