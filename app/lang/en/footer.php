<?php 
return [
	"About" => "About",
	"Terms of Service" => "Terms of Service",
	"Privacy Policy" => "Privacy Policy",
	"Help" => "Help",
	"Advertise" => "Advertise",
	"Contact Us" => "Contact Us",
	"More" => "More",
	"Careers" => "Careers",
	"Your Name" => "Your Name",
	"Email Address" => "Email Address",
	"Follow" => "Follow",
	"Us On" => "Us On",
];