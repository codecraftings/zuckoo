<?php
return [
    'home'           => 'Home',
    'about_us'       => 'About Us',
    'write_review'   => 'Write a Review',
    'find_friends'   => 'Find Friends',
    'messages'       => 'Messages',
    'blog'           => 'Blog',
    'events'         => 'events',
    'contact_us'     => 'Contact Us',
    'login'          => 'LOG IN >>',
    'signup'         => 'SIGN UP >>',
    "stay_connected" => "Stay Connected",
    "more_cities"    => "More Cities",

    "search_what" => "What? or Who?",
    "search_where" => "Where?",
    "search_what_caption" => "ex. Doctor, Plumbers, Builders",
    "search_where_caption"=> "ex. Papeete, Taravao",
    "search_btn" => "Search",
	"Messages" => "Messages",
];