<?php
return [
    'home'         => 'Accueil',
    'about_us'     => "L'APF",
    'write_review' => 'Ecrire un commentaire',
    'find_friends' => 'Trouver des amis',
    'messages'     => 'Messages',
    'blog'        => 'Blog',
    'events'       => 'Evenements',
    'contact_us'   => 'Contactez nous',

    'login'=> 'Connexion',
    'signup' => 'Inscription',
    "stay_connected" => "Restez connecté !!",
    "more_cities"    => "Plus de villes",

    "search_what" => "Quoi? ou Qui?",
    "search_where" => "Ou?",
    "search_what_caption" => "ex. Entreprise ou personne",
    "search_where_caption"=> "ex. Papeete, Taravao",
    "search_btn" => "Chercher",
	"Messages" => "Messagerie",
];