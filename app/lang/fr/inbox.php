<?php 
return [
	"Search Messages" => "Chercher mot",
	"Refresh" => "Rafraichir",
	"Select All" => "Selectionner tous",
	"Delete" => "Effacer",
	"Delete All" => "Effacer tous",
];