<div class="slider">
	@if(count($photos)>3)
	<a href="javascript:void()" class="left-arrow"></a>
	<a href="javascript:void()" class="right-arrow"></a>
	@endif
	<div class="wrapper">
		<ul>
			@foreach($photos as $key=>$photo)
			<li class="slide"><img data-mfp-src="{{$photo->url}}" src="{{$photo->thumb_url}}" alt="{{$photo->caption}}" /></li>
			@endforeach
			@if(count($photos)<1)
			<li style="display:block;width:600px;text-align:center;"><img src="{{url('img/no_image.jpg')}}" alt="No Photo" /></li>
			@endif
		</ul>
	</div>
</div>