<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="Annuaire Polynesian">
<link rel="shortcut icon" href="img/favicon.png">
<title>
@section('window-title')
Zuckoo By Annuaire Polynesian
@show
</title>
<!--
<link rel="stylesheet" href="{{url('css/normalize.css')}}">
<link rel="stylesheet" href="{{url('css/main.css')}}">
<link rel="stylesheet" href="{{url('css/style2.css')}}">
<link rel="stylesheet" href="css/jquery.ui.tabs.css">-->
{{HTML::style('css/all.min.css')}}
<script type="text/javascript">
	var base_url = "{{url('/')}}";
	@if(isset($current_user))
		var current_user = {{json_encode($current_user)}};
		@else
		var current_user = "null";
	@endif
</script>
<!--
<script type="text/javascript" src="{{url('js/jquery.js')}}"></script>
<script type="text/javascript" src="{{url('js/jquery.ui.core.js')}}"></script>
<script type="text/javascript" src="{{url('js/jquery.ui.widget.min.js')}}"></script>
<script type="text/javascript" src="{{url('js/jquery.ui.tabs.min.js')}}"></script>

<script type="text/javascript" src="{{url('js/jquery.raty.min.js')}}"></script>
<script type="text/javascript" src="{{url('js/moment.js')}}"></script>
<script type="text/javascript" src="{{url('js/main.js')}}"></script>
<script type="text/javascript" src="{{url('js/build/main.min.js')}}"></script>
-->
{{HTML::script('js/build/jquery.min.js')}}
@section('extra-scripts')
@show
<!--[if IE]>
<link rel="stylesheet" href="css/ie.css">
<link rel="stylesheet" href="css/jquery.placeholder.min.css">
<script type="text/javascript" src="js/jquery.placeholder.min.js"></script>
<![endif]-->