@extends('emails.layout')
@section('email-body')
<p>Hi, {{$user->present()->name()}}</p>
<p>
	You have requested to reset your password on <a href="{{url('/')}}">Zuckoo</a>
</p>
<p>
	To reset your password, complete this form: <a href="{{$verify_url = url('password/reset/confirm?token='.$token.'&email='.$user->getReminderEmail())}}">{{$verify_url}}</a>.<br/>
	This link will expire in {{ Config::get('auth.reminder.expire', 60) }} minutes.
</p>
<p>
	If you unaware about this request, just ignore it. No changes will be made to your password unless you click that link
</p><br>
<p>
	Thanks and Regards
</p>
<p>
	<a href="{{url('/')}}">Zuckoo Support Team</a>
</p>
@stop
