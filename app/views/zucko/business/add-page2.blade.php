@extends('layouts.main')
@section('main-content')
<h2 class="title-stripped">
    <span class="text">{{trans("business.Add Business")}}</span>
</h2>
<p>
    {{trans("business.form3")}}
</p><br>
<div class="form-container">
    <div class="inside">
        <h3>
            {{trans("business.Would you like to tell us more about your business?")}}
        </h3>
        <p>
            <small>
                Your business is successfully submited. This is just some extra information that will help our users find your business.
            </small>
        </p><br>
        @if(Session::has('info'))
        <div class="msg">{{Session::get('info')}}</div>
        @endif
        @if(Session::has('success'))
        <div class="msg success">{{Session::get('success')}}</div>
        @endif
        @if(Session::has('error'))
        <div class="msg error">{{Session::get('error')}}</div>
        @endif
        <form enctype="multipart/form-data" method="post" action="{{Request::fullUrl()}}">
          <div style="position:relative;" class="form-field full">
             <span class="label">{{trans("business.Opening hours")}}</span>
             <div class="cf"></div>
             @foreach($business->meta->opening_hours as $key=>$hour)
             <div style="margin:5px 0px 0px 0px;" class="fl hour-selector">
                <select name="day1[]" class="input input-medium">
                  <option{{$hour->day1=="sun"?" selected":""}} value="sun">{{trans("app.Sunday")}}</option>
                  <option{{$hour->day1=="mon"?" selected":""}} value="mon">{{trans("app.Monday")}}</option>
                  <option{{$hour->day1=="tue"?" selected":""}} value="tue">{{trans("app.Tuesday")}}</option>
                  <option{{$hour->day1=="wed"?" selected":""}} value="wed">{{trans("app.Wednessday")}}</option>
                  <option{{$hour->day1=="thu"?" selected":""}} value="thu">{{trans("app.Thursday")}}</option>
                  <option{{$hour->day1=="fri"?" selected":""}} value="fri">{{trans("app.Friday")}}</option>
                  <option{{$hour->day1=="sat"?" selected":""}} value="sat">{{trans("app.Saturday")}}</option>
              </select>
              To 
              <select name="day2[]" class="input input-medium">
                  <option{{$hour->day2=="sun"?" selected":""}} value="sun">{{trans("app.Sunday")}}</option>
                  <option{{$hour->day2=="mon"?" selected":""}} value="mon">{{trans("app.Monday")}}</option>
                  <option{{$hour->day2=="tue"?" selected":""}} value="tue">{{trans("app.Tuesday")}}</option>
                  <option{{$hour->day2=="wed"?" selected":""}} value="wed">{{trans("app.Wednessday")}}</option>
                  <option{{$hour->day2=="thu"?" selected":""}} value="thu">{{trans("app.Thursday")}}</option>
                  <option{{$hour->day2=="fri"?" selected":""}} value="fri">{{trans("app.Friday")}}</option>
                  <option{{$hour->day2=="sat"?" selected":""}} value="sat">{{trans("app.Saturday")}}</option>
              </select>

              <input type="text" name="time1[]" value="{{$hour->time1}}" class="input input-xxxsmall time-input" placeholder="00" /> AM To &nbsp;&nbsp;<input name="time2[]" type="text" placeholder="00" value="{{$hour->time2}}" class="input input-xxxsmall time-input" /> PM
              <a href="javascript:void()" class="minus-btn {{$key==0?'hidden':''}}"><small>Remove</small></a>
              <br>
          </div>
          @endforeach
          <a href="javascript:void()" class="add-hour-btn"><i class="icon-plus"></i></a>
          <div class="cf"></div>
      </div>
      <div class="form-field half">
        <span class="label">{{trans("business.Good for groups")}}?</span>
        <select class="input input-medium" name="good_for_groups">
            <option value="1">{{trans("app.Yes")}}</option>
            <option value="0">{{trans("app.No")}}</option>
        </select>
    </div>
    <div class="form-field half">
        <span class="label">{{trans("business.Water services")}}?</span> <select name="water_service" class="input input-medium"><option value="1">{{trans("app.Yes")}}</option><option value="0">{{trans("app.No")}}</option></select>
    </div>
    <div class="form-field half">
        <span class="label">{{trans("business.Credit cards accepted")}}?</span> <select name="credit_card_accepted" class="input input-medium"><option value="1">{{trans("app.Yes")}}</option><option value="0">{{trans("app.No")}}</option></select>
    </div>
    <div class="form-field half">
        <span class="label">{{trans("business.Parking Valet")}}?</span> <select name="parking_valet" class="input input-medium"><option value="1">{{trans("app.Yes")}}</option><option value="0">{{trans("app.No")}}</option></select>
    </div>
    <div class="form-field half">
        <span class="label">{{trans("business.Wheelchair accessible")}}?</span> <select name="wheelchair_accessible" class="input input-medium"><option value="1">{{trans("app.Yes")}}</option><option value="0">{{trans("app.No")}}</option></select>
    </div>
    <div class="form-field half">
        <input type="hidden" name="price_currency" value="xpf">
        <span class="label">{{trans("business.Price Range")}}(In XPF)</span> <input maxlength="5" type="text" placeholder="000" class="input input-xxsmall" name="price_low"/> To <input placeholder="000" maxlength="5" type="text" name="price_high" class="input input-xxsmall"/>
    </div>
    <div class="form-field half">
        <span class="label">{{trans("business.Outdoor seating")}}?</span> <select name="outdoor_seating" class="input input-medium"><option value="1">{{trans("app.Yes")}}</option><option value="0">{{trans("app.No")}}</option></select>
    </div>
    <div class="form-field half">
        <span class="label">{{trans("business.Good for kids")}}?</span> <select name="good_for_kids"
                                                                                           class="input input-medium">
            <option value="1">{{trans("app.Yes")}}</option>
            <option value="0">{{trans("app.No")}}</option>
        </select>
    </div>
    <div class="form-field half">
        <span class="label">{{trans("business.Food Type")}} </span> <select name="food_type" class="input input-medium">
        <option value="none">Not Aplicable</option>
        <option value="tahitian">Tahitian Food</option>
        <option value="chinese">Chinese Food</option>
        <option value="french">French Food</option>
        <option value="bbq">BBQ Food</option>
        <option value="sea">Sea Food</option>
        <option value="meat">Meat Food</option>
        </select>
    </div>
    <div class="form-field half">
        <span class="label">{{trans("business.Good for")}} </span> <select name="good_for" class="input input-medium">
        <option value="none">Not Aplicable</option>
        <option value="breakfast">Breakfast</option>
        <option value="lunch">Lunch</option>
        <option value="dinner">Dinner</option>
        </select>
    </div>
    <div class="form-field half">
        <span class="label">{{trans("business.Take reservations")}}?</span> <select name="take_reservation" class="input input-medium"><option value="1">{{trans("app.Yes")}}</option><option value="0">{{trans("app.No")}}</option></select>
    </div>
    <div class="form-field half">
        <span class="label">{{trans("business.Alcohol")}}?</span> <select name="alcohol" class="input input-medium"><option value="1">{{trans("app.Yes")}}</option><option value="0">{{trans("app.No")}}</option></select>
    </div>
    <div class="form-field half">
        <span class="label">{{trans("business.Delivery")}}?</span> <select name="delivery" class="input input-medium"><option value="1">{{trans("app.Yes")}}</option><option value="0">{{trans("app.No")}}</option></select>
    </div>
    <div class="form-field half">
        <span class="label">{{trans("business.To go")}}?</span> <select name="to_go" class="input input-medium"><option value="1">{{trans("app.Yes")}}</option><option value="0">{{trans("app.No")}}</option></select>
    </div>
    <div class="form-field half">
        <span class="label">{{trans("business.Dogs allowed")}}?</span> <select name="dogs_allowed" class="input input-medium"><option value="1">{{trans("app.Yes")}}</option><option value="0">{{trans("app.No")}}</option></select>
    </div>
    <div class="cf"></div>
    <div class="form-field full">
        <span class="label">Pictures:</span><br>
        <div>
            <input multiple type="file" class="hidden" id="photosinput" name="pictures[]"/>
            <div class="img-placeholder fl"><i class="icon-plus"></i></div>
            <div class="img-placeholder fl"><i class="icon-plus"></i></div>
            <div class="img-placeholder fl"><i class="icon-plus"></i></div>
            <div class="cf"></div>
        </div>
    </div>
    <div style="margin-bottom:10px;" class="cf"></div>
    <div class="notice">*All Fields are required</div>
    <input type="submit" name="submit" class="red-button fr" value="{{trans("business.Save Business")}}" />
    <div class="cf"></div>
</form>
</div>
<script type="text/javascript">
    (function($){
        $(document).ready(function(){
            $('.hour-selector .minus-btn').click(function(e) {
              $(e.target).parents('.hour-selector').remove();
            });

            $('.add-hour-btn').click(function(event) {
              var b = $('.hour-selector').last().clone();
              b.find('.minus-btn').show();
              $('.hour-selector').parent().append(b);
              $('.hour-selector .minus-btn').click(function(e) {
                $(e.target).parents('.hour-selector').remove();
              });
            });
            $('.img-placeholder').click(function(){
                $('#photosinput').click();
            });
            $('#photosinput').change(function(e){
                var p = $(this).parent();
                var c = p.find('.img-placeholder').last().clone();
                var files = e.target.files;
                var len = files.length;
                p.find('.img-placeholder').remove();
                c.click(function(){
                    $('#photosinput').click();
                });
                p.prepend(c);
                if(len>10){
                  alert('Sorry you cannot select more than 10 files at once!');
                  $(e.target).val('');
                  return;
                }
                for(var i=0;i<len;i++){
                    var reader = new FileReader();
                    reader.onload = function(e){
                        var b = c.clone();
                        b.html('<img src="'+e.target.result+'">');
                        p.prepend(b);
                    }
                    reader.readAsDataURL(files[i]);
                }
            });
        });
    })(jQuery);
</script>
</div>
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn', 'mobile-app')))
@stop