@extends('layouts.business-profile')
@section('window-title')
{{$business->name}} | @parent
@stop
@section('main-content')
<div class="company_content_top">
	<div style="width:580px;max-width:600px;" class="company_info fl">
		<h2 title="{{$business->name}}"><a href="{{$business->profile_url}}">{{$business->present()->title(15)}}</a><small> >> Reviews</small></h2>
	</div><!--END hote-->
	<div class="cf"></div>
	<div class="">
		<h3 class="blue-header">Write Your Review</h3>
		@if(isset($current_user))
		@include('zucko.review.components.add-review', array('business'=>$business,'myreview'=>$myreview));
		@else
		Please login or register to review this business
		@endif
	</div>
	<h3 class="blue-header">Reviews By Others</h3>
	<div class="reviews-container">
	@include('zucko.review.components.list-reviews',array('reviews'=>$business->reviews))
	</div>
</div>
<div style="margin-bottom:20px;" class="cf"></div>

	@stop
	@section('sidebar')
	<div class="profile-widget video">
		<img src="{{url('img/no_video.jpg')}}" alt="" />
	</div><!--END video-->
	<div class="profile-widget compliments">
		<h2 class="heading">Releted Businesses<i class="grey_arrow fr"></i></h2>
		<div class="list-item">
			<div class="img-container img-container-curve left">
				<img src="{{url('img/friends.jpg')}}" alt="sample" />
			</div>
			<div class="text_box left">
				<h5><a>Jack Glove</a></h5>
				<p>Oscar wild once said,</p>
			</div>
			<div class="cf"></div>
		</div>
		<div class="list-item">
			<div class="img-container img-container-curve left">
				<img src="{{url('img/friends.jpg')}}" alt="sample" />
			</div>
			<div class="text_box left">
				<h5><a>Jack Glove</a></h5>
				<p>Oscar wild once said,</p>
			</div>
			<div class="cf"></div>
		</div>
		<div class="list-item">
			<div class="img-container img-container-curve left">
				<img src="{{url('img/friends.jpg')}}" alt="sample" />
			</div>
			<div class="text_box left">
				<h5><a>Jack Glove</a></h5>
				<p>Oscar wild once said,</p>
			</div>
			<div class="cf"></div>
		</div>
		<div class="list-item">
			<div class="img-container img-container-curve left">
				<img src="{{url('img/friends.jpg')}}" alt="sample" />
			</div>
			<div class="text_box left">
				<h5><a>Jack Glove</a></h5>
				<p>Oscar wild once said,</p>
			</div>
			<div class="cf"></div>
		</div>
	</div><!--END spcial offers1-->
	<div class="profile-widget">
		<h2 class="heading">Related Special Offers<i class="grey_arrow fr"></i></h2>
		<div class="list-item">
			<div class="img-container img-container-curve left">
				<img src="{{url('img/sample6.png')}}" alt="sample" />
			</div>
			<div class="text_box left">
				<h5><a>Place to eat and drink...</a></h5>
				<p>Here’s where I like to eat and drink in the high...</p>
			</div>
			<div class="cf"></div>
		</div>
		<div class="list-item">
			<div class="img-container img-container-curve left">
				<img src="{{url('img/sample6.png')}}" alt="sample" />
			</div>
			<div class="text_box left">
				<h5><a>Place to eat and drink...</a></h5>
				<p>Here’s where I like to eat and drink in the high...</p>
			</div>
			<div class="cf"></div>
		</div>
		<div class="list-item">
			<div class="img-container img-container-curve left">
				<img src="{{url('img/sample6.png')}}" alt="sample" />
			</div>
			<div class="text_box left">
				<h5><a>Place to eat and drink...</a></h5>
				<p>Here’s where I like to eat and drink in the high...</p>
			</div>
			<div class="cf"></div>
		</div>
	</div>
	@stop