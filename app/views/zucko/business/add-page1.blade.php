        @extends('layouts.main')
        @section('footer-scripts')
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5y_HSH5nG4nXBclv13VrRZ5b81S5cRZc&amp;sensor=false"></script>
        <script type="text/javascript" src="{{url('js/map.js')}}"></script>
        @stop
        @section('main-content')
        <h2 class="title-stripped"><span class="text">{{trans("business.Add Business")}}</span></h2>
        <p>
            {{trans("business.form1")}}
            {{trans("business.form2")}}
        </p>
        @if(Session::has('info'))
        <div class="msg">{{Session::get('info')}}</div>
        @endif
        @if(Session::has('success'))
        <div class="msg success">{{Session::get('success')}}</div>
        @endif
        @if(Session::has('error'))
        <div class="msg error">{{Session::get('error')}}</div>
        @endif
        <br>
        <form id="lddbd_add_business_form" enctype="multipart/form-data" autocomplete="off" method="post">      
          <div class="form-container">
            <div class="inside">
              <h3>{{trans("business.Create Business")}}</h3>
              <div class="blue-header">
                <input type="radio" id="littypeoonbe" name="listing_type" value="place" class="radio" {{Session::get('listing_type')=='place'?'checked':''}} > 
                <label for="littypeoonbe" class="text1">{{trans("app.Local Business or Place")}}</label>
                <input id="littypeoonbesdf" type="radio" name="listing_type" value="product" class="radio" {{Session::get('listing_type')=='product'?'checked':''}}>
                <label for="littypeoonbesdf" class="text1">{{trans("app.Product or Brand")}}</label>
                <input id="littypeoonbesdfwr" type="radio" name="listing_type" value="company" class="radio" {{Session::get('listing_type')=='company'?'checked':''}}> 
                <label for="littypeoonbesdfwr" class="text1">{{trans("app.Company, Institution or Organization")}}</label>
              </div>
              <div class="cf"></div>
              <div class="form-field half zmargi">
                <input type="text" name="name" placeholder="{{trans("app.Company, Institution or Organization")}}" value="{{Session::get('name')}}" class="input input-full required" maxlength="20" />
              </div>
              <div class="form-field half zmargi" >
                <select  id="country_name" name="country" class="input input-full">
                  <option value="">{{trans("app.Select Country")}}</option>
                  @foreach($countries as $country)
                  <option value="{{$country->name}}" {{Session::get('country')==$country->name?"selected":""}}>{{$country->name}}</option>
                  @endforeach
                </select>
              </div>
              <div class="cf"></div>	
              <div class="form-field half zmargi">
                <input value="{{Session::get('address')}}" type="text" name="address" placeholder="{{trans("business.Business Address")}}" class="input input-full" />
              </div>      
              <div class="form-field half zmargi">
                <input value="{{Session::get('city')}}" type="text" name="city" placeholder="{{trans("business.City")}}" class="input input-full" maxlength="20" />
              </div>    
              <div class="cf"></div> 
              <div class="form-field half zmargi">
                <select class="input input-full" id="island_name" name="state" >
                 <option value="">{{trans("business.Select your island")}}</option>
                 @foreach($states as $state)
                 <option value="{{$state->name}}" {{Session::get('state')==$state->name?"selected":""}}>{{$state->name}}</option>
                 @endforeach
               </select>
             </div>
             <div class="form-field half zmargi">
              <input value="{{Session::get('zip_code')}}" type="text" name="zip_code" id="zip_code" placeholder="Boite postale or Zip Code " class="input input-full required" maxlength="20" />
            </div>  
            <div class="cf"></div> 
            <div class="form-field half zmargi">
              <input value="{{Session::get('email')}}" type="text" name="email" placeholder="Email Address" class="input input-full email" />
            </div>      
            <div class="form-field half zmargi">
              <input value="{{Session::get('fax')}}" type="text" name="fax" placeholder="Fax" class="input input-full" maxlength="30"/>
            </div>    
            <div class="cf"></div> 
            <div class="form-field half zmargi">
              <select class="input input-full" name="company_type">
               <option value="" {{Session::get('company_type')==""?"selected":""}}>{{trans("business.Type of company")}}</option>
               <option value="SARL" {{Session::get('company_type')=="SARL"?"selected":""}}>SARL</option>
               <option value="EURL" {{Session::get('company_type')=="EURL"?"selected":""}}>EURL</option>
               <option value="SELARL" {{Session::get('company_type')=="SELARL"?"selected":""}}>SELARL</option>
               <option value="SA" {{Session::get('company_type')=="SA"?"selected":""}}>SA</option>
               <option value="SAS" {{Session::get('company_type')=="SAS"?"selected":""}}>SAS</option>
               <option value="SASU" {{Session::get('company_type')=="SASU"?"selected":""}}>SASU</option>
               <option value="SNU" {{Session::get('company_type')=="SNU"?"selected":""}}>SNU</option>
               <option value="SCP" {{Session::get('company_type')=="SCP"?"selected":""}}>SCP</option>

             </select>
           </div>      
           <div class="form-field half zmargi">
            <input value="{{Session::get('website')}}" type="text" name="website" placeholder="Website" class="input input-full" />
          </div>    
          <div class="cf"></div> 
          <div class="form-field half zmargi">
            <input value="{{Session::get('tahiti_numero')}}" type="text"  placeholder="Numero De Tahiti (If you are in Tahiti)" class="input input-full"  name="tahiti_numero" maxlength="20" />
          </div>      
          <div class="form-field half zmargi">
            <input value="{{Session::get('tahiti_rc')}}" type="text"  placeholder="RC ( If you are in Tahiti)" class="input input-full" name="tahiti_rc" maxlength="20" />
          </div>    
          <div class="cf"></div>
          <div class="form-field half zmargi">
            <input type="text"  placeholder="Facebook Page Url" class="input input-full" name="fb_url" maxlength="150" />
          </div>  
          <div class="form-field half zmargi">
            <input value="{{Session::get('phone')}}" type="text" name="phone" placeholder="Business Phone Number" class="input input-full" maxlength="30" />
          </div>    
          <div class="cf"></div>   

          <div class="form-field half zmargi">
            <input value="{{Session::get('twitter_url')}}" type="text"  placeholder="Twitter Page Url" class="input input-full" name="twitter_url" maxlength="150" />
          </div>    

          <div class="form-field half zmargi">
            <input value="{{Session::get('youtube_url')}}" type="text"  placeholder="Youtube Channel Url" class="input input-full" name="youtube_url" maxlength="150" />
          </div>
          <div class="cf"></div> 
          <!--
          <div class="form-field half zmargi">
            <input value="{{Session::get('gplus_url')}}" type="text"  placeholder="Googleplus Page Url" class="input input-full" name="gplus_url" maxlength="30" />
          </div> 
          <div class="form-field half zmargi">
            <input type="text"  value="{{Session::get('linkedin_url')}}" placeholder="Linkedin Url" class="input input-full" name="linkedin_url" maxlength="30" />
          </div>    
          <div class="cf"></div>
          -->
          <div class="form-field half zmargi">
            <select class="input input-full required" name="category_id">
             <option value="">{{trans("business.Type of activity")}} </option>
             @foreach($categories as $category)
             <option value="{{$category->id}}" {{Session::get('category_id')==$category->id?"selected":""}}>{{$category->name}}</option>
             @endforeach
           </select>
         </div>
         <div class="form-field half zmargi">
          <select class="input input-full" name="responsibility">
            <option value="">{{trans("business.Your Responsibility")}} </option>
            <option value="owner" {{Session::get('responsibitly')=="owner"?"selected":""}}>Owner</option>
            <option value="director" {{Session::get('responsibitly')=="director"?"selected":""}}>Director</option>
            <option value="manager" {{Session::get('responsibitly')=="manager"?"selected":""}}>Manager</option>
          </select>
        </div>
        <div class="form-field half">
            <div class="input-container">
                <input type="file" name="logo" style="display:none;" id="imagefileinput">
                <input type="text" class="input input-long" value="Upload a Logo" disabled placeholder="" />
                <div class="input-addon"><button style="font-size: 13px; padding: 3px 11px;" onclick="$('#imagefileinput').click();" type="button" class="red-button">Browse</button></div>
            </div>
        </div>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('input[type="file"]').change(function(e) {
                  var files = e.target.files;
                  if(files.length>0){
                    $(e.target).parent().find('input[type="text"]').val(files[0].name);
                  }else{
                    $(e.target).parent().find('input[type="text"]').val("Upload a Logo");
                  }
                });
            });
        </script>
        <div class="cf"></div>
        <div class="form-field full editpage maps-container">
          <span class="label">Find your place and drop the pin:</span><br>
          <?php
           $latitude = Session::get('latitude')?:'-17.651409';
           $longitude = Session::get('longitude')?:'-149.425869';
          ?>
        <input type="hidden" id="latitude" name="latitude" value="{{$latitude}}" />
        <input type="hidden" id="longitude" name="longitude" value="{{$longitude}}" />  
          <div data="{{$latitude}},{{$longitude}}" id="map-canvas"></div>
          <small style="color:#404040;font-size:11px;margin:-18px 0px 8px;display:block;">Your gps position helps us to recommend your business to the users nearby</small>
        </div>
        <div class="form-field full editpage textarea">
          <span class="label">Description</span>
          <textarea name="description" placeholder="Add some details about your business" class="input input-full">{{Session::get('description')}}</textarea>
        </div> 
        <div class="cf"></div>  
        <div class="cf"></div>
        <div style="color:#838181;padding: 10px;">
         <input type="checkbox" id="accept-terms-check" name="terms" value="1" class="required"> 
         <label for="accept-terms-check">&nbsp; {{trans("business.I agree to the Terms and Conditions.")}}</label>
       </div>
       {{Form::token()}}
       <div class="cf"></div>
       <div style="width: 100%; margin-top: 10px;text-align:right;">
         <input type="submit" class="red-button" name="add" value="{{trans("business.Add Business")}}" style="font-size:16px;float:none;text-transform:none;font-family:Arial;" />
         Or &nbsp;
         <input type="submit" class="red-button" name="add" value="{{trans("business.Add + Infos")}}" style="font-size:16px;float:none;text-transform:none;font-family:Arial;" />
       </div>


       <div class="cf"></div>

     </div>

   </div>
 </form>
 <script type="text/javascript">
 jQuery(document).ready(function($) {
    $(document).scrollTop($("#lddbd_add_business_form").offset().top-100);
 });
 </script>
@stop
 @section('sidebar')
 @include('common.sidebar',array('widgets'=>array('add-btn', 'mobile-app')))
 @stop
