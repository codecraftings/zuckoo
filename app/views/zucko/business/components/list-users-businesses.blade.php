@if(count($businesses)<1)
Opps... No Businesses Found :(
@endif
@foreach($businesses as $key=>$business)
<div class="result-item">
    <div style="padding: 5px 8px;" class="inside">
        <div style="margin: 0px 7px 0px 35px;" class="info fl">
            <h3><span class="num">{{$key+1}}</span><a href="{{$business->profile_url}}">{{$business->present()->title(20)}}</a></h3>
            <address>
                <strong>{{$business->address}}</strong><br>
                {{$business->present()->city_name()}}, {{$business->present()->state_name()}} {{$business->zip_code}}<br>
                Phone number {{$business->present()->phone()}}<br>
            </address> 
        </div>
        <div style="width:134px;" class="stats fr">
            <div data-score="{{$business->rating_score?:1}}" class="rating"></div>
            <div class="review-count"><a href='{{$business->review_page}}'>{{$business->reviews()->count()}} reviews</a></div>
            <div class="">
                @if(isset($current_user)&&$current_user->id==$business->user->id)
                <a class="red-button" href="{{url('business/'.$business->id.'/edit')}}">Edit Business</a>
                @endif
            </div>
        </div>
        <div class="cf"></div>
    </div>
</div>
@endforeach
{{$businesses->links()}}