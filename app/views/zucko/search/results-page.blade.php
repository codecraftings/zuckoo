@extends('layouts.main', array('page_slag'=>'searchpage'))
@section('window-title')
Search Businesses | 
@if(Input::get('find'))
Find {{ucwords(Input::get('find'))}}
@endif
@if(Input::get('from'))
From {{ucwords(Input::get('from'))}}
@endif
@if(Input::get('cats'))
| Categories: {{ucwords(join(Input::get('cats'),','))}}
@endif
| @parent
@stop
@section('main-content')
<h2 class="title-stripped">
    <span class="text">{{trans("app.Search Results")}}</span>
</h2>
<div id="results-filter" class="">
    <h3 class="blue-header-title">{{trans("app.Search results for")}} {{Input::get('find')}}/{{Input::get('from')}} <span class="fr"></span></h3>
    <div id="" class="filters">
        <div class="col">
            <h5>Sort By</h5>
            <ul class="vmenu sort-option">
                <li>
                    <a class="{{$query->sorting=='match'?'active':''}} radio-btn sorting" data-value="match" href="javascript:void(0);">{{trans("app.Best Match")}}</a>
                </li>
                <li>
                    <a class="{{$query->sorting=='rating'?'active':''}} radio-btn sorting" data-value="rating" href="javascript:void(0);">{{trans("app.Highest Rated")}}</a>
                </li>
                <li>
                    <a class="{{$query->sorting=='review_count'?'active':''}} radio-btn sorting" data-value="review_count" href="javascript:void(0);">{{trans("app.Most Reviewed")}}</a>
                </li>
            </ul>
        </div>
        <div class="col hidden">
            <h5>Distance</h5>
            <ul class="vmenu distance-option">
                <li>
                    <a class="{{$query->distance=='eye'?'active':''}} radio-btn distance" data-value="eye" href="javascript:void();">Bird's Eye View</a>
                </li>
                <li>
                    <a class="{{$query->distance=='driving'?'active':''}} radio-btn distance" data-value="driving" href="javascript:void();">Driving(5km)</a>
                </li>
                <li>
                    <a class="{{$query->distance=='biking'?'active':''}} radio-btn distance" data-value="biking" href="javascript:void();">Biking(2km)</a>
                </li>
                <li>
                    <a class="{{$query->distance=='walking'?'active':''}} radio-btn distance" data-value="walking" href="javascript:void();">Walking(300m)</a>
                </li>
            </ul>
        </div>
        <div class="col">
            <h5>Features</h5>
            <ul class="vmenu feature-option">
                <li>
                    <label><input type="checkbox"{{in_array('open', $query->features)?" checked":""}} name="feature-option" id="current_hour_id" value="open"> {{trans("app.Open Now")}}( <span class="moment-date realtime" data-format="hh:mm A">HH:MM PM</span> )
                    </label>
                </li>
                <li>
                    <label><input type="checkbox"{{in_array('delivery', $query->features)?" checked":""}} name="feature-option" value="delivery"> {{trans("app.Delivery")}}</label>
                </li>
                <li>
                    <label><input type="checkbox"{{in_array('take_out', $query->features)?" checked":""}} name="feature-option" value="take_out"> {{trans("app.Take-out")}}</label>
                </li>
            </ul>
        </div>
        <div class="col">
            <h5>Price</h5>
            <ul class="vmenu price-option">
                <li>
                    <label><input{{in_array('high',$query->prices)?' checked':''}} type="checkbox" name="price-option" value="high"> 5001 XPF to more</label>
                </li>
                <li>
                    <label><input{{in_array('mid',$query->prices)?' checked':''}} type="checkbox" name="price-option" value="mid"> 3001 to 5000 XPF</label>
                </li>
                <li>
                    <label><input{{in_array('low',$query->prices)?' checked':''}} type="checkbox" name="price-option" value="low"> 1501 to 3000 XPF</label>
                </li>
                <li>
                    <label><input{{in_array('very_low',$query->prices)?' checked':''}} type="checkbox" name="price-option" value="very_low"> 1 to 1500 XPF</label>
                </li>
            </ul>
        </div>
        <div class="col">
            <h5>Category</h5>
            <ul class="vmenu feature-option">
                @foreach($categories->slice(2, 4) as $category)
                <li>
                    <label><input{{in_array($category->slag,$query->cats)?' checked':''}}  type="checkbox" name="cats-filter" value="{{$category->slag}}">{{$category->name}}</label>
                </li>
                @endforeach
            </ul>
            <a href="javascript:void()" class="show-all-cats-btn blue">show more</a>
        </div>
        <div class="cf"></div>
        <div class="filters-wide">
            <div id="filter-all-cats" class="col hidden">
                <h5>All Categories</h5>
                <ul class="menu all-cats">
                    @foreach($categories as $category)
                    <li>
                        <label><input{{in_array($category->slag,$query->cats)?' checked':''}} type="checkbox" name="cats-filter2" value="{{$category->slag}}">{{$category->name}}</label>
                    </li>
                    @endforeach
                    <!--
                    <li><label><input{{in_array('ft-tahitian',$query->cats)?' checked':''}} type="checkbox" name="cats-filter2" value="ft-tahitian">Tahitian Food</label>
                    </li>
                    <li><label><input{{in_array('ft-chinese',$query->cats)?' checked':''}} type="checkbox" name="cats-filter2" value="ft-chinese">Chinese Food</label>
                    </li>
                    <li><label><input{{in_array('ft-french',$query->cats)?' checked':''}} type="checkbox" name="cats-filter2" value="ft-french">French Food</label>
                    </li>
                    <li><label><input{{in_array('ft-bbq',$query->cats)?' checked':''}} type="checkbox" name="cats-filter2" value="ft-bbq">BBQ Food</label>
                    </li>
                    <li><label><input{{in_array('ft-sea',$query->cats)?' checked':''}} type="checkbox" name="cats-filter2" value="ft-sea">Sea Food</label>
                    </li>
                    <li><label><input{{in_array('ft-meat',$query->cats)?' checked':''}} type="checkbox" name="cats-filter2" value="ft-meat">Meat food</label>
                    </li>
                    -->
                </ul>
            </div>
            <div class="cf"></div>
        </div>
    </div>
</div>
<button id="hide-filter-btn" class="red-button fl toggle-filter-btn">{{trans("app.Hide Filters")}}<i class="white-arrow-down"></i></button>
<div class="paging-option fr">
    {{$offset = $query->pagging*$query->page-9}} to {{$businesses->getTotal()<($offset+$query->pagging-1)?$businesses->getTotal():($offset+$query->pagging-1)}} of {{$businesses->getTotal()}} - {{trans("app.Results per page")}}:
    <select id="searchpagepagging" name="pagging-option">
        <option value="10">10</option>
    </select>
</div>
<div class="cf"></div>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $("#mainsearchform").append('<input type="hidden" name="time" value="'+moment().format("dddhhA")+'"/>');
        $('#hide-filter-btn').click(function(event) {
            var container = $('#results-filter');
            var btn = $(this);
            if(container.hasClass('hidden2')){
                container.removeClass('hidden2');
                container.fadeIn('400', function() {
                });
                btn.html('Hide Filters<i class="white-arrow-down"></i>');
                return;
            }
            if(!container.hasClass('hidden2')){
                container.addClass('hidden2');
                container.fadeOut('400', function() {
                    btn.html('Show Filters');
                });
                return;
            }
        });
        $('#searchfieldsorting, #searchfielddistance, #searchfieldfeature, #searchfieldprice,#searchfieldcats, #searchfieldpagging').change(function(e){
            $('#mainsearchform').submit();
        });
        $('.radio-btn.sorting').click(function(e){
            var $this = $(e.target);
            $('.radio-btn.sorting.active').removeClass('active');
            $this.addClass('active');
            $('#searchfieldsorting').val($this.attr('data-value')).trigger('change');
        });
        $('.radio-btn.distance').click(function(e){
            var $this = $(e.target);
            $('.radio-btn.distance.active').removeClass('active');
            $this.addClass('active');
            $('#searchfielddistance').val($this.attr('data-value')).trigger('change');
        });
        $('.show-all-cats-btn').click(function(){
            if($('#filter-all-cats').hasClass('hidden')){
                $('#filter-all-cats').removeClass('hidden');
                $('.show-all-cats-btn').html('hide more');
            }
            else{
                $('#filter-all-cats').addClass('hidden');
                $('.show-all-cats-btn').html('show more');
            }
        });
        $('input[name="feature-option"]').change(function(event) {
            $('.extrafilters .searchfieldfeature').remove();
            $('input[name="feature-option"]:checked').each(function(i,val){
                var inp = $('<input type="hidden" name="features[]"/>');
                inp.addClass('searchfieldfeature');
                inp.val($(val).val());
                $('.extrafilters').append(inp);
            });
            $('#mainsearchform').submit();
        });
        $('input[name="price-option"]').change(function(event) {
            $('.extrafilters .searchfieldprice').remove();
            $('input[name="price-option"]:checked').each(function(i,val){
                var inp = $('<input type="hidden" name="prices[]"/>');
                inp.addClass('searchfieldprice');
                inp.val($(val).val());
                $('.extrafilters').append(inp);
            });
            $('#mainsearchform').submit();
        });
        $('input[name="cats-filter"]').change(function(event) {
            $('.extrafilters .searchfieldcats').remove();
            $('input[name="cats-filter"]:checked').each(function(i,val){
                var inp = $('<input type="hidden" name="cats[]"/>');
                inp.addClass('searchfieldcats');
                inp.val($(val).val());
                $('.extrafilters').append(inp);
            });
            $('#mainsearchform').submit();
        });
        $('input[name="cats-filter2"]').change(function(event) {
            $('.extrafilters .searchfieldcats').remove();
            $('input[name="cats-filter2"]:checked').each(function(i,val){
                var inp = $('<input type="hidden" name="cats[]"/>');
                inp.addClass('searchfieldcats');
                inp.val($(val).val());
                $('.extrafilters').append(inp);
            });
            $('#mainsearchform').submit();
        });
    });
</script>
<div class="results-container">
    @if(count($businesses)<1)
    <div class="nothing-found-msg"> 
        Opps... Your search don't match any business :(
    </div>
    @endif
    @foreach($businesses as $key=>$business)
    @if($business->is_bal)
    <div class="result-item">
        <div style="position:relative;" class="inside">
            <?php
            $data = $business->getSerializedData();
            ?>
            <span style="position: absolute; top: 17px; left: 12px; margin: 0px;" class="num">{{$key+1}}</span>
            <ul style="margin-left:30px;margin-top:8px;" class="classified-info"> 
                @foreach($data as $key=>$value)
                <li><span class="key">{{$key}}: </span><span class="value">{{$value}}</span></li>
                @endforeach
            </ul>
            <div class="cf"></div>
        </div>
    </div>
    @else
    <div class="result-item">
        <div class="inside">
            <div class="img-container img-container-curve fl">
                <img src="{{url($business->photos()->count()>0?$business->photos()->first()->thumb_path:'img/no_image.jpg')}}" />
            </div>
            <div class="info fl">
                <h3><span class="num">{{$key+1}}</span><a href="{{$business->profile_url}}">{{$business->present()->title(20)}}</a></h3>
                <address>
                    <strong>{{$business->address}}</strong><br>
                    {{$business->present()->city_name()}}, {{$business->present()->state_name()}} {{$business->zip_code}}<br>
                    Phone number {{$business->present()->phone()}}<br>
                </address> 
            </div>
            <div class="stats fr">
                <div data-score="{{$business->rating_score?:1}}" class="rating"></div>
                <div class="review-count"><a href='{{$business->review_page}}'>{{$count = $business->reviews()->count()}} {{Lang::choice("app.reviews", $count)}}</a></div>
                <div class="cat-info">{{$business->present()->category_name()}} | {{$business->present()->city_name()}} </div>
                <div class="buttons">
                    <button class="red-button show-review-btn">{{trans("review.Write Review")}}</button>
                    <a style="margin: 8px 0px 0px 0px;" href="javascript:void()" class="icon-arrow-down-black fr"></a>
                </div>
            </div>
            <div class="cf"></div>
            <div class="comments all-reviews hidden">
                <div class="msg hidden"></div>
                <div class="comment">
                    <div class="img-container img-container-curve fl">
                        <img src="{{isset($current_user)?$current_user->profile_pic:url('img/sample5.png')}}" />
                    </div>
                    <form autocomplete="off" name="add-review-form">
                        <div style="width: 580px;" class="desc fl">
                            @if(isset($current_user))
                            {{Form::token()}}
                            <input type="hidden" name="business_id" value="{{$business->id}}">
                            <h5 style="margin: 0px 0px 3px;"><a class="blue" href="{{$current_user->profile_url}}">{{$current_user->present()->name()}}</a> <span class="date">Pick a rating <span style="vertical-align:middle;margin:0px 13px 0px;" data-score="0" class="rating2"></span></span></h5>
                            <textarea style="width: 430px; height: 43px;" name="description" class="input fl" placeholder="Write Your Review"></textarea>
                            <button style="float: left; margin: 14px 0px 0px 10px;" class="blue-button submit-review-btn fl">{{trans("review.Post Review")}}</button>
                            @else
                            <div style="margin:10px 0px 0px 0px;">{{trans("app.you aren't logged in")}}. Please <a href="{{url('login')}}">login</a> to review this business.
                            </div>
                            @endif
                            <a style="margin: 20px 0px 0px 0px;" href="javascript:void()" class="icon-arrow-down-black2 fr hidden"></a>
                        </div>
                    </form>
                    <div class="cf">

                    </div>
                </div>
                <div class="others-review">
                    <?php
                    $reviews = $business->reviews()->take(5)->get();
                    ?>
                    @foreach($reviews as $key=>$review)
                    <div class="comment">
                        <div class="img-container img-container-curve fl">
                            <img src="{{$review->user->profile_pic}}" />
                        </div>
                        <div class="desc fl">
                            <h5><a class="blue" href="{{$review->user->profile_url}}">{{$review->user->present()->name()}}</a> <span class="moment-date date" data-time="{{$review->created_at->timestamp}}" data-format="MMMM DD, YYYY \a\t hh:mm A">{{$review->created_at->format('F j, Y \a\t h:i A ')}}</span> <span data-score="{{$review->rating}}" style="vertical-align:middle;margin:0px 0px 0px 20px;" class="rating"></span></h5>
                            <p>
                                {{$review->present()->desc(100)}}
                            </p>
                        </div>
                        <div class="cf"></div>
                    </div>
                    @endforeach
                </div>
                <div class="cf">
                    <a style="margin: 10px 0px 0px 0px;" href="javascript:void()" class="icon-arrow-up-black fr"></a>
                </div>
            </div>
        </div>
    </div>
    @endif
    @endforeach
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('.rating2').raty({
                'readOnly':false,
                'score': function(){
                    return $(this).attr('data-score');
                }
            });
            $('.submit-review-btn').click(function(event){
                event.preventDefault();
                var $this = $(this);
                var parent = $this.parents('.result-item');
                $this.html('Posting...');
                $this.attr('disabled', 'true');
                var form = parent.find('form[name="add-review-form"]');
                var data = form.serialize();
                var api = new ZuckoApi('review/create', data);
                api.call('post').done(function(res){
                    parent.find('div.msg').removeClass('error info').addClass('success').html('Review sent successfully').fadeIn(200);
                    form[0].reset();
                }).fail(function(res){
                    parent.find('div.msg').removeClass('success info').addClass('error').html(res.error_message).fadeIn(200);
                }).always(function(){
                    $this.html('Post Review');
                    $this.removeAttr('disabled');
                });
            });
            $('.show-review-btn').click(function(event) {
                var parent = $(event.target).parents('.result-item');
                event.preventDefault();
                parent.find('div.msg').hide();
                parent.find('form')[0].reset();
                parent.find('.icon-arrow-down-black').fadeOut(200, function(){
                    parent.find('.icon-arrow-up-black').show();
                    parent.find('.all-reviews').fadeIn(300, function(){
                    });
                    
                });
                parent.find('.others-review').mCustomScrollbar();
            });
            $('.icon-arrow-down-black').click(function(event) {
                var parent = $(this).parents('.result-item');
                parent.find('.icon-arrow-down-black').fadeOut(200, function(){
                    parent.find('.icon-arrow-up-black').show();
                    parent.find('.all-reviews').fadeIn(300, function(){
                    });
                    
                });
                parent.find('.others-review').mCustomScrollbar();
            });
            $('.icon-arrow-up-black').click(function(event) {
                var parent = $(this).parents('.result-item');
                parent.find('.icon-arrow-up-black').fadeOut(200, function(){ 
                    parent.find('.all-reviews').fadeOut(300, function(){
                        parent.find('.icon-arrow-down-black').fadeIn(200);
                        parent.find('.others-review').mCustomScrollbar('destroy');
                    });
                });
            });
        });
</script>
{{$businesses->appends(Input::only('find','from','sorting','distance','feature','price','cats','pagging'))->links()}}
</div>
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn', 'mobile-app')))
@stop