@extends('layouts.main')
@section('main-content')
<h2 class="title-stripped"><span class="text">{{trans("cities.Search City")}}</span></h2>
@if(isset($q))
<h3 class="blue-header-title">Search Results for {{$q}}</h3>
@else
<h3 class="blue-header-title">{{trans("cities.Popular Cities")}}</h3>
@endif
<div class="city-list-container cf">
	@if(count($cities)<1)
	<div class="nothing-found-msg">
		Ops! Your search don't match any city.
	</div>
	@endif
	<ul class="city-list">	
		@foreach($cities as $city)
		<li><a href="{{$city->events_page}}">{{$city->name}}<span class="event-count">({{$city->events()->where('listing_status','=','published')->count()}})</span></a></li>
		@endforeach
	</ul>
</div><!--END papeete-->
<div class="search_cities cf">
	<form method="get" action="">
	<input style="vertical-align:middle;" name="q" placeholder="{{trans("cities.Choose a different city")}}" class="input input-long" value="{{Input::get('q')}}" type="text" />
		<input type="submit" style="float:none;" class="red-button" value="{{trans("cities.SEARCH CITY")}}" />
	</form>
</div><!--END papeete-->
<h3 class="blue-header-title hidden">Annu@ire International Sites</h3>
<div class="countries hidden">
	<p>Australia   Austria   Belgium   Brazil   Canada   Czech Republic   Denmark   Finland   France   Germany
		Ireland   Italy   New Zealand   Norway   Poland   Singapore   Spain   Sweden   Switzerland   The Netherlands   Turkey   United Kingdom   United States</p>
</div><!--END countries-->
	@stop
	@section('sidebar')
	@include('common.sidebar',array('widgets'=>array('add-btn', 'mobile-app')))
	@stop