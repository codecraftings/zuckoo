@extends('layouts.main')
@section('main-content')
@section('window-title')
{{$review->user->present()->name()}}'s review about {{$review->business->present()->title(25)}} | @parent
@stop
@section('main-content')
	<div class="user-recent-feeds review-profile cf">
		<div class="user-thumb fl" style="width: 140px;text-align:center;">
			<div style="width: 80px;height:80px;margin: 10px 28px;" class="img-container img-container-curve">
				<img src="{{$review->user->profile_pic}}">
				<h3 style="font-size: 15px; line-height: 15px; margin: 12px 0px; padding: 0px; color: rgb(220, 57, 5);"><a href="{{$review->user->profile_url}}">{{$review->user->present()->name()}}</a></h3>
			</div>
		</div>
		<div style="width:500px;float:right;" data-review='{{$review->withUserInteractions(isset($current_user)?$current_user:false)->toJson(JSON_HEX_APOS|JSON_HEX_QUOT)}}' class="feed-item backbone-review-view">
			<div class="businss fl">
				<h4 style="font-size:16px;font-weight:400;font-family:NotoSansRegular;color:#d72e00;"><a href="{{$review->business->profile_url}}">{{$review->business->present()->title(18)}}</a></h4>
				<p style="color:#404040;font-size:12px;">Category: <a class="red" href="{{$review->business->category->page_url}}">{{$review->business->present()->category_name()}}</a></p>
				<div data-score="{{$review->business->rating_score}}" style="margin-top:2px;" class="rating"></div>
			</div><!--END hotel-->
			<div style="width:260px;" class="business-address fr">
				<address style="font-style:normal!important;">
					{{$review->business->present()->addr(20)}}<br/>
					{{$review->business->present()->city_name()}}, {{$review->business->zip_code}}<br/>
				</address>
					Phone: {{$review->business->present()->phone()}}
					
			</div><!--END category-->
			<div class="desc2">
				<p>
					{{nl2br(e($review->description))}}
				</p>
				<p class="post-date">
					Posted on {{$review->created_at->toDayDateTimeString()}}
				</p>
			</div><!--END desc-->
			<div style="margin-top:4px;font-family:NotoSansRegular;color:#808080;font-size:13px;font-weight:400;" class="fl">
				Was this review 
				<a href="{{isset($current_user)?'javascript:void(0)':url('login')}}" style="" data-rid="{{$review->id}}" data-type="useful" class="red-button vote-button useful review-rating-button">USEFUL<i class="icon-tick"></i></a>
				<a href="{{isset($current_user)?'javascript:void(0)':url('login')}}" style="margin-left:0px!important" data-rid="{{$review->id}}" data-type="funny" class="red-button vote-button funny review-rating-button">FUNNY<i class="icon-tick"></i></a>
			</div>
			<div class="cf"></div>
			<div style="margin-top:7px;" class="comment_options cf">
				<!--
				<a class="" href=""><i class="send_commpliments"></i> Send Complement&nbsp;&nbsp;</a>
				<a href=""><i class="send_friend"></i> Like This&nbsp;&nbsp;</a>
				-->
				<a href="{{$review->permalink}}"><i class="ink_review"></i> Link to this review</a>
				<a style="margin-left:8px;text-decoration:none;cursor:default;" href="javascript:void(0)"><i class="vote_result_icon"></i> Useful <span class="stats-useful">0%</span></a>
				<a style="margin-left:8px;text-decoration:none;cursor:default;" href="javascript:void(0)"><i class="vote_result_icon"></i> Funny <span class="stats-funny">0%</span></a>
				<a style="margin-left:8px;text-decoration:none;cursor:default;" href="javascript:void(0)"><i class="likes_count_icon"></i> Likes <span class="stats-likes">00</span></a>
			</div>
		</div>
		<div class="cf"></div>
	</div>
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn', 'mobile-app')))
@stop