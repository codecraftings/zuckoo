<div class="reviews-list">
	@if(count($reviews)<1)
	Ops no reviews for this business yet!
	@endif
	{{--dd(current_user())--}}
	@foreach($reviews as $review)
	<div data-review='{{$review->withUserInteractions(isset($current_user)?$current_user:false)->toJson(JSON_HEX_APOS|JSON_HEX_QUOT)}}' class="review-item backbone-review-view">
		<div class="poster-pic fl">
			<div class="img-container img-container-curve">
				<img src="{{$review->user->profile_pic or url('img/no_image.jpg')}}"/>
			</div>
		</div>
		<div class="review-content fl">
			<a href='{{$review->user->profile_url}}' class="poster-name fl">{{$review->user->present()->name()}}</a>
			<span class="date fl">Posted on {{$review->created_at->toDayDateTimeString()}}</span>
			<div data-score="{{$review->rating}}" class="rating fr">
			</div>
			<div class="cf"></div>
			<p class="description">
				{{nl2br(e($review->description))}}
			</p>
			<div class="actions">
				<!--
				<i class="icon icon-comment"></i><a href="#"> send complement </a><i class="separator"></i>
				-->
				<span class="stats-likes">{{$review->likes()->count()}}</span><a class="like-btn" href="{{isset($current_user)?'javascript:void(0);':url('login')}}"><i class="icon icon-like"></i></a>
				<!--
				<i class="separator"></i><i class="icon icon-view"></i><a href="#">views @{{$review->view_count}}</a>
				-->
			</div>
		</div>
		<div class="cf"></div>
	</div>
	@endforeach
</div>