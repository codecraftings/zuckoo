<div class="review_admin area add-review-container">
	<form method="post" name="add-review-form">
		<div class="write_comment review-writer">
			<div class="msg hidden top"></div>
			<div style="margin-right: 18px; margin-bottom: 5px;" class="rating-picker fr">
				Pick a rating <div class="rating2 fr"></div>
			</div>
			<div class="cf"></div>
			{{Form::token()}}
			<div class="img-container img-container-curve fl">
				<img src="{{isset($current_user)?$current_user->profile_pic:url('img/sample5.png')}}"/>
			</div>
			<div class="write_admin fr">
				<input type="hidden" name="business_id" value="{{$business->id}}">
				<textarea type="text" name="description" class="input fr" placeholder="Write you review..."></textarea>
				<div style="margin-bottom:7px;" class="cf"></div>
				<button type="button" class="blue-button submit-review-button fr" value="POST">POST</button>
				<div class="cf"></div>
			</div>
			<div class="cf"></div>
		</div>
	</form>
	<div style="max-height:none;" class="others-review"> 
		@foreach($business->reviews as $review)
		<div id="review_{{$review->id}}" class="one_comment review-item2 cf">
			<div class="img-container img-container-curve fl">
				<img src="{{$review->user->profile_pic}}"/>
			</div>
			<div class="a_comment fr cf">
				<h5>
					<a href="{{$review->user->profile_url}}">{{$review->user->nickname}}</a>
					&nbsp;&nbsp;<span class="date moment-date" data-time="{{$review->created_at->timestamp}}" data-format="MMMM DD, YYYY \a\t hh:mm A">{{$review->created_at->format('F j, Y \a\t h:i A')}}</span>
					<span data-score="{{$review->rating}}" class="rating fr"></span>
				</h5>

				<p>
					{{$review->present()->desc(200)}}
				</p>
				<button type="button" class="blue-button fr reply-button">REPLY</button>
				
				<a class="gray-small fr show-comments-button" style="display:inline-block;margin: 3px 27px 0px 0px;" href="#comments">{{$review->comments()->count()}} comments</a>
				<a class="gray-small fr like-review-button" style="display:inline-block;margin: 3px 27px 0px 0px;" data-rid="{{$review->id}}" href="{{$review->like_url}}">{{$review->likes()->count()}} likes</a>
				<div class="cf"></div> 
				<div class="comments-container2 hidden review-comment-items">
					<div class="review-comment-items-list"> 
						@foreach($review->comments as $comment)
						<div style="margin:8px 0px 0px 0px;" id="comment_{{$comment->id}}" class="one_comment review-comment-item sub cf">
							<div class="img-container img-container-curve fl">
								<img data-src="@{{&user.profile_pic}}" src="{{$comment->user->profile_pic}}"/>
							</div>
							<div class="a_comment fr cf">
								<h5>
									<a data-href="@{{&user.profile_url}}" data-bind="@{{user.nickname}}" href="{{$comment->user->profile_url}}">{{$comment->user->nickname}}</a>
									&nbsp;&nbsp;<span data-bind="@{{created_at}}" class="date moment-date" data-time="{{$comment->created_at->timestamp}}" data-format="MMMM DD, YYYY \a\t hh:mm A">{{$comment->created_at->format('F j, Y \a\t h:i A')}}</span>
								</h5>

								<p data-bind="@{{description}}">
									{{$comment->description}}
								</p>
							</div>
						</div>
						@endforeach
					</div>
					<form autocomplete="off" name="review-comment-form" action="{{$review->comments_page}}" method="post">
						<div class="msg hidden"></div>
						<div class="write_reply cf">
							<div class="img-container img-container-curve fl">
								<img src="{{isset($current_user)?$current_user->profile_pic:url('img/sample5.png')}}"/>
							</div>
							<input type="hidden" name="target_id" value="{{$review->id}}">
							<textarea name="description" type="text" class="input fr" placeholder="Write your reply.."></textarea>
							<div class="cf"></div>
							{{Form::token()}}
							<button type="button" class="blue-button write-reply-button fr cf">POST</button>
						</div>
					</form>
				</div>
			</div>
		</div><!--END one_comment-->
		@endforeach
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.rating2').raty({
			'readOnly':false,
			'score': function(){
				return $(this).attr('data-score');
			}
		});
		$('.like-review-button').click(function(event) {
			/* Act on the event */
			event.preventDefault();
			var $this = $(this);
			$this.html('wait..');
			$this.attr('disabled', 'true');
			var api = new ZuckoApi('review/like', {
				'review_id':$this.attr('data-rid')
			});
			api.call('post').done(function(res){
				$this.html(res.likes_count+" likes");
			}).fail(function(res){
				$this.html('like');
			})
		});
		$('.write-reply-button').click(function(event) {
			event.preventDefault();
			var $this = $(this);
			var parent = $this.parents('.review-comment-items');
			$this.html('Posting..');
			$this.attr('disabled', 'true');
			var form = parent.find('form[name="review-comment-form"]');
			var data = form.serialize();
			var api = new ZuckoApi('review/comment', data);
			api.call('post').done(function(res){
				console.log(res);
				res.comment.created_at = "Just now";
				var com = parent.find('.review-comment-item').first().cloneWithData(res.comment);
				parent.find('.review-comment-items-list').append(com);
				parent.find('div.msg').removeClass('error info').addClass('success').html(res.message).fadeIn(200);
				form[0].reset();
			}).fail(function(res){
				parent.find('div.msg').removeClass('success info').addClass('error').html(res.error_message).fadeIn(200);
			}).always(function(){
				$this.html('Post');
				$this.removeAttr('disabled');
			});
		});
		$('.submit-review-button').click(function(event){
			event.preventDefault();
			var $this = $(this);
			var parent = $this.parents('.add-review-container');
			$this.html('Posting...');
			$this.attr('disabled', 'true');
			var form = parent.find('form[name="add-review-form"]');
			var data = form.serialize();
			var api = new ZuckoApi('review/create', data);
			api.call('post').done(function(res){
				parent.find('div.msg.top').removeClass('error info').addClass('success').html('Review sent successfully').fadeIn(200);
				form[0].reset();
			}).fail(function(res){
				parent.find('div.msg.top').removeClass('success info').addClass('error').html(res.error_message).fadeIn(200);
			}).always(function(){
				$this.html('Post Review');
				$this.removeAttr('disabled');
			});
		});
		$('.reply-button, .show-comments-button').click(function(event) {
			/* Act on the event */
			event.preventDefault();
			var $this = $(this);
			var parent = $this.parents('.review-item2');
			var cont = parent.find('.comments-container2');
			if(cont.is(':visible')){
				cont.fadeOut('400', function() {
					
				});
				return;
			}
			cont.fadeIn('400', function() {
				
			});
		});
	});
</script>