@extends('layouts.main')
@section('window-title')
    {{trans("blog.Add A Blog")}} | @parent
@stop
@section('extra-scripts')
{{HTML::style(url('css/redactor.css'))}}
{{HTML::script(url('js/redactor.js'))}}
@stop
@section('main-content')
<h2 class="title-stripped">
    <span class="text">{{trans("blog.Add A Blog")}}</span>
</h2>
<!-- Main Content Here -->
<p>
    {{trans("lines.Do you want to talk or debate on something? Here is the right place to do it")}}!
</p>
<p>
    {{trans("lines.The Annuaire Polynesien allow you to share your experiences with your friends or just poeple. Just fill in the form below and here we go")}}!
</p>
<div class="form-container">
    <div class="inside">
        <h3>
            {{trans("blog.Add A Blog")}}
        </h3>
        @if(Session::has('info'))
        <div class="msg">{{Session::get('info')}}</div>
        @endif
        @if(Session::has('success'))
        <div class="msg success">{{Session::get('success')}}</div>
        @endif
        @if(Session::has('error'))
        <div class="msg error">{{Session::get('error')}}</div>
        @endif
        <br>
        <form method="post" action="" enctype="multipart/form-data" autocomplete="off">
            <p>1- {{trans("blog.Pick a category")}}</p>
            <ul class="category-list">
                @foreach($categories as $category)
                @if(isset($category->style->color))
                <li>
                    <a href="{{$category->page_url}}" data-id="{{$category->id}}" class="category{{Session::get('category_id')==$category->id?' selected':''}} selectable color-{{$category->style->color}}"><i class="icon {{$category->style->class}}"></i>{{trans("cat.".$category->name)}}</a>
                </li>
                @endif
                @endforeach
            </ul>
            <input type="hidden" name="category_id" value="{{Session::get('category_id')}}">
            {{Form::token()}}
            <p>2- {{trans("blog.Enter blog title and description")}}</p><br>
            <div class="form-field half">
                <input class="input input-long" type="text" value="{{Session::get('title')}}" name="title" placeholder="{{trans("blog.Your Blog Title")}}" />
            </div>
            <div class="cf"></div>
            <div style="margin-bottom:0px;" class="form-field full">
                <span class="label">Upload a featured image</span>
            <small style="color:#404040;font-size:11px;margin:-8px 0px 8px;display:block;">This will be shown at the top of your blog post. Choose atleast 650x300px width image</small>
            </div>
            <div class="form-field half">
                <div class="input-container">
                    <input type="file" id="imgfileinp" name="photo" class="hidden"/>
                    <input id="imgsellabel" type="text" class="input input-long" value="Upload Pictures" disabled placeholder="" />
                    <div class="input-addon"><button id="imgselectbtn" type="button" class="blue-button">{{trans("register.Browse")}}</button></div>
                </div>
            </div>
            <div class="cf"></div>
            <div class="form-field full">
                <textarea id="blog_description" value="" name="content" class="input input-xlong" style="height:100px;" placeholder="Description">{{Session::get('content')}}</textarea>
            </div>
            <div class="notice">*All Fields are required</div>
            <input type="submit" name="submit" class="red-button fr" value="{{trans("app.Submit")}}" />
            <div class="cf"></div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(
        function()
        {
            $('#imgselectbtn').click(function(){
                $('#imgfileinp').click();
            });
            $('#imgfileinp').change(function(e){
                var files = e.target.files;
                $('#imgsellabel').val(files.length+" Pictures Selected");
            });
            $('#blog_description').redactor({
                imageUpload: ''
            });
            $('.category.selectable').click(function(event) {
                event.preventDefault();
                $('.category.selectable.selected').removeClass('selected');
                $(this).addClass('selected');
                $('input[name="category_id"]').val($(this).attr('data-id'));
            });
        }
    );
</script>
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn','latest-news','mobile-app')))
@stop