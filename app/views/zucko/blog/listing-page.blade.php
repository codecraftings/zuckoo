@extends('layouts.main')
@section('window-title')
Blogs | @parent
@stop
@section('main-content')
<h2 style="width: 606px; float:right;margin-bottom: 20px" class="title-stripped">
    <span class="text">Blogs{{isset($search_term)?': '.$search_term:''}}</span>
</h2>
<div class="cf"></div>
@foreach($blogs as $key=>$blog)
<div style="margin: 0px 0px 25px 0px;" class="blog_posts color-{{$key%2==1?'blue':'maron'}}">
    <div class="blog_date fl cf">
        <h4>{{$blog->created_at->format('d')}}</h4>
        <p>{{$blog->created_at->format('F')}}</p>
        <p>{{$blog->created_at->format('Y')}}</p>
    </div><!--END blog_date-->
    <div class="post_content fr">
        <img src="{{$blog->img->url or url('img/no_image.jpg')}}" alt="blog" />
        <div class="blog-title">
            <h4 class="fl"><a href="{{$blog->permalink}}">{{$blog->present()->show_title(40)}}</a></h4>
            <div class="comments_no_area fr">
                <i class="comments_no fl"></i><p class="fr">{{$blog->comments()->count()}}</p>
            </div><!--END comments_no_area-->
            <div class="cf"></div>
        </div><!--END title-->
        <div class="the_post cf">
            <p>
                {{$blog->present()->desc(250, '...')}}
            </p>
        </div><!--END the_post-->
        <div class="post_actions">
            <a href="{{$blog->author->profile_url}}"><i class="gravatar"></i>{{$blog->author->nickname}}</a>
            <a href="{{$blog->category->listing_page}}"><i class="category_icon1"></i>{{$blog->category->name}}</a>
            <a style="margin:0px 0px 0px 21px;color:white" class="dark-button" href="{{url('blog/add')}}">{{trans("app.ADD YOUR BLOG")}}!</a>
            <a href="{{$blog->permalink}}" class="read-more fr"><i class="right_read_icon"></i>{{trans("app.Read More")}}</a>
            <div class="cf"></div>
        </div><!--END post_actions-->
    </div><!--END blog_post-->
    <div class="cf"></div>
</div><!--END blog_posts-->
<div class="cf"></div>
@endforeach
@if(count($blogs)<1)
<div class="nothing-found-msg"> 
    Ops no blog found!
</div>
@endif
{{$blogs->appends(Input::only('cat','q','pagging'))->links()}}
<!--Main Content-->
<!--Sidebar-->
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn', 'top-category', 'mobile-app')))
@stop