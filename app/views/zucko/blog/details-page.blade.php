@extends('layouts.main')
@section('window-title')
Zucko Blog | {{$blog->present()->show_title(40)}} | @parent
@stop
@section('main-content')
<h2 style="" class="title-stripped">
	<span style="display:inline-block;font-size:22px;max-width:74%;" class="text">{{$blog->present()->show_title(100)}}</span>
</h2>
<div class="cf"></div>
<div class="blog-header">
	<img src="{{$blog->img->url or url('img/no_image.jpg')}}" alt="building" />
	<div class="edit_option">

		<a href="javascript:void();" style="margin-left:8px;" class="red disabled"><i style="vertical-align:middle;" class="time_icon" ></i> {{$blog->created_at->format('F d, Y')}}</a>
		<span class="vseparator"></span>
		<a href="{{$blog->author->profile_url}}" class="red"><i class="admin_icon"></i> {{$blog->author->present()->name()}}</a>
		<a href="#comments" style="font-size: 22px; color: #AAA5A5; margin-right: 5px;" class="fr"><i class="comments_no"></i>&nbsp;&nbsp;{{$blog->comments()->count()}}</a>
		@if(isset($current_user)&&$blog->author->id==$current_user->id)
		<a style="margin-right: 20px;" href="{{url('blog/edit/'.$blog->id)}}" class="red-button fr">{{trans("app.Edit")}}</a>
		@endif
		<div class="cf"></div>
	</div><!--END edit_option-->
	<div class="cf"></div>
</div><!--END edit_blog-->
<div class="blog_topic_text">
	{{$blog->present()->desc()}}
</div><!--END blog_topic_text-->
<div id="comments">
	<h4 class="blog-comments-title">{{$blog->comments->count()}} THOUGHTS ON “{{$blog->present()->show_title(30)}}”</h4>
	@if(Session::has('info'))
	<div class="msg">{{Session::get('info')}}</div>
	@endif
	@if(Session::has('success'))
	<div class="msg success">{{Session::get('success')}}</div>
	@endif
	@if(Session::has('error'))
	<div class="msg error">{{Session::get('error')}}</div>
	@endif
	<div class="comments inside-post-comments">
		@foreach($blog->comments as $comment)
		@if($comment->parent_id==0)
		<div id="comment_{{$comment->id}}" class="comment">
			<div class="img-container img-container-curve fl">
				<img src="{{$comment->user->profile_pic}}" />
			</div>
			<div class="desc fl">
				<h5>
					<a href="{{$comment->user->profile_url}}">{{$comment->user->nickname}}</a> 
					<span class="date">{{$comment->created_at->format('F d, Y \a\t h:i A')}}</span>
					<button class="blue-button fr reply-button">Reply</button>
					@if(1==0&&isset($current_user)&&$comment->user->id==$current_user->id)
					<button class="red-button fr">Eidt</button>
					@endif
				</h5>
				<p>
					{{e($comment->description)}}
				</p>
			</div>
			<div class="cf"></div>
			@if($comment->replies()->count()>0)
			@foreach($comment->replies as $reply)
			<div id="comment_{{$reply->id}}" class="comment sub">
				<div class="img-container img-container-curve fl">
					<img src="{{$reply->user->profile_pic}}" />
				</div>
				<div class="desc fl">
					<h5><a href="{{$reply->user->profile_url}}">{{$reply->user->nickname}}</a> <span class="date">{{$reply->created_at->format('F d, Y \a\t h:i A')}}</span></h5>
					<p>
						{{e($reply->description)}}
					</p>
				</div>
				<div class="cf"></div>
			</div>
			@endforeach
			@endif
			<form method="post" action="{{url('blog/comment/create')}}">
				<div class="reply_section sub hidden fr">
					<h2>{{trans("app.LEAVE A REPLY")}}</h2>
					@if(!isset($current_user))
					<p>Please <a href="{{url('login')}}">login</a> to leave a reply.</p>
					@else
					<p>Logged in as : <a href="{{$current_user->profile_url}}">{{$current_user->nickname}}</a></p><br>
					@endif
					<input type="hidden" name="blog_id" value="{{$blog->id}}" />
					<input type="hidden" name="parent_id" value="{{$comment->id}}" />
					{{Form::token()}}
					<textarea class="input" rows="5" name="description" placeholder="Comment"></textarea>
					<div class="fl">
						<input style="display:inline-block;margin-right:20px;" class="blue-button fl" type="submit" value="Post Reply" />
					</div>
					<button type="button" style="font-size: 13px; padding: 4px 15px;" class="red-button cancel-button fl">Cancel</button>
					<div class="cf"></div>
				</div><!--END reply-->
			</form>
			<div class="cf"></div>
		</div>
		@endif
		@endforeach
	</div>
	<form method="post" action="{{url('blog/comment/create')}}">
		<div class="reply_section fr">
			<h2>{{trans("app.LEAVE A COMMENT")}}</h2>
			@if(!isset($current_user))
			<p>Please <a href="{{url('login')}}">login</a> to leave a comment on this blog post.</p>
			@else
			<p>Logged in as : <a href="{{$current_user->profile_url}}">{{$current_user->nickname}}</a></p><br>
			@endif
			<input type="hidden" name="blog_id" value="{{$blog->id}}">
			{{Form::token()}}
			<textarea name="description" class="input" rows="5" placeholder="{{trans("app.Comment")}}"></textarea>
			<input class="blue-button" type="submit" value="{{trans("blog.Post comment")}}" />
		</div><!--END reply-->
	</form>
	<div style="margin-bottom:40px;" class="cf"></div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.reply-button').click(function(event) {
			var $this = $(this);
			var parent = $this.parents('.comment');
			$('.reply_section.sub').hide();
			parent.find('.reply_section').fadeIn('400', function() {
				
			});
		});
		$('.cancel-button').click(function(event) {
			$(this).parents('.reply_section').fadeOut('400', function() {
				
			});
		});
	});
</script>
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn','latest-news','mobile-app')))
@stop