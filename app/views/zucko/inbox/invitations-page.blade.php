@extends('layouts.main')
@section('window-title')
Your Invitations | @parent
@stop
@section('main-content')
<h2 class="title-stripped">
    <span class="text">Inbox</span>
</h2>
<div class="tabs-container" id="inbox-tabs">
    <ul class="tabs-menu">
        <li class=""><a class="tabs-menu-button" href="{{url('inbox')}}">Messages</a></li>
        <li class="ui-tabs-active"><a class="tabs-menu-button" href="{{url('inbox/invitations')}}">Invitations ({{count($invitations)}})</a></li>
        <li class=""><a class="tabs-menu-button" href="{{url('inbox/sent')}}">Sent Messages</a></li>
    </ul>
    <div class="tab-content" id="message-tab">
        <div class="messages-container">
            @if(count($invitations)<1)
            <div style="margin:10px 10px 0px 10px;" class="msg error">You dont have any pending invitations</div>
            @endif
            @foreach($invitations as $user)
            <div class="cursor-default message">
                <div class="fl img-section">
                    <div class="img-container img-container-curve small">
                        <img src="{{$user->profile_pic}}" />
                    </div>
                    <input class="hidden" type="checkbox" />
                </div>
                <div class="fr msg-content">
                    <h5><a class="blue" href="{{$user->profile_url}}">{{$user->present()->name()}}</a>
                    </h5>
                    <p><a href="{{url('invitations/accept/'.$user->id)}}" class="blue-button accept">Accept</a> <a class="red-button decline" href="{{url('invitations/decline/'.$user->id)}}">Decline</a></p>
                </div>
                <div class="cf"></div>
            </div>
            @endforeach
        </div>
        <div class="inbox-actions hidden">
            <button class="red-button">Select All</button>
            <button class="red-button">Delete</button>
            <button class="red-button">Delete All</button>
        </div>
        <div class="cf"></div>
    </div>
    <div id="invitation-tab">

    </div>
    <div id="sent-tab">

    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(document).scrollTop($('.messages-container>.message:last-child').offset().top-200);
    });
</script>
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn','latest-news', 'mobile-app')))
@stop