@extends('layouts.main')
@section('window-title')
Inbox | @parent
@stop
@section('main-content')
<h2 class="title-stripped">
    <span class="text">Inbox</span>
</h2>
<div class="tabs-container" id="inbox-tabs">
    <ul class="tabs-menu">
        <li class="ui-tabs-active"><a class="tabs-menu-button" href="{{url('inbox')}}">Messages</a></li>
        <li><a class="tabs-menu-button" href="{{url('inbox/invitations')}}">Invitations ({{count($invitations)}})</a></li>
        <li class=""><a class="tabs-menu-button" href="{{url('inbox/sent')}}">Sent Messages</a></li>
    </ul>
    <div class="tab-content" id="message-tab">
        <div class="blue-header-title"><input type="text" class="input input-long" placeholder="Search Messages"><button onclick="top.location.reload()" class="red-button fr">Refresh</button></div>
        <div class="messages-container">
            <h3>{{$thread->subject}} - Started By {{$thread->first_user()->first()->present()->name()}}</h3>
            @foreach($thread->messages as $message)
            <div class="cursor-default message">
                <div class="fl img-section">
                    <div class="img-container img-container-curve small">
                        <img src="{{$message->sender->profile_pic}}" />
                    </div>
                    <input class="hidden" type="checkbox" />
                </div>
                <div class="fr msg-content">
                    <h5><a class="blue" href="{{$message->sender->profile_url}}">{{$message->sender->present()->name()}}</a>
                    </h5> 
                    <p>{{$message->text(1000)}}</p>
                    <div class="date"><i class="icon-calender2"></i> {{$message->created_at->diffForHumans()}}</div>
                </div>
                <div class="cf"></div>
            </div>
            @endforeach
            <div class="create message">
                <div class="fl img-section">
                    <div class="img-container img-container-curve small">
                        <img src="{{$current_user->profile_pic}}" />
                    </div>
                    <input class="hidden" type="checkbox" />
                </div>
                <div class="fr msg-content">
                    <form action="" method="post">
                        @if(Session::has('error'))
                        {{Session::get('error')}}
                        @endif
                        <input type="hidden" name="sender_id" value="{{$current_user->id}}">
                        <input type="hidden" name="receiver_id" value="{{$thread->other_user_id($current_user->id)}}">
                        <textarea name="message" placeholder="Write your reply"></textarea>
                        <div class="cf"></div>
                        <input type="submit" class="blue-button fr button" value="Send Your Reply">
                    </form>
                </div>
                <div class="cf"></div>
            </div>
        </div>
        <div class="inbox-actions hidden">
            <button class="red-button">Select All</button>
            <button class="red-button">Delete</button>
            <button class="red-button">Delete All</button>
        </div>
        <div class="cf"></div>
    </div>
    <div id="invitation-tab">

    </div>
    <div id="sent-tab">

    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(document).scrollTop($('.messages-container>.message:last-child').offset().top-200);
    });
</script>
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn','latest-news', 'mobile-app')))
@stop