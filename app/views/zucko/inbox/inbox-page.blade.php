@extends('layouts.main')
@section('window-title')
    {{trans("header.Messages")}} | @parent
@stop
@section('main-content')
<h2 class="title-stripped">
    <span class="text">{{trans("header.Message")}}</span>
</h2>
<div class="tabs-container" id="inbox-tabs">
    <ul class="tabs-menu">
        <li class="ui-tabs-active"><a class="tabs-menu-button" href="#message-tab">{{trans("header.Messages")}}</a></li>
        <li><a class="tabs-menu-button" href="{{url('inbox/invitations')}}">Invitations ({{count($invitations)}})</a></li>
        <li class=""><a class="tabs-menu-button" href="{{url('inbox/sent')}}">Sent Messages</a></li>
    </ul>
    <div class="tab-content" id="message-tab">
        <div class="blue-header-title">
        <form action="" autocomplete="off">
        <input value="{{Input::get('search')}}" type="text" name="search" class="input input-long" placeholder="{{trans("inbox.Search Messages")}}">
        <button onclick="top.location.reload()" class="red-button fr">{{trans("inbox.Refresh")}}</button>
        </form>
        </div>
        <form autocomplete="off" method="post" name="inbox-form" action="">
            <div id="" class="messages-container">
                @if(count($threads)<1)
                <div style="margin:10px 10px 0px 10px;" class="msg error">You dont have any ongoing conversations</div>
                @endif
                @foreach($threads as $thread)
                <div class="message {{$thread->lastMessage()->readBy($current_user->id)?'':'unread'}}">
                    <div class="fl img-section">
                        <div class="img-container img-container-curve small">
                            <img src="{{$thread->lastMessage()->sender->profile_pic}}" />
                        </div>
                        <input class="thread-selector" name="thread[]" value="{{$thread->id}}" type="checkbox" />
                    </div>
                    <div onclick="top.location.href='{{$thread->permalink}}'" class="cursor-pointer fr msg-content">
                        <h5><a href="{{$thread->permalink}}">"{{$thread->subject}}"</a>
                            <small>last message by <a class="blue" href="{{$thread->lastMessage()->sender->profile_url}}">{{$thread->lastMessage()->sender->present()->name()}}</a></small>
                        </h5> 
                        <p>{{$thread->lastMessage()->text(100)}}</p>
                        <div class="date"><i class="icon-calender2"></i> {{$thread->lastMessage()->created_at->diffForHumans()}}</div>
                    </div>
                    <div class="cf"></div>
                </div>
                @endforeach
                <input type="hidden" name="action" value=""/>
                <input type="hidden" name="box-type" value="received"/>
            </form>
        </div>
        {{$threads->links()}}
        <div class="inbox-actions">
            <button type="button" class="red-button select-all-button">{{trans("inbox.Select All")}}</button>
            <button type="button" class="red-button delete-button">{{trans("inbox.Delete")}}</button>
            <button type="button" class="red-button delete-all-button">{{trans("inbox.Delete All")}}</button>
        </div>
        <div class="cf"></div>
    </div>
    <div id="invitation-tab">

    </div>
    <div id="sent-tab">

    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(document).scrollTop($('#inbox-tabs').offset().top-100);
        $('.select-all-button').click(function(event) {
            var $this = $(event.target);
            if($this.hasClass('ulta')){
                $('input.thread-selector').prop('checked',false);
                $this.removeClass('ulta');
                return;
            }else{
                $('input.thread-selector').prop('checked',true);
                $this.addClass('ulta');
            }
        });
        $('.delete-button').click(function(event) {
            var checked = $('input.thread-selector:checked');
            if(checked.length<1){
                return;
            }
            var conf = confirm('Are you sure you want to delete these threads?');
            if(conf){
                $('input[name="action"]').val('delete');
                $('form[name="inbox-form"]').submit();
            }
        });
        $('.delete-all-button').click(function(event) {
            var conf = confirm('Are you sure you want to delete all threads?');
            if(conf){
                $('input[name="action"]').val('delete-all');
                $('form[name="inbox-form"]').submit();
            }
        });
    });
</script>
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn','latest-news', 'mobile-app')))
@stop