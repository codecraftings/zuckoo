@extends('layouts.main')
@section('window-title')
    {{trans("app.Login")}} | @parent
@stop
@section('main-content')
<h2 class="title-stripped">
    <span class="text">{{trans("app.Login")}}</span>
</h2>
<center>
    <p>
        {{trans("app.We are happy to see you back")}}!
    </p><br>
</center>
<div class="form-container">
    <div class="inside">
        <h3>
            {{trans("app.Login Here")}}
        </h3>
        @if(Session::has('info'))
        <div class="msg">{{Session::get('info')}}</div>
        @endif
        @if(Session::has('success'))
        <div class="msg success">{{Session::get('success')}}</div>
        @endif
        @if(Session::has('error'))
        <div class="msg error">{{Session::get('error')}}</div>
        @endif
        <form name="login-form" id="zuckologinform" method="post" action="{{url('login')}}" autocomplete="off">
            <div class="form-field half">
                <input class="input input-long" type="text" value="{{Session::get('email')}}" name="email" placeholder="Email" />
            </div>
            <div class="form-field half">
                <input class="input input-long" type="password" value="{{Session::get('password')}}" name="password" placeholder="Password" />
            </div>
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            @if(Session::has('pending_url'))
            <input type="hidden" name="pending_url" value="{{Session::get('pending_url')}}">
            @endif
            <div class="form-field half">
            <a href="{{url('password/reset')}}">{{trans("app.Forgot Password")}}?</a>
            </div>
            <div class="notice"></div>
            <input type="submit" name="submit" class="red-button fr" value="Login" />
            <div class="cf"></div>
        </form>
    </div>
</div>
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn', 'mobile-app')))
@stop