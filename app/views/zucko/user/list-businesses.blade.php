@extends('layouts.user-profile')
@section('window-title')
{{$user->present()->name()}} | Businesses | @parent
@stop
@section('right-sidebar')
@include('zucko.user.components.left-sidebar',array('user'=>$user))
@stop
@section('middle-content')
<div class="user-recent-feeds cf">
<h5 class="blue-header-title">Businesses Added By {{$user->present()->name()}}</h5>
	@include('zucko.business.components.list-users-businesses',array('businesses'=>$user->businesses()->paginate(Input::get('pagging',10))))
</div>
@stop
@section('left-sidebar')
@include('zucko.user.components.right-sidebar', array('user'=>$user))
@stop
