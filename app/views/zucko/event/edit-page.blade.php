@extends('layouts.main')
@section('window-title')
Edit | {{$event->title}} | @parent
@stop
@section('main-content')
<h2 class="title-stripped">
    <span class="text">Edit Event : {{$event->title}}</span>
</h2>
<div class="form-container">
    <div class="inside">
        <h3>
            Edit Event
        </h3>
        @if(Session::has('info'))
        <div class="msg">{{Session::get('info')}}</div>
        @endif
        @if(Session::has('success'))
        <div class="msg success">{{Session::get('success')}}</div>
        @endif
        @if(Session::has('error'))
        <div class="msg error">{{Session::get('error')}}</div>
        @endif
        <form method="post" autocomplete="off" enctype="multipart/form-data" action="">
            <div class="form-field half">
                <input class="input input-long" value="{{Session::get('title')?:$event->title}}" type="text" name="title" placeholder="Title" />
            </div>
            <div class="form-field half">
                When
                <div class="input-group-inline">
                    <div class="input-container">
                        <input value="{{Session::get('event_date')?:$event->event_time->format('d/m/Y')}}" type="text" name="event_date" class="input input-medium" placeholder="dd/mm/yyyy" />
                        <div class="input-addon"><i class="icon-calender"></i></div>
                    </div>
                    <select name="event_time" class="input input-small">
                        <option value="{{$time = $event->event_time->format('h:i A')}}">{{$time}}</option>
                        @for($h=1;$h<=23;$h++)
                        <option value="{{$t=$h%2?floor($h/2).':30':($h/2).':00'}} AM">{{$t}} AM</option>
                        @endfor
                        <option value="12:00 PM">12:00 PM</option>
                        <option value="12:30 PM">12:30 PM</option>
                        @for($h=2;$h<=23;$h++)
                        <option value="{{$t=$h%2?floor($h/2).':30':($h/2).':00'}} PM">{{$t}} PM</option>
                        @endfor
                        <option value="12:00 AM">12:00 AM</option>
                    </select>
                </div>
            </div>
            <div class="form-field half">
                <input value="{{Session::get('location')}}" class="input input-long" type="text" name="location" placeholder="Location" />
            </div>
            <div class="form-field half">
                <div class="input-container">
                    <input type="file" id="imgfileinp" name="photos[]" class="hidden" multiple/>
                    <input id="imgsellabel" type="text" class="input input-long" value="Upload Pictures" disabled placeholder="" />
                    <div class="input-addon"><button id="imgselectbtn" type="button" class="blue-button">Browse</button></div>
                </div>
            </div>
            <div class="form-field half">
                <input value="{{Session::get('link')}}" class="input input-long" type="text" name="link" placeholder="Link" />
            </div>
            <div class="form-field half">
                <input value="{{Session::get('video_url')}}" class="input input-long" type="text" name="video_url" placeholder="Video Url" />
            </div>
            <div class="form-field full">
                <textarea class="input input-xlong" name="description" placeholder="Description">{{Session::get('description')}}</textarea>
            </div>
            <div class="notice">*All Fields are required</div>
            <input type="submit" class="red-button" value="Add Event" />
            <div class="cf"></div>
        </form>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('#imgselectbtn').click(function(){
            $('#imgfileinp').click();
        });
        $('#imgfileinp').change(function(e){
            var files = e.target.files;
            $('#imgsellabel').val(files.length+" Pictures Selected");
        });
    });
</script>
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn', 'mobile-app')))
@stop