@extends('layouts.main')
@section('window-title')
{{trans("event.Add Events")}} | @parent
@stop
@section('footer-scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5y_HSH5nG4nXBclv13VrRZ5b81S5cRZc&amp;sensor=false"></script>
<script type="text/javascript" src="{{url('js/map.js')}}"></script>
@stop
@section('main-content')
<h2 class="title-stripped">
    <span class="text">{{trans("event.Add Events")}}</span>
</h2>
<p>
    {{trans("event.add_form1")}},
    <br>
    {{trans("event.add_form2")}}
</p><br>
<p>
    {{trans("event.add_form3")}}
</p><br>
<div class="form-container">
    <div class="inside">
        <h3>
            {{trans("event.Create Event")}}
        </h3>
        @if(Session::has('info'))
        <div class="msg">{{Session::get('info')}}</div>
        @endif
        @if(Session::has('success'))
        <div class="msg success">{{Session::get('success')}}</div>
        @endif
        @if(Session::has('error'))
        <div class="msg error">{{Session::get('error')}}</div>
        @endif
        <form method="post" autocomplete="off" enctype="multipart/form-data" action="">
            <div class="form-field half">
                <input class="input input-long" value="{{Session::get('title')}}" type="text" name="title" placeholder="{{trans("event.title")}}" />
            </div>
            <div class="form-field half">
                <small>{{trans("event.When")}}</small>
                <div class="input-group-inline">
                    <div class="input-container">
                        <input value="{{Session::get('event_date')}}" type="text" name="event_date" class="input input-medium datepicker" placeholder="dd/mm/yyyy" />
                        <div class="input-addon"><i class="icon-calender show-datepicker-btn"></i></div>
                    </div>
                    <select name="event_time" class="input input-small">
                        @for($h=1;$h<=23;$h++)
                        <option value="{{$t=$h%2?floor($h/2).':30':($h/2).':00'}} AM">{{$t}} AM</option>
                        @endfor
                        <option selected value="12:00 PM">12:00 PM</option>
                        <option value="12:30 PM">12:30 PM</option>
                        @for($h=2;$h<=23;$h++)
                        <option value="{{$t=$h%2?floor($h/2).':30':($h/2).':00'}} PM">{{$t}} PM</option>
                        @endfor
                        <option value="12:00 AM">12:00 AM</option>
                    </select>
                </div>
            </div>
            <div class="form-field half">
                <input value="{{Session::get('location')}}" class="input input-long" type="text" name="location" placeholder="{{trans("event.Location")}}" />
            </div>
            <div class="form-field half">
                <input value="{{Session::get('city')}}" class="input input-long" type="text" name="city" placeholder="{{trans("event.City")}}" />
            </div>
            <div class="form-field half">
                <input value="{{Session::get('link')}}" class="input input-long" type="text" name="link" placeholder="{{trans("event.Website Link")}}" />
            </div>
            <div class="form-field half">
                <div class="input-container">
                    <input type="file" id="imgfileinp" name="photos[]" class="hidden" multiple/>
                    <input id="imgsellabel" type="text" class="input input-long" value="{{trans("event.Upload Pictures")}}" disabled placeholder="" />
                    <div class="input-addon"><button id="imgselectbtn" type="button" class="blue-button">{{trans("event.Browse")}}</button></div>
                </div>
            </div>
            <div class="form-field half">
                <input value="{{Session::get('video_url')}}" class="input input-long" type="text" name="video_url" placeholder="{{trans("event.Video Url")}}" />
            </div>
            <div class="form-field full">
                <textarea class="input input-xlong" name="description" placeholder="{{trans("event.Description")}}">{{Session::get('description')}}</textarea>
            </div>
            <div style="margin-top: -18px; margin-right: 0px;" class="form-field full maps-container">
              <span class="label">Find your place and drop the pin:</span><br>
              <?php
              $latitude = Session::get('latitude')?:'-17.651409';
              $longitude = Session::get('longitude')?:'-149.425869';
              ?>
              <input type="hidden" id="latitude" name="latitude" value="{{$latitude}}" />
              <input type="hidden" id="longitude" name="longitude" value="{{$longitude}}" />  
              <div data="{{$latitude}},{{$longitude}}" id="map-canvas"></div>
              <small style="color:#404040;font-size:11px;margin:-18px 0px 8px;display:block;">Your gps position helps us to recommend your event to the users nearby</small>
          </div>
          <div class="notice">*{{trans("event.All Fields are required")}}</div>
          <input type="submit" class="red-button fr" value="{{trans("event.Add Event")}}" />
          <div class="cf"></div>
      </form>
  </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('#imgselectbtn').click(function(){
            $('#imgfileinp').click();
        });
        $('#imgfileinp').change(function(e){
            var files = e.target.files;
            $('#imgsellabel').val(files.length+" Pictures Selected");
        });
        $('.datepicker').datepicker({
            dateFormat: 'dd/mm/yy'
        });
        $('.show-datepicker-btn').click(function(event){
            $('.datepicker').datepicker('show');
        });
    });
</script>
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn', 'mobile-app')))
@stop