@extends('layouts.main')
@section('main-content')
@section('window-title')
{{$event->title}} | @parent
@stop
@section('footer-scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5y_HSH5nG4nXBclv13VrRZ5b81S5cRZc&amp;sensor=false"></script>
<script type="text/javascript">
	if($('#static-map')){
		google.maps.event.addDomListener(window, 'load', function(){
			var mapOptions = {
				zoom: 16,
				center: new google.maps.LatLng({{$event->latitude}},{{$event->longitude}}),
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};

			var map = new google.maps.Map(document.getElementById('static-map'),
				mapOptions);
			var marker = new google.maps.Marker({
				position: map.getCenter(),
				map: map,
				draggable: false,
				title: 'You Are Here'
			});
		});
	}
</script>
@stop
@section('main-content')
<div data-fullpage="true" data-event='{{$event->withUserInteractions(isset($current_user)?$current_user:false)->toJson(JSON_HEX_APOS|JSON_HEX_QUOT)}}' class="backbone-event-view event-details cf">
	<h2 class="title"><a href="{{$event->profile_ur}}">{{$event->present()->show_title()}}</a> </h2>
	@if($event->embedded_video)
	<div class="event-video">
		<iframe src="{{$event->embedded_video}}"></iframe>
	</div>
	@endif
	<p class="description">
		{{nl2br($event->present()->desc(2000))}}
	</p>
	<div class="event-metas">
		<span class="">Event Date: {{$event->event_time->format('l j F Y h:i A')}}</span>
		<span class="fr">{{$event->event_time->diffForHumans()}}</span>
		<div class="cf"></div>
		<span>Event Location: {{$event->present()->show_location(50)}}</span>
		<div class="cf"></div>
		<span>Event Website: <a href="{{$event->link}}">{{$event->link}}</a></span>
	</div><!--END interested-->
	<div class="photos">
		@include('common.small-slider',array('photos'=>$event->photos))
	</div>
	@if($event->latitude&&$event->longitude)
	<div class="geolocation">
		<div id="static-map" data=""></div>
	</div>
	@endif
</div><!--END event-->
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn', 'mobile-app')))
@stop