@extends('layouts.main')
@section('window-title')
Zucko On Mobile | @parent
@stop
@section('main-content')
<h2 class="title-stripped">
    <span class="text">Zucko On Mobile</span>
</h2>
<p>
    Soon available on google play and applestore
</p><br>
<!-- Main Content Here -->
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn','latest-news','mobile-app')))
@stop