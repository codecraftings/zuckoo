@extends('layouts.main')
@section('window-title')
    {{trans("header.contact_us")}} | @parent
@stop
@section('main-content')
<h2 class="title-stripped">
    <span class="text">{{trans("header.contact_us")}}</span>
</h2>
<!-- Main Content Here -->
<p>
    {{trans("lines.If you desire to contact us, or if you have a question or a suggestion, do not hesitate to contact by sending us an email through the forms below")}}!
</p><br>
<div class="form-container">
    <div class="inside">
        <h3>
            {{trans("app.Contact Form")}}
        </h3>
        @if(Session::has('info'))
        <div class="msg">{{Session::get('info')}}</div>
        @endif
        @if(Session::has('success'))
        <div class="msg success">{{Session::get('success')}}</div>
        @endif
        @if(Session::has('error'))
        <div class="msg error">{{Session::get('error')}}</div>
        @endif
        <form method="post" action="">
            <div class="form-field half">
                <input class="input input-long" type="text" value="{{Session::get('name')}}" name="name" placeholder="{{trans("app.Name")}}*" />
            </div>
            <div class="form-field half">
                <input class="input input-long" type="text" value="{{Session::get('email')}}" name="email" placeholder="Email*" />
            </div>
            <div class="form-field half">
                <input class="input input-long" type="text" value="{{Session::get('subject')}}" name="subject" placeholder="{{trans("app.Subject")}}*" />
            </div>
            <div class="form-field full">
                <textarea name="message" class="input input-xlong" style="height:100px;" placeholder="Message*">{{Session::get('message')}}</textarea>
            </div>
            {{Form::token()}}
            <div class="notice">*All Fields are required</div>
            <input type="submit" class="red-button fr" value="{{trans("app.Submit")}}" />
            <div class="cf"></div>
        </form>
    </div>
</div>
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn','latest-news','mobile-app')))
@stop