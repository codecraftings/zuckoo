@extends('layouts.main')
@section('window-title')
    {{trans("header.about_us")}} | @parent
@stop
@section('main-content')
<h2 class="title-stripped">
    <span class="text">{{trans("about.About")}} Zuckoo</span>
</h2>
<p>
</p>
<br>
<div class="box-content fl">
    <div class="icon-container fl"><i class="icon-about business"></i></div>
    <div class="contents fr">
        <h4>{{trans("about.Business Service")}}</h4>
        <p>{{trans("about.business_text")}}</p>
    </div>
</div>
<div class="box-content fr">
    <div class="icon-container fl"><i class="icon-about food"></i></div>
    <div class="contents fr">
        <h4>{{trans("about.Food")}}</h4>
        <p>{{trans("about.food_text")}}!</p>
    </div>
</div>
<div class="box-content fl">
    <div class="icon-container fl"><i class="icon-about auto"></i></div>
    <div class="contents fr">
        <h4>{{trans("about.Automotive")}}</h4>
        <p>{{trans("about.automotive_text")}}!</p>
    </div>
</div>
<div class="box-content fr">
    <div class="icon-container fl"><i class="icon-about health"></i></div>
    <div class="contents fr">
        <h4>{{trans("about.Health & Medicine")}}</h4>
        <p>{{trans("about.health_text")}}</p>
    </div>
</div>
<div class="box-content fl">
    <div class="icon-container fl"><i class="icon-about edu"></i></div>
    <div class="contents fr">
        <h4>{{trans("about.Education")}}</h4>
        <p>{{trans("about.education_text")}}</p>
    </div>
</div>
<div class="box-content fr">
    <div class="icon-container fl"><i class="icon-about it"></i></div>
    <div class="contents fr">
        <h4>{{trans("about.IT Service")}}</h4>
        <p>{{trans("about.it_text")}}</p>
    </div>
</div>
<div class="box-content fl">
    <div class="icon-container fl"><i class="icon-about retail"></i></div>
    <div class="contents fr">
        <h4>{{trans("about.Retail Shopping")}}</h4>
        <p>{{trans("about.shopping_text")}}</p>
    </div>
</div>
<div class="box-content fr">
    <div class="icon-container fl"><i class="icon-about travel"></i></div>
    <div class="contents fr">
        <h4>{{trans("about.Travel & Transport")}}</h4>
        <p>{{trans("about.travel_text")}}</p>
    </div>
</div>
<!-- Main Content Here -->
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn','latest-news','mobile-app')))
@stop