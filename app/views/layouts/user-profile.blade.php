<!DOCTYPE html>
<html>
<head>
	@section('html-header')
	@include('common.html-header')
	@show
</head>
<body>
	<header>
		@section('page-header')
		@include('common.page-header')
		@show
	</header>
	@include('zucko.search.components.wide-search-area')
	<div class="body-section">
		@section('body-top')
		@show
		<div class="row">
			<div class="container user_profile_page">
				<section id="user_profile">
					<div class="profile user-right-sidebar fl">
						@section('right-sidebar')
						@show
					</div>
					<div class="user-profile-middle fl">
						@section('middle-content')
						@show
					</div>
					<aside id="company_sidebar" class="user_sidebar right">
						@section('left-sidebar')
						@include('common.sidebar',array('widgets'=>array('add-btn','latest-news','mobile-app')))
						@show
					</aside>
					<div class="cf"></div>
				</section>
			</div>
		</div>
	</div>
	<footer>
		@section('footer')
		@include('common.footer')
		@show
	</footer>
</body>
</html>