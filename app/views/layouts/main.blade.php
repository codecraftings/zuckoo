<!DOCTYPE html>
<html>
<head>
	@section('html-header')
	@include('common.html-header')
	@show
</head>
<body class="{{$page_slag or 'general'}}">
	<header>
		@section('page-header')
		@include('common.page-header')
		@show
	</header>
	@include('zucko.search.components.wide-search-area')
	<div class="body-section">
		@yield('body-top')
		<div class="row">
		    <div class="container">
		        <div class="main-content fl">
		        @section('main-content')
		        @show
		        </div>
		        <div class="sidebar fr">
		        @section('sidebar')
		        @include('common.sidebar',array('widgets'=>array('add-btn','latest-news','mobile-app')))
		        @show
		        </div>
		        <div class="cf"></div>
		    </div>
		</div>
	</div>
	<footer>
		@section('footer')
		@include('common.footer')
		@show
	</footer>
</body>
</html>