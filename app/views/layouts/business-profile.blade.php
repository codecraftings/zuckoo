<!DOCTYPE html>
<html>
<head>
	@section('html-header')
	@include('common.html-header')
	@show
</head>
<body>
	<header>
		@section('page-header')
		@include('common.page-header')
		@show
	</header>
	@include('zucko.search.components.wide-search-area')
	<div class="body-section">
		@section('body-top')
		@show
		<div class="row">
		    <div class="container company-profile-page">
		        <div id="company_content" class="left">
		        @section('main-content')
		        @show
		        </div>
		        <div id="company_sidebar" class="user_sidebar right">
		        @section('sidebar')
		        @include('common.sidebar',array('widgets'=>array('add-btn','latest-news','mobile-app')))
		        @show
		        </div>
		        <div class="cf"></div>
		    </div>
		</div>
	</div>
	<footer>
		@section('footer')
		@include('common.footer')
		@show
	</footer>
</body>
</html>