<meta charset="utf-8">
<title>
@section('window-title')
Zuckoo | Admin Panel
@show
</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes"> 

<link href="{{url('admin/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('admin/css/bootstrap-responsive.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('admin/css/font-awesome.css')}}" rel="stylesheet">

<link href="{{url('admin/css/style.css')}}" rel="stylesheet" type="text/css">
<link href="{{url('admin/css/pages/signin.css')}}" rel="stylesheet" type="text/css">
<link href="{{url('admin/css/pages/dashboard.css')}}" rel="stylesheet" type="text/css">
<script src="{{url('admin/js/jquery-1.7.2.min.js')}}"></script>
<script src="{{url('admin/js/bootstrap.js')}}"></script>

<script src="{{url('admin/js/signin.js')}}"></script>