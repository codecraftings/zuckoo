@extends('admin.layouts.master')
@section('body')
@include('admin.common.top-menu',array('page_id'=>'reviews'))
<div class="account-container big">
	
	<div class="content clearfix">
		{{Form::model($review, array('enctype'=>"multipart/form-data", 'autocomplete'=>"off", 'method'=>'post'))}}

		<div class="form-fields">
			<h1>Edit: {{$review->present()->title()}}</h1>	
			@if(Session::has('error'))
			<div class="alert alert-error">
				{{Session::get('error')}}
			</div>
			@endif
			@if(Session::has('success'))
			<div class="alert alert-success">
				{{Session::get('success')}}
			</div>
			@endif
			{{Form::token()}}
			<div class="field clearfix">
				{{Form::label('rating','Rating Score')}}
				{{Form::text('rating', null, array('required'=>'true', 'placeholder'=>'Rating Score', 'class'=>'input'))}}
			</div>
			<div class="field clearfix">
				{{Form::label('description','Review Text')}}
				{{Form::textarea('description', null, array('required'=>'true', 'placeholder'=>'Review Text', 'class'=>'input'))}}
			</div>
		</div> <!-- /login-fields -->

			<div class="actions clearfix">

				<button class="button btn btn-success btn-large">Submit</button>

			</div> <!-- .actions -->



			{{Form::close()}}

		</div> <!-- /content -->

	</div>
	@stop