@extends('admin.layouts.master')
@section('body')
@include('admin.common.top-menu',array('page_id'=>'users'))
<div class="account-container big">
	
	<div class="content clearfix">
		{{Form::model($user, array('enctype'=>"multipart/form-data", 'autocomplete'=>"off", 'method'=>'post'))}}

		<div class="form-fields">
			<h1>Edit: {{$user->present()->name()}}</h1>	
			@if(Session::has('error'))
			<div class="alert alert-error">
				{{Session::get('error')}}
			</div>
			@endif
			@if(Session::has('success'))
			<div class="alert alert-success">
				{{Session::get('success')}}
			</div>
			@endif
			{{Form::token()}}
			<div class="field clearfix">
				{{Form::label('first_name','First Name')}}
				{{Form::text('first_name', null, array('required'=>'true', 'placeholder'=>'First Name', 'class'=>'input'))}}
			</div>
			<div class="field clearfix">
				{{Form::label('last_name','Last Name')}}
				{{Form::text('last_name', null, array('required'=>'true', 'placeholder'=>'Last Name', 'class'=>'input'))}}
			</div>
			<div class="field clearfix">
				{{Form::label('email','Email')}}
				{{Form::email('email', null, array('required'=>'true', 'placeholder'=>'Email', 'class'=>'input'))}}
			</div>
			<div class="field clearfix">
				{{Form::label('nickname','Nickname')}}
				{{Form::text('nickname', null, array('required'=>'true', 'placeholder'=>'Nickname', 'class'=>'input'))}}
			</div>
			<div class="field clearfix">
				{{Form::label('city_name','City')}}
				{{Form::text('city_name', null, array('required'=>'true', 'placeholder'=>'City', 'class'=>'input'))}}
			</div>
			<div class="field clearfix">
				{{Form::label('gender','Gender')}}
				{{Form::select('gender', array(
					'MALE' => 'MALE',
					'FEMALE' => 'FEMALE'
				),null, array('class'=>'input'))}}
			</div>
			<div class="field clearfix">
				{{--Form::label('role_id','Role')--}}
				{{--Form::select('role_id', array(
					Zucko\User\Role::USER => 'User'
				),null, array('class'=>'input'))--}}
			</div>
		</div> <!-- /login-fields -->

		<div class="actions clearfix">

			<button class="button btn btn-success btn-large">Submit</button>

		</div> <!-- .actions -->



		{{Form::close()}}

	</div> <!-- /content -->

</div>
@stop