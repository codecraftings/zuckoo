<?php include('header.php');?>
<?php $page_id="apps"; include('top-menu.php');?>
<?php
$apptypes = AppType::query()->orderBy('name','asc')->get();
?>
<div class="main">
	<div class="main-inner">
		<div class="container">
			<div class="row">
				<div class="span10">
					<div class="widget widget-table action-table">
							<?php if(Session::has('error')){?>
								<div class="alert alert-error">
								<?php echo Session::get('error');?>
								</div>
							<?php }?>
							<?php if(Session::has('success')){?>
								<div class="alert alert-success">
								<?php echo Session::get('success');?>
								</div>
							<?php }?>
						<?php if(Input::has('edit')){
							$type = AppType::find(Input::get('edit'));
							if($type){
							?>
						<h4>Update Type: <?php echo $type->name;?></h4>
						<form autocomplete="off" style="margin-top:15px;" id="editform" class="form-inline" method="post" action="">
						<input type="hidden" name="_token" value="<?php echo csrf_token();?>">
						<label for="typename">Name: </label>
						<input class="input-medium" id="typename" name="typename" value="<?php echo $type->name;?>">
						<label for="typeslag">Slag: </label>
						<input class="input-medium" id="typeslag" name="typeslag" value="<?php echo $type->slag;?>"/>
						<input type="submit" name="submit" value="Update" class="btn btn-primary"/>
						<a href="<?php echo Website::admin_url().'apptypes';?>" class="btn btn-cancel">Cancel</a>
						</form>
						<?php }}?>
						<h4>Create New Type</h4>
						<form class="form-inline" style="margin-top:15px;" method="post" action="<?php echo Website::admin_url().'apptypes?new=1';?>" autocomplete="off" style="">
							<input type="hidden" name="_token" value="<?php echo csrf_token();?>">
							<label for="typename">Name: </label>
							<input class="input-medium" id="typename" name="typename" value="">
							<label for="typeslag">Slag: </label>
							<input class="input-medium" id="typeslag" name="typeslag" value=""/>
							<input type="submit" name="submit" value="Submit" class="btn btn-primary"/>
						</form>
						<div class="widget-header"> <i class="icon-th-list"></i>
							<h3>List of Apps Types</h3>
						</div>
						<!-- /widget-header -->
						<div class="widget-content">
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>Type Name</th>
										<th>Total App</th>
										<th class="td-actions"> </th>
									</tr>
								</thead>
								<tbody>
									<?php
									foreach($apptypes as $apptype){
									?>
									<tr>
										<td><?php echo $apptype->name;?></td>
										<td><a href="<?php echo Website::admin_url().'list-apps?apptype='.$apptype->id;?>"><?php echo $apptype->apps()->count();?></a></td>
										<td class="td-actions">
											<a target="_blank" href="<?php echo Website::base_url().'apps/'.$apptype->slag;?>" class="btn btn-small btn-success">
											<i class="btn-icon-only icon-link"> </i>
											</a>
											<a href="<?php echo Website::admin_url().'apptypes?edit='.$apptype->id;?>" class="btn btn-small btn-primary">
												<i class="btn-icon-only icon-pencil"> </i>
											</a>
											<a href="javascript:;" onclick="if(confirm('Do you really want to delete this type?')){top.location.href='<?php echo Website::admin_url().'apptypes?delete='.$apptype->id;?>';}" class="btn btn-danger btn-small">
												<i class="btn-icon-only icon-trash"> </i>
											</a>
										</td>
									</tr>
									<?php }?>
								</tbody>
								<tfoot>
								</tfoot>
							</table>
						</div>
						<!-- /widget-content --> 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /widget --> 
<?php include('footer.php');?>