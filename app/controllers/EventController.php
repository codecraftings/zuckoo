<?php
use Zucko\Business\Repos\BusinessRepo;
use Zucko\User\Services\Session\SessionService;
use Zucko\Address\Repos\AddressRepo;
use Zucko\Photo\Repos\PhotoRepo;
use Zucko\Event\Repos\EventRepo;
use Zucko\Services\File\Uploader;
use Carbon\Carbon;
use Zucko\Services\Api\Api;
use Zucko\Services\Api\API_ERROR;
class EventController extends BaseController{
	protected $business;
	protected $session;
	protected $event;
	protected $photo;
	protected $uploader;
	protected $carbon;
	protected $api;
	public function __construct(Api $api, BusinessRepo $business, SessionService $session, AddressRepo $address, PhotoRepo $photo, EventRepo $event, Uploader $uploader, Carbon $carbon){
		$this->business = $business;
		$this->session = $session;
		$this->event = $event;
		$this->photo = $photo;
		$this->uploader = $uploader;
		$this->carbon = $carbon;
		$this->api = $api;
		parent::__construct($session, $address);
	}
	public function ajaxAddInterestedUser(){
		$event = $this->event->get(Input::get('event_id'));
		if(!$event){
			return $this->api->error(API_ERROR::msg(API_ERROR::UNKNOWN_ERROR));
		}
		$user = $this->session->getCurrentUser();
		$event = $this->event->addInterestedUser($event, $user->id);
		if(!$event){
			return $this->api->error($this->event->errors()->first());
		}
		return $this->api->success(array('interested_count'=>$event->interested_count));
	}
	public function ajaxDeleteInterestedUser(){
		$event = $this->event->get(Input::get('event_id'));
		if(!$event){
			return api_error();
		}
		$user = $this->session->getCurrentUser();
		$event = $this->event->removeInterestedUser($event, $user->id);
		if(!$event){
			return api_error($this->event->errors()->first());
		}
		return $this->api->success(array('interested_count'=>$event->interested_count));
	}
	public function listEvents(){
		$query = new stdClass();
		$query->name = Input::get('name');
		$query->place_name = Input::get('place');
		$query->pagging = Input::get('pagging',10);
		$query->date = Input::get('date');
		$filters = array();
		if(!empty($query->place_name)){
			$filters['place_name'] = $query->place_name;
		}
		if(Input::has('city_id')){
			$filters['city_id'] = Input::get('city_id');
		}
		if($query->date){
			switch ($query->date) {
				case 'today':
				$filters['day1'] = $this->carbon->today()->startOfDay();
				$filters['day2'] = $this->carbon->today()->endOfDay();
				break;
				case 'tomorrow':
				$filters['day1'] = $this->carbon->tomorrow()->startOfDay();
				$filters['day2'] = $this->carbon->tomorrow()->endOfDay();
				break;
				case 'weekend':
				if($this->carbon->now()->dayOfWeek==Carbon::SUNDAY){
					$filters['day1'] = $this->carbon->today()->startOfDay();
					$filters['day2'] = $this->carbon->today()->endOfDay();
				}
				else{	
					$filters['day1'] = $this->carbon->today()->next(Carbon::SUNDAY)->startOfDay();
					$filters['day2'] = $this->carbon->today()->next(Carbon::SUNDAY)->endOfDay();
				}
				break;
				case 'this_week':
				$filters['day1'] = $this->carbon->now()->startOfWeek();
				$filters['day2'] = $this->carbon->now()->endOfWeek();
				break;
				case 'next_week':
				$filters['day1'] = $this->carbon->today()->addDays(7)->startOfWeek();
				$filters['day2'] = $this->carbon->today()->addDays(7)->endOfWeek();
				break;
				case 'custom':
				$filters['day1'] = $this->carbon->createFromFormat('d/m/Y',Input::get('custom_date'))->startOfDay();
				$filters['day2'] = $this->carbon->createFromFormat('d/m/Y',Input::get('custom_date'))->endOfDay();
					//dd($filters);
				break;
				default:
					# code...
				break;
			}
		}
		$events = $this->event->search($query->name, $filters, $query->pagging, Input::get('page',1));
		$data = array();
		$data['events'] = $events;
		$data['event_query'] = $query;
		return View::make('zucko.event.listing-page')->with($data);
	}
	public function showEvent($id, $event=""){
		$event = $this->event->get($id);
		if(!$event){
			return Redirect::to('/');
		}
		return View::make('zucko.event.profile-page')->withEvent($event);
	}
	public function showCityEvents($city){
		$city = $this->address->getCityBySlag($city);
		return Redirect::to('events?place='.urlencode($city->name));
	}
	public function showAdd(){
		return View::make('zucko.event.add-page');
	}
	public function submitAdd(){
		$data = Input::except('photos');
		//dd($data);
		try{
			$data['event_time'] = $this->carbon->createFromFormat('d/m/Y h:i A', $data['event_date'].' '.$data['event_time']);
		}
		catch(Exception $e){
			$data['error'] = "This is not valid date format.";
			return Redirect::back()->with($data);
		}
		$user = $this->session->getCurrentUser();
		$data['user_id'] = $user->id;
		if(!($city = $this->address->getCityByName($data['city']))){
			$city = $this->address->saveCity($data['city']);
		}
		$data['city_id'] = $city->id;
		$data['location'] = $data['location'].", ".$data['city'];
		$event = $this->event->create($data);
		$this->address->updateCity($city->id);
		if(!$event){
			$data['error'] = $this->event->errors()->first();
			return Redirect::back()->with($data);
		}
		if(Input::hasFile('photos')){
			$photos = $this->photo->createFromUpload(Input::file('photos'), $event->id, 'Zucko\Event\Event', $user->id, $event->title);
			if(!$photos){
				$data['error'] = $this->photo->errors()->first()?:"Something went wrong";
				return Redirect::back()->with($data);
			}
		}
		return Redirect::to($event->profile_url);
	}
	public function showEditPage(){

	}
	public function submitEditPage(){
		
	}
}