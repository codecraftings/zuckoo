<?php
use Zucko\Business\Repos\BusinessRepo;
use Zucko\Review\Repos\ReviewRepo;
use Zucko\Event\Repos\EventRepo;
use Zucko\User\Services\Session\SessionService;
use Zucko\Services\Mail\Mail;
use Zucko\Address\Repos\AddressRepo;
class PagesController extends BaseController {
	protected $business;
	protected $review;
	protected $event;
	protected $mail;
	public function __construct(BusinessRepo $business, EventRepo $event, ReviewRepo $review, SessionService $session, Mail $mail, AddressRepo $address){
		$this->business = $business;
		$this->review = $review;
		$this->event = $event;
		$this->mail = $mail;
		parent::__construct($session, $address);
	}
	public function showHome()
	{
		View::share('categories',$this->business->getCategories());
		View::share('recent_reviews',$this->review->getRecents(3));
		View::share('top_reviews',$this->review->getTop(3));
		View::share('top_events',$this->event->getPopular('',3));
		View::share('latest_events', $this->event->getRecent('',2));
		View::share('popular_businesses',$this->business->getPopularBusinesses('city', 12));
		return View::make('zucko.home');
	}
	public function showCities(){
		$q = Input::get('q');
		if(empty($q)){
			View::share('cities', $this->address->getPopularCities());
		}else{
			View::share('q', $q);
			View::share('cities', $this->address->getCitiesByName($q));
		}
		return View::make('zucko.search.list-cities-page');
	}
	public function showAboutPage(){
		return View::make('pages.about-us');
	}
	public function showTermsPage(){
		return View::make('pages.terms');
	}
	public function showPrivacyPage(){
		return View::make('pages.privacy');
	}
	public function showCareerPage(){
		return View::make('pages.career');
	}
	public function showMobilePage(){
		return View::make('pages.mobile');
	}
	public function showContactPage(){
		return View::make('pages.contact-us');
	}
	public function submitContactPage(){
		$data = Input::all();
		$attempt = $this->mail->sendContactEmail($data['name'],$data['email'], $data['subject'],$data['message']);
		if(!$attempt){
			$data['error'] = $this->mail->errors()->first();
			//dd($data);
			return Redirect::back()->with($data);
		}else{
			return Redirect::back()->withSuccess("Your message sent successfully to admin. We will get back to you shortly");
		}
	}
}
