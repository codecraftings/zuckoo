<?php
use Zucko\User\Services\Session\SessionService;
use Zucko\User\Repos\UserRepo;
use Zucko\User\Repos\FriendsRepo;
use Zucko\Inbox\Repos\InboxRepo;
use Zucko\Services\File\Uploader;
use Carbon\Carbon;
use Zucko\Address\Repos\AddressRepo;
class InboxController extends BaseController{
	protected $user;
	protected $inbox;
	protected $friends;
	protected $uploader;
	protected $carbon;
	public function __construct(UserRepo $user, InboxRepo $inbox, FriendsRepo $friends, SessionService $session, Uploader $uploader, Carbon $carbon, AddressRepo $address){
		$this->user = $user;
		$this->friends = $friends;
		$this->inbox = $inbox;
		$this->uploader = $uploader;
		$this->carbon = $carbon;
		parent::__construct($session, $address);
	}
	public function showInboxPage(){
		$user = $this->session->getCurrentUser();
		$invitations = $this->friends->getInvitations($user);
		$threads = $this->inbox->getThreads($user->id, Input::get('pagging',20), Input::get('search'), 'received');
		$data = array();
		$data['threads'] = $threads;
		$data['invitations'] = $invitations;
		return View::make('zucko.inbox.inbox-page')->with($data);
	}
	public function submitInboxAction(){
		$action = Input::get('action');
		$user = $this->session->getCurrentUser();
		switch ($action) {
			case 'delete':
			$threads = Input::get('thread');
			foreach ($threads as $key => $thread) {
				$thread = $this->inbox->getThread($thread);
				if($thread){
					$this->inbox->deleteThread($user->id, $thread);
				}
			}
			break;
			case 'delete-all':
			$threads = $this->inbox->getThreads($user->id, 0, '', Input::get('box-type','both'))->get();
			foreach ($threads as $key => $thread) {
				$this->inbox->deleteThread($user->id, $thread);
			}
			break;
			default:
				# code...
			break;
		}
		return Redirect::back();
	}
	public function showInvitationsPage(){
		$user = $this->session->getCurrentUser();
		$invitations = $this->friends->getInvitations($user);
		$data['invitations'] = $invitations;
		return View::make('zucko.inbox.invitations-page')->with($data);
	}
	public function showSentPage(){
		$user = $this->session->getCurrentUser();
		$invitations = $this->friends->getInvitations($user);
		$threads = $this->inbox->getThreads($user->id, Input::get('pagging',20), Input::get('search'), 'sent');
		$data = array();
		$data['threads'] = $threads;
		$data['invitations'] = $invitations;
		return View::make('zucko.inbox.sent-inbox-page')->with($data);
	}
	public function showCompose(){
		return View::make('zucko.inbox.compose-page');
	}
	public function submitCompose(){
		$receiver = $this->user->getByNick(Input::get('user'));
		$data = Input::all();
		$user = $this->session->getCurrentUser();
		if(!$receiver||!$user||$receiver->id==$user->id){
			$data['error'] = "That user does not exist";
			return Redirect::back()->with($data);
		}
		$thread = $this->inbox->createThread(array(
			'user1_id' => $user->id,
			'user2_id' => $receiver->id,
			'subject' => $data['subject']
			));
		if(!$thread){
			$data['error'] = $this->inbox->errors()->first();
			return Redirect::back()->with($data);
		}
		$message = $this->inbox->createMessage(array(
			'sender_id' => $user->id,
			'receiver_id' => $receiver->id,
			'message' => $data['message'],
			'thread_id' => $thread->id
			));
		if(!$message){
			$data['error'] = $this->inbox->errors()->first();
			return Redirect::back()->with($data);
		}
		return Redirect::back()->withSuccess('Your message sent successfully');
	}
	public function showThreadPage($id){
		$thread = $this->inbox->getThread($id);
		if(!$thread){
			return Redirect::to('/');
		}
		$user = $this->session->getCurrentUser();
		$invitations = $this->friends->getInvitations($user);
		View::share('invitations',$invitations);
		if(!$this->inbox->accessibleBy($user->id, $thread)){
			return Redirect::to('/');
		}
		$this->inbox->changeReadStatusThread($thread->id, $user->id);
		return View::make('zucko.inbox.thread-page')->withThread($thread);
	}
	public function submitReply($id){
		$thread = $this->inbox->getThread($id);
		if(!$thread){
			return Redirect::to('/');
		}
		$user = $this->session->getCurrentUser();
		$data = Input::only('sender_id','receiver_id','message');
		$data['sender_id'] = $user->id;
		$data['thread_id'] = $thread->id;
		if(!$this->inbox->accessibleBy($data['sender_id'], $thread)||!$this->inbox->accessibleBy($data['receiver_id'], $thread)){
			return Redirect::to('/');
		}
		$message = $this->inbox->createMessage($data);
		if(!$message){
			$data['error'] = $this->inbox->errors()->first();
			return Redirect::back()->with($data);
		}
		return Redirect::back();
	}
}