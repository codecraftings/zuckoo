<?php
use Zucko\Business\Repos\BusinessRepo;
use Zucko\User\Services\Session\SessionService;
use Zucko\Address\Repos\AddressRepo;
use Zucko\Photo\Repos\PhotoRepo;
use Zucko\Services\File\Uploader;
class BusinessController extends BaseController{
	protected $business;
	protected $photo;
	protected $uploader;
	public function __construct(BusinessRepo $business, SessionService $session, AddressRepo $address, PhotoRepo $photo, Uploader $uploader){
		$this->business = $business;
		$this->photo = $photo;
		$this->uploader = $uploader;
		parent::__construct($session, $address);
		View::share('countries',$this->address->getAllCountries());
		View::share('states',$this->address->getAllStates());
		View::share('categories',$this->business->getCategories());
	}
	public function addPhotosByUser(){
		$business = $this->business->get(Input::get('business_id'));
		$user = $this->session->getCurrentUser();
		if(!$business){
			return Redirect::to('/');
		}
		if(Input::hasFile('photos')){
			//dd(Input::file('pictures'));
			$photos = $this->photo->createFromUpload(Input::file('photos'), $business->id, 'Zucko\Business\Business', $user->id, $business->name);
			if(!$photos){
				$error = $this->photo->errors()->first()?:"Something went wrong";
				return Redirect::back()->withError($error);
			}
		}
		return Redirect::to($business->profile_url);
	}
	public function showEditPage($id){
		$business = $this->business->get($id);
		if(!$business){
			return Redirect::to('/');
		}
		$user = $this->session->getCurrentUser();
		if($business->user_id!=$user->id){
			return Redirect::to('/');
		}
		//dd($business->meta);
		return View::make('zucko.business.edit-page')->withBusiness($business);
	}
	public function submitEditPage($id){
		//dd(Input::all());
		$business = $this->business->get($id);
		if(!$business){
			return Redirect::to('/');
		}
		$user = $this->session->getCurrentUser();
		if($business->user_id!=$user->id){
			return Redirect::to('/');
		}
		$oldphotos = Input::get('oldphotos');
		foreach ($business->photos as $key => $photo) {
			if(!$oldphotos||!in_array($photo->id, $oldphotos)){
				$this->business->removePhoto($business, $photo);
			}
		}
		$data = Input::except('newphoto','_token','logo');
		if(Input::hasFile('newphoto')){
			$photos = $this->photo->createFromUpload(Input::file('newphoto'), $business->id, 'Zucko\Business\Business', $user->id, $business->name);
			if(!$photos){
				$data['error'] = $this->photo->errors()->first()?:"Something went wrong";
				return Redirect::back()->with($data);
			}
		}
		if(Input::hasFile('logo')){
			$photos = $this->photo->createFromUpload(Input::file('logo'), $business->id, 'Zucko\Business\Business', $user->id, $business->name, 400, 160);
			if(!$photos){
				$data['error'] = $this->photo->errors()->first()?:"Something went wrong";
				return Redirect::back()->with($data);
			}
			$data['logo'] = $photos[0]->id;
		}
		if(!($city = $this->address->getCityByName($data['city']))){
			$city = $this->address->saveCity($data['city']);
		}
		$data['city_id'] = $city->id;
		$opening_hours = Input::get('day1');
		foreach ($opening_hours as $key => &$hour) {
			$hour = new stdClass();
			$hour->day1 = Input::get('day1')[$key];
			$hour->day2 = Input::get('day2')[$key];
			$hour->time1 = Input::get('time1')[$key];
			$hour->time2 = Input::get('time2')[$key];
		}
		$data['opening_hours'] = $opening_hours;
		if(!($business = $this->business->update($business, $data))){
			$data['error'] = $this->business->errors()->first();
			return Redirect::back()->with($data);
		}
		return Redirect::back()->withSuccess('Your business informations updated successfully.');
	}
	public function showProfile($id, $name=""){
		$business = $this->business->get($id);
		if(!$business){
			return Redirect::to('/');
		}
		$user = $this->session->getCurrentUser();
		if(!$business->is_published()&&(!$user||$user->id!=$business->user_id)){
			return Redirect::to('/');
		}
		$business->view_count += 1;
		$business->save();
		View::share('related_businesses',$this->business->getPopularBusinesses('city', 4));
		return View::make('zucko.business.profile-page')->withBusiness($business);
	}
	public function showReviews($id, $name=""){
		$business = $this->business->get($id);
		if(!$business){
			return Redirect::to('/');
		}
		$data = array();
		if($this->session->isLoggedIn()){	
		$myreview = $business->reviews()->where('user_id','=', $this->session->getCurrentUser()->id)->first();
		$data['myreview'] = $myreview;
		}

		$data['business'] = $business;
		return View::make('zucko.business.reviews-page')->with($data);
	}
	public function showSearchPage(){
		$q = Input::get('find');
		$where = Input::get('from');
		$filters = array();
		$query = new stdClass();
		$query->sorting = Input::get('sorting','match');
		$query->distance = Input::get('distance','none');
		$query->page = Input::get('page',1);
		$query->pagging = Input::get('pagging', 10);
		$query->features = Input::get('features',array());
		$query->prices = Input::get('prices',array());
		$query->cats = Input::get('cats',array());
		$query->food_type = Input::get('food_type', array());
		$query->page = Input::get('page',1);
		$filters['sorting'] = $query->sorting;
		$filters['category_ids'] = array();
		$filters['food_type'] = array();
		if(Input::has('features')){
			$filters['features'] = $query->features;
		}
		if(Input::has('time')){
			$filters['time'] = Input::get('time');
		}
		if(Input::has('prices')){
			$filters['prices'] = $query->prices;
		}
		foreach ($query->cats as $key => $cat) {
			if(starts_with($cat, 'ft-')){
				array_push($filters['food_type'], $cat);
				continue;
			}
			$cat = $this->business->getCategoryBySlag($cat);
			array_push($filters['category_ids'], $cat->id);
		}
		$data = array();
		$result = $this->business->search($q, $where, $filters,$query->page, $query->pagging);
		if(!Input::has('features')&&!Input::has('prices')){
			$result = $this->business->appendBals($q, $where, $result, $filters);
		}
		$data['businesses'] = Paginator::make($result->items, $result->total_items, $result->pagging);
		$data['query'] = $query;
		//dd($data);
		return View::make('zucko.search.results-page')->with($data);
	}
	public function showAddOne(){
		return View::make('zucko.business.add-page1');
	}
	public function submitAddOne(){
		$data = Input::except('logo','_token');
		if(!($city = $this->address->getCityByName($data['city']))){
			$city = $this->address->saveCity($data['city']);
		}
		$data['city_id'] = $city->id;
		$user = $this->session->getCurrentUser();
		$data['user_id'] = $user->id;
		$business = $this->business->create($data);
		if(!$business){
			$data['error'] = $this->business->errors()->first();
			return Redirect::back()->with($data);
		}
		if(Input::hasFile('logo')){
			$photos = $this->photo->createFromUpload(Input::file('logo'), $business->id, 'Zucko\Business\Business', $user->id, $business->name, 400, 160);
			if($photos){
				$this->business->update($business, array(
						'logo' => $photos[0]->id
					));
			}
		}
		return Redirect::to('business/add/more-info/'.$business->id);
	}
	public function showAddSecond($id){
		$business = $this->business->get($id);
		//dd($business);
		if(!$business){
			return Redirect::to('business/add');
		}
		return View::make('zucko.business.add-page2')->withBusiness($business);
	}
	public function submitAddSecond($id){
		$business = $this->business->get($id);
		//dd($business);
		if(!$business){
			return Redirect::to('business/add');
		}
		$data = Input::except('pictures');
		$opening_hours = Input::get('day1');
		foreach ($opening_hours as $key => &$hour) {
			$hour = new stdClass();
			$hour->day1 = Input::get('day1')[$key];
			$hour->day2 = Input::get('day2')[$key];
			$hour->time1 = Input::get('time1')[$key];
			$hour->time2 = Input::get('time2')[$key];
		}
		$data['opening_hours'] = $opening_hours;
		if(!($business = $this->business->update($business, $data))){
			$data['error'] = $this->business->errors()->first();
			return Redirect::back()->with($data);
		}
		$user = $this->session->getCurrentUser();
		if(Input::hasFile('pictures')){
			$photos = $this->photo->createFromUpload(Input::file('pictures'), $business->id, 'Zucko\Business\Business', $user->id, $business->name);
			if(!$photos){
				$data['error'] = $this->photo->errors()->first()?:"Something went wrong";
				return Redirect::back()->with($data);
			}
		}
		return Redirect::to($business->profile_url);
	}
}