<?php
use Zucko\User\Services\Session\SessionService;
use Zucko\Address\Repos\AddressRepo;
class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected $session;
	protected $address;
	public function __construct(SessionService $session, AddressRepo $address){
		$this->session = $session;
		$this->address = $address;
		View::share('top_cities',$this->address->getPopularCities(6));
		if($this->session->isLoggedIn()){
			View::share('current_user',$this->session->getCurrentUser());
		}
	}
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
