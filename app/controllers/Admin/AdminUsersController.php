<?php
use Zucko\Admin\User\Services\Session\SessionService;
use Zucko\User\Repos\UserRepo;
class AdminUsersController extends AdminController{
	protected $user;
	public function __construct(SessionService $session, UserRepo  $user){
		$this->user = $user;
		parent::__construct($session, $user);
	}
	public function showListUsers(){
		$filters = array();
		$filters['sort'] = Input::get('sort','new');
		$users = $this->user->search(Input::get('search'),$filters,Input::get('pagging',100),Input::get('page',1));
		return View::make('admin.zucko.user.list-users')->withUsers($users);
	}
	public function showEditUser($id){
		$user = $this->user->get($id);
		if(!$user){
			return Redirect::to(admin_url('/'));
		}
		return View::make('admin.zucko.user.edit-user')->withUser($user);
	}
	public function submitEditUser($id){
		$user = $this->user->get($id);
		if(!$user){
			return Redirect::to(admin_url('/'));
		}
		$data = Input::except('_token');
		$user = $this->user->update($user, $data);
		if(!$user){
			$error = $this->user->errors()->first();
			return Redirect::back()->withError($error)->withInput($data);
		}
		if($this->session->isAdmin()&&isset($data['role_id'])){
			$this->user->changeRole($user, $data['role_id']);
		}
		return Redirect::back()->withSuccess('User details updated successfully');
	}
	public function deleteUser($id){
		$this->user->delete($id);
		return Redirect::back();
	}
}