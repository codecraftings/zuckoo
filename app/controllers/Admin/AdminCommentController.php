<?php
use Zucko\Admin\User\Services\Session\SessionService;
use Zucko\Business\Repos\BusinessRepo;
use Zucko\Address\Repos\AddressRepo;
use Zucko\Comment\Repos\CommentRepo;
use Zucko\User\Repos\UserRepo;
use Zucko\Review\Repos\ReviewRepo;
class AdminCommentController extends AdminController{
	protected $business;
	protected $address;
	protected $comment;
	public function __construct(SessionService $session, ReviewRepo $review, CommentRepo $comment, BusinessRepo  $business, AddressRepo $address, UserRepo $user){
		$this->business = $business;
		$this->address = $address;
		$this->comment = $comment;
		$this->review = $review;
		parent::__construct($session, $user);
	}
	public function showListPage(){
		$filters = array();
		$filters['sort'] = Input::get('sort','new');
		$comments = $this->comment->search(Input::get('search'),$filters,Input::get('pagging',100),Input::get('page',1));
		return View::make('admin.zucko.comment.list-comments')->withComments($comments);
	}
	public function showEditPage($id){
		$comment = $this->comment->get($id);
		if(!$comment){
			return Redirect::back();
		}
		return View::make('admin.zucko.comment.edit-comment')->withComment($comment);
	}
	public function submitEditPage($id){
		$comment = $this->comment->get($id);
		if(!$comment){
			return Redirect::back();
		}
		$data = Input::except('_token');
		$comment = $this->comment->update($comment, $data);
		if(!$comment){
			return Redirect::back()->withError($this->comment->errors()->first())->withInput($data);
		}
		return Redirect::back()->withSuccess("Updated successfully!");
	}
	public function deleteComment($id){
		$this->comment->delete($id);
		return Redirect::back();
	}
}