<?php
namespace Zucko\Photo\Services\Validator;
use Zucko\Services\Validator\LaravelValidator;
class CreateValidator extends LaravelValidator{
	protected $rules = array(
		'user_id' => 'required|exists:users,id',
		'target_id' => 'required',
		'target_type' => 'required|in:Zucko\Event\Event,Zucko\Business\Business,Zucko\Blog\Blog,Zucko\User\User',
		'caption' => '',
		'relative_path' => 'required',
		'thumb_path' => 'required'
		);
}