<?php
namespace Zucko\Photo;
/*
id
user_id
target_id
target_type
created_at
updated_at
caption
relative_path
thumb_path
*/
use Zucko\BaseModel as Base;
class Photo extends Base{
	protected $table = "photos";
	protected $fillable = array('user_id','target_id','relative_path','thumb_path','target_type','caption');
	protected $appends = array('url', 'thumb_url');
	public function getThumbUrlAttribute(){
		return url($this->thumb_path);
	}
	public function target(){
		return $this->morphTo();
	}
	public function getUrlAttribute(){
		return url($this->relative_path);
	}
}