<?php
namespace Zucko\Inbox\Repos;
use Zucko\Inbox\Message;
use Zucko\Inbox\Thread;
use Zucko\Inbox\Services\Validator\MessageCreateValidator;
use Zucko\Inbox\Services\Validator\ThreadCreateValidator;
use Illuminate\Support\MessageBag;
class InboxRepo{
	protected $message;
	protected $thread;
	protected $msg_create_validator;
	protected $thread_create_validator;
	protected $errors;
	public function __construct(Message $message, Thread $thread, MessageCreateValidator $msg_create_validator, ThreadCreateValidator $thread_create_validator, MessageBag $bag){
		$this->message = $message;
		$this->thread = $thread;
		$this->msg_create_validator = $msg_create_validator;
		$this->thread_create_validator = $thread_create_validator;
		$this->errors = $bag;
	}
	public function getThread($id){
		if($id instanceof Thread){
			return $id;
		}
		return $this->thread->find($id);
	}
	public function deleteThread($user_id, $thread){
		$thread = $this->getThread($thread);
		if($thread->user1_id==$user_id){
			$thread->user1_deleted = 1;
		}
		if($thread->user2_id==$user_id){
			$thread->user2_deleted = 1;
		}
		return $thread->update();
	}
	public function accessibleBy($user_id, $thread){
		$thread = $this->getThread($thread);
		if(!$thread){
			return false;
		}
		if($thread->user2_id==$user_id||$thread->user1_id==$user_id){
			return true;
		}else{
			return false;
		}
	}
	public function getMessages($thread_id){
		$messages = $this->message->where('thread_id','=',$thread_id)->orderBy('id','desc');
		return $messages;
	}
	public function changeReadStatusThread($thread_id, $user_id){
		return $this->getMessages($thread_id)->where('receiver_id','=',$user_id)->update(array('unread'=>0));
	}
	public function changeReadStatus($msg_id){
		$msg= $this->message->find($msg_id);
		return $msg->update(array('unread'=>0));
	}
	public function getThreads($user_id, $pagging=10, $q="", $boxType = "both"){
		$threads = $this->thread->where(function($query) use($user_id){
			$query->where(function($query) use($user_id){
				$query->where('user1_id','=',$user_id)
				->where('user1_deleted','=',0);
			})->orWhere(function($query) use($user_id){
				$query->where('user2_id','=',$user_id)
				->where('user2_deleted','=',0);
			});
		})->orderBy('updated_at','desc');
		if($boxType=="received"){
			$threads = $threads->whereHas('messages', function($query) use($user_id){
				$query->where('receiver_id','=',$user_id);
			});
		}
		if($boxType=="sent"){
			$threads = $threads->whereHas('messages', function($query) use($user_id){
				$query->where('sender_id','=',$user_id);
			});
		}
		if($q){
			$threads = $threads->whereHas('messages', function($query) use($q){
				$query->where('message','like','%'.$q.'%');
			});
		}
		//dd($threads->toSql());
		if($pagging>0){	
			$threads = $threads->paginate($pagging);
		}
		return $threads;
	}
	public function createThread(array $data){
		$this->thread_create_validator->with($data);
		if(!$this->thread_create_validator->passes()){
			$this->errors = $this->thread_create_validator->errors();
			return false;
		}
		$thread = $this->thread->create($data);
		return $thread;
	}
	public function createMessage(array $data){
		$this->msg_create_validator->with($data);
		if(!$this->msg_create_validator->passes()){
			$this->errors = $this->msg_create_validator->errors();
			return false;
		}
		$data['unread'] = true;
		$message = $this->message->create($data);
		return $message;
	}
	public function errors(){
		return $this->errors;
	}
}