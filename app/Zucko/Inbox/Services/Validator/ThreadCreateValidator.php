<?php
namespace Zucko\Inbox\Services\Validator;
use Zucko\Services\Validator\LaravelValidator;
class ThreadCreateValidator extends LaravelValidator{
	protected $rules = array(
			'user1_id' => 'required|exists:users,id',
			'user2_id' => 'required|exists:users,id',
			'subject' => 'required',
		);
}