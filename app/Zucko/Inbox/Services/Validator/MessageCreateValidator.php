<?php
namespace Zucko\Inbox\Services\Validator;
use Zucko\Services\Validator\LaravelValidator;
class MessageCreateValidator extends LaravelValidator{
	public $rules = array(
			'sender_id' => 'required|exists:users,id',
			'receiver_id' => 'required|exists:users,id',
			'thread_id' => 'required|exists:threads,id',
			'message' => 'required'
		);
}