<?php
namespace Zucko;
class BaseModel extends \Eloquent{
	const PUBLISHED = "published";
	const DRAFT = "draft";
}