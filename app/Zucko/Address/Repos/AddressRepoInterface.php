<?php
namespace Zucko\Address\Repos;
interface AddressRepoInterface{
	public function getAllCountries();
	public function getAllStates();
	public function getCityByName($name);
	public function getCitiesByName($name);
	public function getStatesByName($name);
	public function saveCity($name);
	public function addState($name);
}