<?php
namespace Zucko\Address;
/*
id
name
country_id
business_count
*/
use Zucko\BaseModel as Base;
class State extends Base{
	public $timestamps = false;
	protected $table = "states";
	protected $fillable = array('name');
	
}