<?php
namespace Zucko\Address;
/*
id
name
slag
business_count
event_count
*/
use Zucko\BaseModel as Base;
class City extends Base{
	public $timestamps = false;
	protected $table = "cities";
	protected $fillable = array('name','slag');
	protected $appends = array('events_page','businesses_page');
	public function getEventsPageAttribute(){
		return url('events?city_id='.$this->id);
	}
	public function getBusinessesPageAttribute(){
		return url('businesses/from/'.$this->slag);
	}
	public function events(){
		return $this->hasMany('Zucko\Event\Event');
	}
	public function businesses(){
		return $this->hasMany('Zucko\Business\Business');
	}
}