<?php
namespace Zucko\Address;
/*
id
name
business_count
*/
use Zucko\BaseModel as Base;

class Country extends Base{
	public $timestamps = false;
	protected $table = "countries";
	protected $fillable = array('name');

}