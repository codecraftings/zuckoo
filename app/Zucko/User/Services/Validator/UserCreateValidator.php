<?php
namespace Zucko\User\Services\Validator;
use Zucko\Services\Validator\LaravelValidator;
class UserCreateValidator extends LaravelValidator{
	protected $rules = array(
			'first_name' => 'required|max:20',
			'last_name' => 'max:20',
			'nickname' => 'required|min:5|max:20|alpha_dash|unique:users,nickname',
			'hidename' => 'boolean',
			'email' => 'required|min:5|max:50|email|unique:users,email',
			'password' => 'required|min:6|max:20|confirmed',
			'gender' => 'required|in:MALE,FEMALE',
			'birthdate' => 'required|date',
			'profile_pic' =>'url'
		);
}