<?php
namespace Zucko\User;
use Zucko\BaseModel as Base;
class Role extends Base{
	const USER = 1;
	const ADMIN = 2;
	protected $table = 'roles';
	public $timestamps = false;
}