<?php
namespace Zucko\User\Presenters;
use Laracasts\Presenter\Presenter;

class UserPresenter extends Presenter{
	public function name(){
		if($this->hide_name){
			return $this->nickname;
		}
		return $this->first_name.' '.$this->last_name;
	}
	public function mood_status(){
		if(!($this->about)){
			return "This user haven't said anything about ".($this->gender=="MALE"?'him':'her').'.';
		}else{
			return $this->about;
		}
	}
}