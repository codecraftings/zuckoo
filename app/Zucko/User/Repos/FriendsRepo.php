<?php
namespace Zucko\User\Repos;
use Zucko\User\Friendship;
use Zucko\User\Repos\UserRepo;
use Illuminate\Support\MessageBag;
class FriendsRepo{
	protected $errors;
	protected $user_repo;
	protected $friendship;
	public function __construct(UserRepo $user_repo, Friendship $friendship, MessageBag $bag){
		$this->user_repo = $user_repo;
		$this->friendship = $friendship;
		$this->errors = $bag;
	}
	public function getFriends($user){
		$user = $this->user_repo->get($user);
		return $user->friends;
	}
	public function getInvitations($user){
		$user = $this->user_repo->get($user);
		return $user->invitations;
	}
	public function addFriend($user_id, $friend_id){
		$ff = $this->friendship->create(array('user_id'=>$user_id,'friend_id'=>$friend_id,'status'=>1));
		$ff->save();
		$ff = $this->friendship->create(array('user_id'=>$friend_id,'friend_id'=>$user_id,'status'=>0));
		$ff->save();
		return true;
	}
	public function acceptFriend($user_id, $friend_id){
		$this->friendship->where('user_id','=',$user_id)->where('friend_id','=',$friend_id)->update(array('status'=>1));
		$this->friendship->where('friend_id','=',$user_id)->where('user_id','=',$friend_id)->update(array('status'=>1));
		return true;
	}
	public function removeFriend($user_id, $friend_id){
		$this->friendship->where('user_id','=',$user_id)->where('friend_id','=',$friend_id)->delete();
		$this->friendship->where('friend_id','=',$user_id)->where('user_id','=',$friend_id)->delete();
		return true;
	}
	public function errors(){
		return $this->errors;
	}
}