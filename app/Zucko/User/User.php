<?php
namespace Zucko\User;

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Laracasts\Presenter\PresentableTrait;
/*
DB Fileds
-----------
id
first_name
last_name
nickname
hide_name--- boolean false
email
password
gender enam(MALE, FEMALE)
zip_code
email_status
birthdate Datetime
profile_pic
remember_token
created_at
updated_at
about
city_name
*/
use Zucko\BaseModel as Base;
class User extends Base implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	use PresentableTrait;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	protected $presenter = 'Zucko\User\Presenters\UserPresenter';
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	protected $fillable = array('first_name','last_name','nickname','hide_name','city_name','email','password','gender','about','zip_code','birthdate','profile_pic');
	protected $guarded = array('id','remember_token','email_status', 'role_id');
	protected $appends = array('profile_url');
	public function getProfileUrlAttribute(){
		return url('user/'.$this->id.'/'.$this->nickname);
	}
	public function getProfilePicAttribute(){
		$pp = $this->attributes['profile_pic'];
		if(empty($pp)){
			return url('img/avatar.jpg');
		}
		return $pp;
	}
	public function email_verified(){
		return $this->email_status=="verified";
	}
	public function role(){
		return $this->belongsTo('Zucko\User\Role');
	}
	public function businesses(){
		return $this->hasMany('Zucko\Business\Business');
	}
	public function blogs(){
		return $this->hasMany('Zucko\Blog\Blog');
	}
	public function comments(){
		return $this->hasMany('Zucko\Comment\Comment');
	}
	public function blog_comments(){
		return $this->hasMany('Zucko\Blog\Comment');
	}
	public function events(){
		return $this->hasMany('Zucko\Event\Event');
	}
	public function reviews(){
		return $this->hasMany('Zucko\Review\Review');
	}
	public function photos(){
		return $this->hasMany('Zucko\Photo\Photo');
	}
	public function compliments_given(){
		return $this->hasMany('Zucko\Review\Compliment');
	}
	public function compliments_received(){
		return $this->morphMany('Zucko\Review\Compliment','target');
	}
	public function votes_given(){
		return $this->hasMany('Zucko\Review\Vote');
	}
	public function likes_given(){
		return $this->hasMany('Zucko\Review\Vote')->where('type','=','like');
	}
	public function messages_received(){
		return $this->hasMany('Zucko\Inbox\Message','receiver_id');
	}
	public function messages_sent(){
		return $this->hasMany('Zucko\Inbox\Message','sender_id');
	}
	public function is_friend($user_id){
		return $this->all_friends()->where('friend_id','=',$user_id)->count()>0;
	}
	public function all_friends(){
		return $this->belongsToMany('Zucko\User\User','friendships','user_id','friend_id');
	}
	public function friends(){
		return $this->belongsToMany('Zucko\User\User','friendships','user_id','friend_id')->wherePivot('status','=',1);
	}
	public function invitations(){
		return $this->belongsToMany('Zucko\User\User','friendships','user_id','friend_id')->wherePivot('status','=',0);
	}
	public function getDates(){
	    return ['birthdate', 'created_at', 'updated_at'];
	}
}
