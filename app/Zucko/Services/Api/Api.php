<?php
namespace Zucko\Services\Api;
use Zucko\Services\Api\API_ERROR;
class Api{
	public function __construct(){

	}
	public function success($data=""){
		$response = array('state'=> 'ok');
		if(is_string($data)){
			$response['message'] = $data;
		}else{
			$response = array_merge($response, $data);
		}
		return \Response::json($response);
	}
	public function error($code, $message =""){
		if(empty($message)){
			$message = API_ERROR::get_message($code);
		}
		$data = array('state'=>'error','error_code'=>$code, 'error_message'=>$message);
		return \Response::json($data);
	}
}