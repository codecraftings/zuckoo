<?php
namespace Zucko\Services\Mail\Validator;
use Zucko\Services\Validator\LaravelValidator;
class ContactMailValidator extends LaravelValidator{
	protected $rules = array(
			'name' => 'required',
			'email' => 'required|email',
			'subject' => 'required',
			'msg_body' => 'required'
		);
}