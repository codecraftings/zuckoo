<?php
namespace Zucko\Services;
use Illuminate\Support\ServiceProvider;
class DependencyServiceProvider extends ServiceProvider{
	public function register(){
		$this->app->bind('Zucko\User\Repos\UserRepoInterface','Zucko\User\Repos\UserRepo');
		$this->app->bind('Zucko\User\Repos\FriendsRepoInterface','Zucko\User\Repos\FriendsRepo');
	}
}