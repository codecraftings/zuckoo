<?php
namespace Zucko\Services\Search;

use Illuminate\Pagination\Factory as Factory;
class Paginator{
	protected $factory;
	public function __construct(Factory $factory){
		$this->factory = $factory;
	}
	public function make($query, $pagging=10, $page=1){
		$total = $query->count();
		$items = $query->skip(($page-1)*$pagging)->take($pagging)->get()->all();
		$this->factory->setCurrentPage($page);
		$result = $this->factory->make($items, $total, $pagging);
		return $result;
	}
}