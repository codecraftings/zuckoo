<?php
namespace Zucko\Comment\Services\Validator;
use Zucko\Services\Validator\LaravelValidator;
class CommentUpdateValidator extends LaravelValidator{
	protected $rules = array(
			'target_id' => 'sometimes|required',
			'target_type' => 'sometimes|required',
			'description' => 'sometimes|required|min:5',
			'user_id' => 'sometimes|required|exists:users,id'
		);
}