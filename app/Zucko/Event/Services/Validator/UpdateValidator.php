<?php
namespace Zucko\Event\Services\Validator;
use Zucko\Services\Validator\LaravelValidator;
class UpdateValidator extends LaravelValidator{
	protected $rules = array(
		'title' => 'sometimes|required|min:10|max:50',
		'location' => 'sometimes|required',
		'link' =>'url',
		'video_url' => 'url',
		'description' =>'sometimes|required',
		'user_id' => 'sometimes|required|exists:users,id',
		'city_id' => 'sometimes|required|exists:cities,id',
		'event_time' => 'sometimes|required|date'
		);
}