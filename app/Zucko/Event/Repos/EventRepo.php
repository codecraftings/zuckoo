<?php
namespace Zucko\Event\Repos;
use Zucko\Event\Event;
use Zucko\Event\Services\Validator\CreateValidator;
use Zucko\Event\Services\Validator\UpdateValidator;
use Illuminate\Support\MessageBag;
use Zucko\Review\Repos\VotingRepo;
use Zucko\Services\Api\API_ERROR;
use Zucko\Services\Search\Paginator;
use Zucko\Photo\Repos\PhotoRepo;
use Zucko\Address\Repos\AddressRepo;
class EventRepo{
	protected $event;
	protected $create_validator;
	protected $update_validator;
	protected $voting;
	protected $errors;
	protected $photo;
	protected $paginator;
	public function __construct(Event $event, AddressRepo $address, PhotoRepo $photo, Paginator $paginator, VotingRepo $voting, MessageBag $bag, CreateValidator $create_validator, UpdateValidator $update_validator){
		$this->event = $event;
		$this->address = $address;
		$this->voting = $voting;
		$this->create_validator = $create_validator;
		$this->update_validator = $update_validator;
		$this->errors = $bag;
		$this->photo = $photo;
		$this->paginator = $paginator;
	}
	public function get($id){
		if($id instanceof Event){
			return $id;
		}
		$event = $this->event->find($id);
		return $event;
	}
	public function powerSearch($search, $filters, $pagging=10, $page=1){
		$q = $this->event->query();
		if($search){
			$q->where('title','like','%'.$search.'%');
		}
		if(isset($filters['sort'])){
			switch ($filters['sort']) {
				case 'updated':
					$q->orderBy('updated_at','desc');
					break;
				case 'old':
					$q->orderBy('created_at','asc');
					break;
				default:
					$q->orderBy('created_at','desc');
					break;
			}
		}
		return $this->paginator->make($q, $pagging, $page);
	}
	public function search($str, $filters, $pagging=10, $page=1){
		$q = $this->event->published();
		if(!empty($str)){
			$q = $q->where('title','like','%'.$str.'%');
		}
		if(isset($filters['place_name'])&&!empty($filters['place_name'])){
			$q = $q->where('location','like','%'.$filters['place_name'].'%');
		}
		if(isset($filters['city_id'])&&!empty($filters['city_id'])){
			$q = $q->where('city_id', '=', $filters['city_id']);
		}
		if(isset($filters['day1'])&&isset($filters['day2'])){
			$q = $q->whereBetween('event_time', array($filters['day1'],$filters['day2']));
		}
		$q->orderBy("id",'desc');
		$result = $this->paginator->make($q, $pagging, $page);
		return $result;
	}
	public function getPopular($city_name, $num=5){
		return $this->event->query()->orderByRaw('rand()')->take($num)->get();
	}
	public function getRecent($city_name, $num=5){
		return $this->event->query()->orderBy('id','desc')->take($num)->get();
	}
	public function create(array $data){
		$this->create_validator->with($data);
		if(!$this->create_validator->passes()){
			$this->errors = $this->create_validator->errors();
			return false;
		}
		$data['listing_status'] = \Zucko\BaseModel::DRAFT;
		$event = $this->event->create($data);
		return $event;
	}
	public function changeStatus($event, $newStatus){
		$event = $this->get($event);
		if($event){
			$event->listing_status = $newStatus;
			$event->save();
		}
	}
	public function removePhoto($event, $photo){
		$event = $this->get($event);
		$photo = $this->photo->get($photo);
		if(!$event||!$photo||$event->id!==$photo->target_id){
			$this->errors->add('photo', 'Something went wrong');
			return false;
		}
		$this->photo->update($photo, array('target_id'=>0));
	}
	public function update($event, array $data){
		$event = $this->get($event);
		if(!$event){
			$this->errors->add('event','This event does not exist.');
			return false;
		}
		$this->update_validator->with($data);
		if(!$this->update_validator->passes()){
			$this->errors = $this->update_validator->errors();
			return false;
		}
		$event->update($data);
		$this->address->updateCity($event->city_id);
		return $event;
	}
	public function delete($event){
		$event = $this->get($event);
		if($event){
			$event->delete();
		}
		return true;
	}
	public function addInterestedUser($event, $user_id){
		$event = $this->get($event);
		if(!$event){
			$this->errors->add('event',API_ERROR::msg(API_ERROR::UNKNOWN_ERROR));
			return false;
		}
		$vote = $this->voting->addInterested($event->id, $user_id);
		if(!$vote){
			$this->errors = $this->voting->errors();
			return false;
		}
		$event->interested_count = $event->interested()->count();
		$event->update();
		return $event;
	}
	public function removeInterestedUser($event, $user_id){
		$event = $this->get($event);
		if(!$event){
			$this->errors->add('event',API_ERROR::msg(API_ERROR::UNKNOWN_ERROR));
			return false;
		}
		$vote = $this->voting->getByOwners($user_id, $event->id, "Zucko\Event\Event", "interested");
		if(false != $vote){
			$this->voting->delete($vote);
		}
		$event->interested_count = $event->interested()->count();
		$event->update();
		return $event;
	}
	public function errors(){
		return $this->errors;
	}
}