<?php
namespace Zucko\Review;
/*
id
user_id
target_it
target_type
type
created_at
updated_at
*/
use Zucko\BaseModel as Base;
class Vote extends Base{
	protected $fillable = array('user_id','target_id','target_type','type');
	protected $table = "votes";
	public function target(){
		return $this->morphTo();
	}
	public function user(){
		return $this->belongsTo('Zucko\User\User');
	}
}