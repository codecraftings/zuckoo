<?php
namespace Zucko\Review\Presenters;
use Laracasts\Presenter\Presenter;
class ReviewPresenter extends Presenter{
	public function desc($len=1000){
		$desc = $this->description;
		if(strlen($desc)>$len){
			$desc = e(substr($desc, 0, $len));
			$desc .='... <a href="'.$this->permalink.'">read more</a>';
		}
		return $desc;
	}
	public function title($len=40){
		$str = $this->user->present()->name()."'s review about ".$this->business->present()->title();
		return limit_str($str, $len, "..");
	}
}