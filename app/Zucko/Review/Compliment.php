<?php
namespace Zucko\Review;
/*
id
user_id
target_id
target_type
description
created_at
updated_at
*/
use Zucko\BaseModel as Base;
class Compliment extends Base{
	protected $table = "compliments";
	public function target(){
		return $this->morphTo();
	}
	public function user(){
		return $this->belongsTo('Zucko\User\User');
	}
}