<?php
namespace Zucko\Review\Services\Validator;
use Zucko\Services\Validator\LaravelValidator;
class VoteCreateValidator extends LaravelValidator{
	protected $rules = array(
			'type' => 'required|in:interested,likes,useful,useful,funny',
			'target_id' => 'required|integer',
			'target_type' => 'required|in:Zucko\Review\Review,Zucko\Event\Event',
			'user_id' => 'required|exists:users,id'
		);
}