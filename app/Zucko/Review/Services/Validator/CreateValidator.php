<?php
namespace Zucko\Review\Services\Validator;
use Zucko\Services\Validator\LaravelValidator;
class CreateValidator extends LaravelValidator{
	protected $rules = array(
			'rating' => 'required|numeric',
			'description' => 'required|min:10',
			'user_id' => 'required|exists:users,id',
			'business_id' => 'required|exists:businesses,id'
		);
}