<?php
namespace Zucko\Review;
/*
id
rating
description
user_id
business_id
created_at
updated_at
view_count
useful_count;
funny_count
likes_count
*/
use Zucko\BaseModel as Base;
use Laracasts\Presenter\PresentableTrait;
class Review extends Base{
	use PresentableTrait;
	protected $presenter = "Zucko\Review\Presenters\ReviewPresenter";
	protected $table = "reviews";
	protected $fillable = array(
			'rating',
			'description',
			'user_id',
			'business_id'
		);
	protected $appends = array('like_url','permalink','comments_page', 'voted_useful','voted_funny', 'voted_likes');
	private $_voted_useful = false;
	private $_voted_funny = false;
	private $_voted_likes = false;
	private $_interaction_calculated = false;
	public function getVotedUsefulAttribute(){
		return $this->_voted_useful;
	}
	public function getVotedFunnyAttribute(){
		return $this->_voted_funny;
	}
	public function getVotedLikesAttribute(){
		return $this->_voted_likes;
	}
	public function withUserInteractions($user){
		if(!$user||$this->_interaction_calculated){
			return $this;
		}
		if($this->votes()->where(array('type'=>'useful','user_id'=>$user->id))->count()>0){
			$this->_voted_useful = true;
		}
		if($this->votes()->where(array('type'=>'funny','user_id'=>$user->id))->count()>0){
			$this->_voted_funny = true;
		}
		if($this->votes()->where(array('type'=>'likes','user_id'=>$user->id))->count()>0){
			$this->_voted_likes = true;
		}
		$_interaction_calculated = true;
		return $this;
	}
	public function getCommentsPageAttribute(){
		return url('review/'.$this->id.'/comments');
	}
	public function getLikeUrlAttribute(){
		return url('reviews/like/'.$this->id);
	}
	public function getPermalinkAttribute(){
		return url('reviews/'.$this->id);
	}
	public function getViewCountAttribute(){
		$this->attributes['view_count'] += 1;
		$this->save();
		return $this->attributes['view_count'];
	}
	public function user(){
		return $this->belongsTo('Zucko\User\User');
	}
	public function business(){
		return $this->belongsTo('Zucko\Business\Business');
	}
	public function votes(){
		return $this->morphMany('Zucko\Review\Vote','target');
	}
	public function likes(){
		return $this->votes()->where('type','=','likes');
	}
	public function comments(){
		return $this->morphMany('Zucko\Comment\Comment', 'target');
	}
}