<?php
namespace Zucko\Business;
use Zucko\Address\Repos\AddressRepo;
use Zucko\Business\Repos\BusinessRepo;
class ImportFromCsv{
	protected $filename;
	protected $fields_index;
	protected $address;
	protected $business;
	public function __construct($filename, AddressRepo $address, BusinessRepo $business){
		$this->filename = $filename;
		$this->business = $business;
		$this->fields_index = array(
			'company_or_service' => 0,
			'address' => 1,
			'zip_code' => 2,
			'brand' => 3,
			'category' => 4,
			'city' => 5,
			'client' => 6,
			'first_name' => 7,
			'position' => 8,
			'email' => 9,
			'facebook' => 10,
			'fax' => 11,
			'logo' => 12,
			'island_country' => 13,
			'last_name' => 14,
			'phone' => 15,
			'pro_ref' => 16,
			'share_capital' => 17,
			'tahiti_id' => 18,
			'tahiti_rc' => 19,
			'company_type' => 20,
			'vini' => 21,
			'website' => 22
			);
		$this->address = $address;
	}
	public function startImport(){
		$rownum = 0;
		$sql = "";
		if (($handle = fopen(base_path().'/resources/db/'.$this->filename, "r")) !== FALSE) {
		    while (($row = fgetcsv($handle)) !== FALSE) {
		        $rownum++;
		    	if($rownum<2){
		    		continue;
		    	}
		        $fieldnum = count($row);
		        \Log::info("$fieldnum fields in line : $rownum");
		        //echo $row[0];
		        //echo ($row[0])."<br>";
		        //var_dump($row);
		        $business = $this->processRow($row);
		        $sql .= $this->insertIntoDb($business);
		        continue;
		        $business = $this->business->createBL($business);
		        //dd($business);
		        if(!$business){
		        	\Log::info($rownum);
		        	\Log::error($this->business->errors());
		        	continue;
		        }
		        \Log::info($business->name." added succesfully");
		    }
		    \Log::info('Finishing Import');
		    fclose($handle);
		}
		echo '<textarea>'.$sql.'</textarea>';
		\Log::error('Problem opening File');
	}
	public function insertIntoDb($data){
		$table = "bl_business";
		$q = "INSERT INTO `".$table."`(";
		foreach ($data as $key => $value) {
			$q .= "`".$key."`, ";
		}
		$q = mb_substr($q, 0, mb_strlen($q)-2);
		$q .= ") VALUES (";
		foreach ($data as $key => $value) {
			$q .= "'".addslashes($value)."', ";
		}
		$q = mb_substr($q, 0, mb_strlen($q)-2).")";
		return $q.";\n";
		//echo \DB::statement($q);
	}
	private function processRow($row){
		$data = array(
			'company_or_service' => $row[0],
			'address' => $row['1'],
			'zip_code' => $row['2'],
			'brand' => $row['3'],
			'category' => $row['4'],
			'city' => $row['5'],
			'client' => $row['6'],
			'first_name' => $row['7'],
			'position' => $row['8'],
			'email' => $row['9'],
			'facebook' => $row['10'],
			'fax' => $row['11'],
			'logo' => $row['12'],
			'island_country' => $row['13'],
			'last_name' => $row['14'],
			'phone' => $row['15'],
			'pro_ref' => $row['16'],
			'share_capital' => $row['17'],
			'tahiti_id' => $row['18'],
			'tahiti_rc' => $row['19'],
			'company_type' => $row['20'],
			'vini' => $row['21'],
			'website' => $row['22']
			);
		$cats = array(
			"Tourism" => 17,
			"Shopping" => 3,
			"Transport" => 11,
			"Professionnal" => 1,
			"Lodging" => 2,
			"Manager" => 8,
			"Brand" => 7,
			"Restaurant" => 16,
			"Sport" => 15,
			"Adminstration" => 18,
			"Administration responsable" => 19
			);
		if(empty($data['category'])){
			$data['category'] = "Unknown";
			$data['category_id'] = 10;
		}else{
			$data['category_id'] = isset($cats[$data['category']])?$cats[$data['category']]:0;
		}
		$data['phone'] = str_replace(" ", "", $data['phone']);
		$data['fax'] = str_replace(" ", "", $data['fax']);
		$data['vini'] = str_replace(" ", "", $data['vini']);
		$data['zip_code'] = preg_replace("/(^|\s)BP\s/i", "$1BP", $data['zip_code']);
		return $data;
	}
}