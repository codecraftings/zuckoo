<?php
namespace Zucko\Business\Presenters;
use Laracasts\Presenter\Presenter;
class BusinessPresenter extends Presenter{
	public function title($maxlen=30){
		//return strlen($this->name)>$maxlen?substr($this->name, 0, $maxlen)."...":$this->name;
		return ucwords(limit_str($this->name, $maxlen, ".."));
	}
	public function addr($maxlen=100){
		//return strlen($this->address)>$maxlen?substr($this->address, 0, $maxlen)."...":$this->address;
		return ucwords(limit_str($this->address, $maxlen, ".."));	
	}
	public function phone(){
		if(!$this->meta->phone){
			return 'N\A';
		}else{
			$num = str_replace(" ", "", $this->meta->phone);
			$num = str_split($num);
			$formated = "";
			foreach ($num as $key => $n) {
				if($key==2){
					$formated .= $n." ";
				}
				elseif ($key==5) {
					$formated .= $n." ";
				}
				else{
					$formated .= $n;
				}
			}
			return $formated;
		}
	}
	public function city_name(){
		if(!$this->city){
			return 'Unknown City';
		}
		return $this->city;
	}
	public function state_name(){
		if(!$this->state){
			return 'Unknown State';
		}
		return $this->state;
	}
	public function category_name(){
		if(!$this->category){
			return 'N\A';
		}
		return $this->category->name;
	}
	public function rating(){
		if($this->rating_score<1){
			return 1;
		}
		return $this->rating_score;
	}
}