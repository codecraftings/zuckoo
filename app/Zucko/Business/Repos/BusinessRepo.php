<?php
namespace Zucko\Business\Repos;
use Zucko\Business\Business;
use Zucko\Business\BusinessMeta;
use Zucko\Business\Category;
use Zucko\Business\Services\Validator\CreateValidator;
use Zucko\Business\Services\Validator\UpdateValidator;
use Zucko\Address\City;
use Zucko\Address\Country;
use Zucko\Address\State;
use Zucko\Business\BLBusiness;
use Illuminate\Pagination\Factory as Paginator;
use Zucko\Photo\Repos\PhotoRepo;
class BusinessRepo{
	protected $business;
	protected $business_meta;
	protected $category;
	protected $city;
	protected $state;
	protected $country;
	protected $create_validator;
	protected $update_validator;
	protected $bl_business;
	protected $paginator;
	protected $photo;
	protected $errors = array();
	public function __construct(Business $business, PhotoRepo $photo, Paginator $paginator, BusinessMeta $business_meta, Category $category, City $city, Country $country, State $state, CreateValidator $create_validator, UpdateValidator $update_validator, BLBusiness $bl_business){
		$this->business = $business;
		$this->business_meta = $business_meta;
		$this->category = $category;
		$this->city = $city;
		$this->country = $country;
		$this->state = $state;
		$this->paginator = $paginator;
		$this->bl_business = $bl_business;
		$this->create_validator = $create_validator;
		$this->update_validator = $update_validator;
		$this->photo = $photo;
	}
	public function getCategory($id){
		if($id instanceof Category){
			return $id;
		}
		return $this->category->find($id);
	}
	public function getCategoriesByName($name){
		$category = $this->category->whereRaw('soundex(`name`) like soundex(?)', array($name));
		return $category->get();
	}
	public function getCategoryBySlag($slag){
		$category = $this->category->where('slag','like',$slag);
		return $category->first();
	}
	public function addCategory($name, $slag =""){
		if(empty($slag)){
			$slag = str_replace(" ", "-", strtolower($slag));
		}
		return $this->category->create(array('name'=>$name, 'slag'=>$slag));
	}
	public function getCategories(){
		return $this->category->all();
	}
	public function powerSearch($search, $filters, $pagging=10, $page=1){
		$q = $this->business->query();
		if(!empty($search)){
			$q->where('name','like','%'.$search.'%');
		}
		$total = $q->count();
		if(isset($filters['sort'])){
			switch ($filters['sort']) {
				case 'updated':
				$q->orderBy('updated_at','desc');
				break;
				case 'old':
				$q->orderBy('created_at','asc');
				break;
				default:
				$q->orderBy('id','desc');
				break;
			}
		}
		$items = $q->skip(($page-1)*$pagging)->take($pagging)->get()->all();
		$this->paginator->setCurrentPage($page);
		return $this->paginator->make($items, $total, $pagging);
	}
	/*
	filters will be array consisting distance, sorting, pricing, city_ids, catgory_ids
	*/
	public function search($what, $where, array $filters, $page=1, $pagging=10){
		$q = $this->business->published()->with('meta');
		if(!empty($what)){
			$q = $q->where(function($query) use($what){
				$query->where('name','like','%'.$what.'%');
			});
		}
		if(!empty($where)){
			$q = $q->where(function($query) use($where){
				$num = str_replace(" ", "", $where);
				$query->where("address","like","%".$where."%")
				->orWhere("zip_code","like", $num)
				->orWhere("city","like","%".$where."%")
				->orWhere("state","like","%".$where."%")
				->orWhere("country","like","%".$where."%");
			});
		}
		if(isset($filters['category_ids'])&&!empty($filters['category_ids'])){
			$q = $q->whereIn('category_id',$filters['category_ids']);
		}
		if(isset($filters['sorting'])){
			switch ($filters['sorting']) {
				case 'rating':
				$q = $q->orderBy('rating_score','desc');
				break;
				case 'views':
				$q = $q->orderBy('view_count','desc');
				break;
				case 'review_count':
				$q = $q->orderBy('review_count','desc');
				break;
				default:
					# code...
				break;
			}
		}
		
		if(isset($filters['features'])){
			$q->where(function($Q) use($filters){
				foreach ($filters['features'] as $key => $feature) {
					$Q->whereExists(function($query) use ($feature, $filters){
						$query->select(\DB::raw(1))->from('business_metadata');
						$query->whereRaw("businesses.id = business_metadata.business_id");
						switch ($feature) {
							case 'open':
							$query->where('business_metadata.opening_hour_hash','like','%;'.$filters['time'].';%');
							break;
							case 'delivery':
							$query->where('business_metadata.delivery','=', 1);
							break;
							case 'take_out':
							$query->where('business_metadata.to_go','=', 1);
							break;
							default:
						# code...
							break;
						}
					});
				}
			});
		}
		if(isset($filters['prices'])){
			$q->where(function($Q) use ($filters){
				foreach ($filters['prices'] as $key => $price) {
					$Q->orWhereExists(function($query) use ($price){
						$query->select(\DB::raw(1))->from('business_metadata');
						$query->whereRaw("businesses.id = business_metadata.business_id");
						$low;
						$high;
						switch ($price) {
							case 'high':
							$low = 5001;
							$high = 20000;
							break;
							case 'mid':
							$low = 3001;
							$high = 5000;
							break;
							case 'low':
							$low = 1501;
							$high = 3000;
							break;
							case 'very_low':
							$low = 1;
							$high = 1500;
							break;
							default:
								# code...
							break;
						}
						$query->where(function($q) use($low, $high){
							$q->where(function($q) use ($low, $high){
								$q->where('price_low','>=', $low)
								->where('price_low','<=', $high);
							});
							$q->orWhere(function($q) use ($low, $high){
								$q->where('price_low','<', $low)
								->where('price_high','>=', $low);
							});
						});
					});
				}
			});
}
$result = new \stdClass();
$result->page = $page;
$result->pagging = $pagging;
$result->total_items = $q->count();
$items = $q->skip(($page-1)*$pagging)->take($pagging)->get()->all();
$result->items = is_array($items)?$items:array();
$result->items_count = count($result->items);
		//dd($result);
return $result;
}
public function appendBals($who, $where, $main_result, $filters){
	$q = $this->bl_business->query();
	if(!empty($who)){
			//$q = $q->whereRaw('match(`company_or_service`,`brand`, `category`, `first_name`, `position`,`last_name`,`company_type`) against(?)',array($who));
		$num = str_replace(" ", "", $who);
		$q = $q->where(function($query) use($who, $num){
			$query->where('company_or_service','like','%'.$who.'%')
			->orWhere('brand','like','%'.$who.'%')
			->orWhere('category','like','%'.$who.'%')
			->orWhere('first_name','like','%'.$who.'%')
			->orWhere('position','like','%'.$who.'%')
			->orWhere('last_name','like','%'.$who.'%')
			->orWhere('email','like','%'.$who.'%')
			->orWhere('phone','like','%'.$num.'%')
			->orWhere('tahiti_rc','like','%'.$who.'%')
			->orWhere('tahiti_id','like','%'.$who.'%')
			->orWhere('website','like','%'.$who.'%')
			->orWhere('facebook','like','%'.$who.'%')
			->orWhere('fax','like','%'.$num.'%')
			->orWhere('vini','like','%'.$num.'%')
			->orWhere('pro_ref','like','%'.$who.'%')
			->orWhere('company_type','like','%'.$who.'%');
		});
	}
	if(isset($filters['category_ids'])&&!empty($filters['category_ids'])){
		$q = $q->whereIn('category_id',$filters['category_ids']);
	}
	if(!empty($where)){
			//$q = $q->whereRaw('match(`address`,`city`, `island_country`) against(?)',array($where));
		$where = preg_replace("/(^|\s)BP\s/i", "$1BP", $where);
		$q = $q->where(function($query) use($where){
			$query->where('address','like','%'.$where.'%')
			->orWhere('city','like','%'.$where.'%')
			->orWhere('zip_code','like','%'.$where.'%')
			->orWhere('island_country','like','%'.$where.'%');
		});
	}
	$tmp = $main_result->total_items;
	$main_result->total_items += $q->count();
	if($main_result->items_count>=$main_result->pagging||$q->count()<1){
		return $main_result;
	}else{
		$empty_slot = $main_result->pagging - $main_result->items_count;
		$bals_offset = ($main_result->page-1)*$main_result->pagging-$tmp;
			//dd($bals_offset<((-1)*$main_result->pagging));
		if($bals_offset<((-1)*$main_result->pagging)){
			return $main_result;
		}else{
			if($bals_offset<0){
				$bals_offset = 0;
			}
			$items = $q->skip($bals_offset)->take($empty_slot)->get()->all();
			$main_result->items = array_merge($main_result->items, $items);
				//dd($main_result);
			$main_result->items_count += $empty_slot;
			return $main_result;
		}
	}
	return $main_result;
}
public function getPopularBusinesses($city="", $num=4){
	$businesses = $this->business->published()->orderBy('id','desc')->take($num)->get();
	return $businesses;
}
public function get($id){
	if($id instanceof Business){
		return $id;
	}
	return $this->business->find($id);
}
public function getByCity($city){

}
public function getByUser($user_id){
	return $this->business->where('user_id','=',$user_id)->get();
}
public function getByCategory($category_id){
	return $this->business->where('listing_type','=','published')->where('category_id','=',$category_id)->get();
}
public function generateHourHash($hourArray){
	$arr = array("sun","mon","tue","wed","thu","fri","sat");
	$hash = "";
	foreach ($hourArray as $key => $hour) {
		$start = array_search($hour->day1, $arr);
		$end = array_search($hour->day2, $arr);
		$a = array();
		if($start<=$end) {
			$a = array_slice($arr, $start, $end-$start+1);
		}else{
			$a = $arr;
			array_splice($a, $end+1, $start-$end-1);
		}
		foreach ($a as $key => $day) {
			$i = (int)$hour->time1;
			for($i; $i<=12;$i++){
				$hash .= ";".ucfirst($day).str_pad($i, 2,"0", STR_PAD_LEFT)."AM;";
			}
			$i = (int)$hour->time2;
			for($i; $i>0;$i--){
				$hash .= ";".ucfirst($day).str_pad($i, 2,"0", STR_PAD_LEFT)."PM;";
			}
		}
	}
	return $hash;
}
public function create(array $data){
	$this->create_validator->with($data);
	if(!$this->create_validator->passes()){
		$this->errors = $this->create_validator->errors();
		return false;
	}
	if(isset($data['opening_hours'])){
		$data['opening_hour_hash'] = $this->generateHourHash($data['opening_hours']);
		$data['opening_hours'] = json_encode($data['opening_hours']);
	}
	if(isset($data['phone'])){
		$data['phone'] = str_replace(" ", "", $data['phone']);
	}
	if(isset($data['fax'])){
		$data['fax'] = str_replace(" ", "", $data['fax']);
	}
	if(isset($data['vini'])){
		$data['vini'] = str_replace(" ", "", $data['vini']);
	}
	$business = $this->business->create($data);
		//dd($data);
	$data['business_id'] = $business->id;
	$meta = $this->business_meta->create($data);
		//$business->meta()->associate($meta);
		//$meta->business_id = $business->id;
		//$meta->save();
	$business->keywords = $this->generate_keywords($business);
		//$business->user_id = $data['user_id'];
	$business->listing_status = \Zucko\BaseModel::DRAFT;
	$business->save();
	return $business;
}
public function createBL(array $data){
	$business = $this->bl_business->create($data);
	return $business;
}
public function generate_keywords($business){
	$keywords = new \stdClass();
	return json_encode($keywords);
}
public function update($business, array $data){
	$business = $this->get($business);
	$this->update_validator->with($data);
	if(!$this->update_validator->passes()){
		$this->errors = $this->update_validator->errors();
		return false;
	}
	if(isset($data['opening_hours'])){
		$data['opening_hour_hash'] = $this->generateHourHash($data['opening_hours']);
		$data['opening_hours'] = json_encode($data['opening_hours']);
	}
	if(isset($data['phone'])){
		$data['phone'] = str_replace(" ", "", $data['phone']);
	}
	if(isset($data['fax'])){
		$data['fax'] = str_replace(" ", "", $data['fax']);
	}
	if(isset($data['vini'])){
		$data['vini'] = str_replace(" ", "", $data['vini']);
	}
	$meta = $business->meta()->first();
	$meta->fill($data);
	$meta->save();
	$business->fill($data);
	$business->keywords = $this->generate_keywords($business);
	$business->save();
	return $business;
}
public function delete($business){
	$business = $this->get($business);
	if($business){
		$business->delete();
	}
	return true;
}
public function changeStatus($business, $status){
	$business = $this->get($business);
	$business->listing_status = $status;
	$business->save();
}
public function removePhoto($business, $photo){
	$business = $this->get($business);
	$photo = $this->photo->get($photo);
	if(!$business||!$photo||$business->id!==$photo->target_id){
		$this->errors->add('photo', 'Something went wrong');
		return false;
	}
	$this->photo->update($photo, array('target_id'=>0));
}
public function errors(){
	return $this->errors;
}
}