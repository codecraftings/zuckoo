<?php
namespace Zucko\Business\Repos;
interface BusinessRepoInterface{
	public function getCategory($id);
	public function getCategoriesByName($name);
	public function getCategoryBySlag($slag);
	public function addCategory($name, $slag);
	public function getCategories();
	public function search($q, array $filters, $pagging);
	public function getPopularBusinesses($city);
	public function get($id);
	public function getByCity($city);
	public function getByUser($user);
	public function getByCategory($category);
	public function create(array $data);
	public function generate_keywords($business);
	public function update($business, array $data);
	public function errors();
}