<?php
namespace Zucko\Business;
/*
id
name
slag
style
business_count
*/
use Zucko\BaseModel as Base;
class Category extends Base{
	protected $table = "categories";
	protected $fillable = array('name','slag','style');
	public $timestamps = false;
	protected $appends = array('page_url');
	public function getStyleAttribute(){
		return json_decode($this->attributes['style']);
	}
	public function getPageUrlAttribute(){
		return url('search?cats[]='.$this->slag);
	}
	public function businesses(){
		return $this->hasMany('Zucko\Business\Business');
	}
}