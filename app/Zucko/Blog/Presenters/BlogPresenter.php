<?php
namespace Zucko\Blog\Presenters;
use Laracasts\Presenter\Presenter;
class BlogPresenter extends Presenter{
	public function show_title($maxlen=100, $ending="..."){
		$title = $this->title;
		if(strlen($title)>$maxlen){
			$title = e(substr($title, 0, $maxlen));
			$title .=$ending;
		}
		return $title;
	}
	public function desc($maxlen=0, $ending="read_more"){
		$desc = $this->content;
		if($maxlen==0){
			return $desc;
		}
		if($ending=="read_more"){
			$ending = '... <a href="'.$this->blog_page.'">read more</a>';
		}
		$desc = strip_tags($desc);
		if(strlen($desc)>$maxlen){
			$desc = e(substr($desc, 0, $maxlen));
			$desc .=$ending;
		}
		return $desc;
	}
}