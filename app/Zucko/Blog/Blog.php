<?php
namespace Zucko\Blog;
/*
id
title
content
category_id
user_id
featured_img
created_at
updated_at
*/
use Zucko\BaseModel as Base;
use Laracasts\Presenter\PresentableTrait;
class Blog extends Base{
	use PresentableTrait;
	protected $presenter = "Zucko\Blog\Presenters\BlogPresenter";
	protected $table = "blogs";
	protected $fillable = array('title','content','category_id','user_id','featured_img');
	protected $appends = array('permalink');
	public function getPermalinkAttribute(){
		return url('blog/'.$this->id.'/'.urlencode($this->present()->show_title(40,"")));
	}
	public function author(){
		return $this->belongsTo('Zucko\User\User', 'user_id');
	}
	public function category(){
		return $this->belongsTo('Zucko\Blog\Category');
	}
	public function comments(){
		return $this->hasMany('Zucko\Blog\Comment');
	}
	public function img(){
		return $this->belongsTo('Zucko\Photo\Photo', 'featured_img');
	}
}