<?php
namespace Zucko\Blog\Services\Validator;
use Zucko\Services\Validator\LaravelValidator;
class CreateValidator extends LaravelValidator{
	protected $rules = array(
			'title' => 'required|min:10|max:100',
			'content' => 'required|min:100|max:5000',
			'category_id' => 'required|exists:blog_categories,id',
			'user_id' => 'required|exists:users,id',
			'featured_img' => 'exists:photos,id'
		);
}