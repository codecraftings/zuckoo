<?php
namespace Zucko\Blog\Services\Validator;
use Zucko\Services\Validator\LaravelValidator;
class UpdateValidator extends LaravelValidator{
	protected $rules = array(
			'title' => 'sometimes|required|min:10|max:100',
			'content' => 'sometimes|required|min:100|max:5000',
			'category_id' => 'sometimes|required|exists:blog_categories,id',
			'user_id' => 'sometimes|required|exists:users,id',
			'featured_img' => 'exists:photos,id'
		);
}