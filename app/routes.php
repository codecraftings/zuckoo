<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::pattern('id', '[0-9]+');
Route::get('/', 'PagesController@showHome');
Route::get('page/about', 'PagesController@showAboutPage');
Route::get('page/privacy', 'PagesController@showPrivacyPage');
Route::get('page/terms', 'PagesController@showTermsPage');
Route::get('page/career', 'PagesController@showCareerPage');
Route::get('page/mobile', 'PagesController@showMobilePage');
Route::get('page/contact', 'PagesController@showContactPage');
Route::post('page/contact', 'PagesController@submitContactPage');

Route::get('login','SessionController@showLogin');
Route::post('login','SessionController@submitLogin');
Route::get('logout','SessionController@logout');
Route::get('signup','SessionController@showSignup');
Route::post('signup',array('before'=>'captcha-test','uses'=>'SessionController@submitSignup'));
Route::get('verify/email','SessionController@verifyEmail');

Route::get('password/reset','SessionController@showPassReset');
Route::post('password/reset',array('before'=>'csrf','uses'=>'SessionController@submitPassReset'));
Route::get('password/reset/confirm','SessionController@showConfirmReset');
Route::post('password/reset/confirm',array('before'=>'csrf','uses'=>'SessionController@submitConfirmReset'));

Route::get('user/{id}','UserController@showUser');
Route::get('user/{id}/edit',array('before'=>'auth-test', 'uses'=>'UserController@showEditUser'));
Route::post('user/{id}/edit',array('before'=>'auth-test', 'uses'=>'UserController@submitEditUser'));
Route::get('user/{id}/business',array('before'=>'auth-test', 'uses'=>'UserController@showUserBusinesses'));
Route::get('user/{id}/events',array('before'=>'auth-test', 'uses'=>'UserController@showUserEvents'));
Route::get('user/{id}/{nickname}',array('before'=>'', 'uses'=>'UserController@showUser'));

Route::put('api/users/{id}', array('before'=>'auth-test', 'uses'=>'UserController@submitEditUser'));

Route::get('cities','PagesController@showCities');
Route::get('search','BusinessController@showSearchPage');

Route::get('business/add',array('before'=>'auth-test','uses'=>'BusinessController@showAddOne'));
Route::post('business/add',array('before'=>'csrf|auth-test','uses'=>'BusinessController@submitAddOne'));
Route::get('business/add/more-info/{id}',array('before'=>'auth-test','uses'=>'BusinessController@showAddSecond'));
Route::post('business/add/more-info/{id}',array('before'=>'auth-test','uses'=>'BusinessController@submitAddSecond'));
Route::get('business/show/{id}','BusinessController@showProfile');
Route::get('business/show/{id}/{name}','BusinessController@showProfile');
Route::get('business/{id}/edit',array('before'=>'auth-test','uses'=>'BusinessController@showEditPage'));
Route::post('business/{id}/edit',array('before'=>'auth-test','uses'=>'BusinessController@submitEditPage'));
Route::get('business/{id}/reviews/{review_id}','');
Route::get('business/from/{city_name}','');
Route::get('business/of/{cat_name}','');
Route::get('business/{id}/reviews','BusinessController@showReviews');
Route::post('business/addphotos',array('before'=>'auth-test','uses'=>'BusinessController@addPhotosByUser'));

Route::get('reviews/write',array('before'=>'auth-test', 'uses'=>'ReviewsController@showAddPage'));
Route::post('reviews/add',array('before'=>'auth-test','uses'=>'ReviewsController@addReview'));
Route::get('reviews/like/{id}',array('before'=>'auth-test','uses'=>'ReviewsController@likeReview'));
Route::get('reviews/{id}','ReviewsController@showPage');

Route::get('api/reviews/{id}', array('before'=>'','uses'=>'ReviewsController@show'));
Route::post('api/reviews/{review_id}/votes', array('before'=>'auth-test','uses'=>'ReviewsController@createVote'));
Route::post('api/reviews/{review_id}/votes/delete/{user_id}/{type}', array('before'=>'auth-test','uses'=>'ReviewsController@deleteVote'));

Route::get('event/add',array('before'=>'auth-test','uses'=>'EventController@showAdd'));
Route::post('event/add',array('before'=>'auth-test','uses'=>'EventController@submitAdd'));
Route::get('events',array('before'=>'','uses'=>'EventController@listEvents'));
Route::get('event/{id}',array('before'=>'','uses'=>'EventController@showEvent'));
Route::get('event/{id}/{name}',array('before'=>'','uses'=>'EventController@showEvent'));
Route::get('events/from/{city}','EventController@showCityEvents');
Route::get('inbox',array('before'=>'auth-test','uses'=>'InboxController@showInboxPage'));
Route::post('inbox',array('before'=>'auth-test','uses'=>'InboxController@submitInboxAction'));
Route::get('inbox/thread/{id}',array('before'=>'auth-test','uses'=>'InboxController@showThreadPage'));
Route::post('inbox/thread/{id}',array('before'=>'auth-test','uses'=>'InboxController@submitReply'));
Route::get('inbox/invitations',array('before'=>'auth-test','uses'=>'InboxController@showInvitationsPage'));
Route::get('invitations/accept/{friend_id}',array('before'=>'auth-test','uses'=>'UserController@acceptFriend'));
Route::get('invitations/decline/{friend_id}',array('before'=>'auth-test','uses'=>'UserController@declineFriend'));
Route::get('invitations/send/{friend_id}',array('before'=>'auth-test','uses'=>'UserController@addFriend'));
Route::get('inbox/sent',array('before'=>'auth-test','uses'=>'InboxController@showSentPage'));
Route::get('inbox/compose',array('before'=>'auth-test','uses'=>'InboxController@showCompose'));
Route::post('inbox/compose',array('before'=>'auth-test','uses'=>'InboxController@submitCompose'));

Route::get('friends',array('before'=>'auth-test','uses'=>'UserController@showFindFriend'));

Route::get('blogs',array('before'=>'','uses'=>'BlogController@showAllBlogs'));
Route::get('blog/add',array('before'=>'auth-test','uses'=>'BlogController@showAddPage'));
Route::post('blog/add',array('before'=>'csrf|auth-test','uses'=>'BlogController@submitAddPage'));
Route::get('blog/edit/{id}',array('before'=>'auth-test','uses'=>'BlogController@showEditPage'));
Route::post('blog/edit/{id}',array('before'=>'csrf|auth-test','uses'=>'BlogController@submitEditPage'));
Route::post('blog/comment/create',array('before'=>'csrf|auth-test', 'uses'=>'BlogController@addComment'));
Route::get('blog/{id}/{title}', 'BlogController@showBlogPage');

Route::post('ajax/review/create', array('before'=>'csrf|auth-test', 'uses'=>'ReviewsController@ajaxAddReview'));
Route::post('ajax/review/comment',array('before'=>'csrf|auth-test','uses'=>'ReviewsController@ajaxAddComment'));
Route::post('ajax/review/like',array('before'=>'auth-test','uses'=>'ReviewsController@ajaxLikeReview'));
Route::post('ajax/event/interested',array('before'=>'auth-test','uses'=>'EventController@ajaxAddInterestedUser'));
Route::post('ajax/event/interested/delete',array('before'=>'auth-test','uses'=>'EventController@ajaxDeleteInterestedUser'));

Route::get('test',function(){
	echo preg_replace("/(^|\s)BP\s/i", "$1bp", "BP dfd bp ada");
});
Route::get('import', function(){
	$importer = new Zucko\Business\ImportFromCsv('first.csv',App::make('Zucko\Address\Repos\AddressRepo'),App::make('Zucko\Business\Repos\BusinessRepo'));
	$importer->startImport();
	//DB::unprepared(file_get_contents(base_path().'/predata.sql'));
});
Route::get('loaddb', function(){
	///DB::unprepared(file_get_contents(base_path().'/resources/db/zucko.sql'));
	if (($handle = fopen(base_path().'/resources/db/states.txt', "r")) !== FALSE) {
	    while (($row = fgetcsv($handle)) !== FALSE) {
	    	DB::table('states')->insert(array(
	    			'name' => $row[0],
	    			'country_id' => 75,
	    			'business_count' => 0
	    		));
	    }
	}
});
$admin = Config::get('app.admin_dir');
Route::group(array(
		'prefix' => $admin
	), function(){
	Route::get('/login','AdminController@showLogin');
	Route::post('/login',array('before'=>'csrf', 'uses'=>'AdminController@submitLogin'));
	Route::group(array(
			'before' => 'admin-auth-test'
		),function(){
		Route::get('/logout','AdminController@logout');
		Route::get('/', array('before'=>'','uses'=>'AdminController@showDashboard'));
		Route::get('/dashboard', array('before'=>'','uses'=>'AdminController@showDashboard'));
		Route::get('/edit-account','AdminController@showEditAccount');
		Route::post('/edit-account',array('before'=>'csrf','uses'=>'AdminController@submitEditAccount'));

		Route::get('/users/list',array('before'=>'','uses'=>'AdminUsersController@showListUsers'));
		Route::get('/users/edit/{id}',array('before'=>'','uses'=>'AdminUsersController@showEditUser'));
		Route::post('/users/edit/{id}',array('before'=>'csrf','uses'=>'AdminUsersController@submitEditUser'));
		Route::post('/users/delete/{id}',array('before'=>'csrf','uses'=>'AdminUsersController@deleteUser'));

		Route::get('/business/list',array('before'=>'','uses'=>'AdminBusinessController@showListPage'));
		Route::post('/business/list',array('before'=>'csrf','uses'=>'AdminBusinessController@listPageAction'));
		Route::get('/business/edit/{id}',array('before'=>'','uses'=>'AdminBusinessController@showEditPage'));
		Route::post('/business/edit/{id}',array('before'=>'csrf','uses'=>'AdminBusinessController@submitEditPage'));
		Route::post('/business/delete/{id}',array('before'=>'csrf','uses'=>'AdminBusinessController@deleteBusiness'));


		Route::get('events/list','AdminEventController@showListPage');
		Route::get('event/edit/{id}','AdminEventController@showEditPage');
		Route::post('event/edit/{id}', array('before'=>'csrf','uses'=>'AdminEventController@submitEditPage'));
		Route::post('event/delete/{id}', array('before'=>'csrf','uses'=>'AdminEventController@deleteEvent'));

		Route::get('reviews/list','AdminReviewController@showListPage');
		Route::get('review/edit/{id}','AdminReviewController@showEditPage');
		Route::post('review/edit/{id}','AdminReviewController@submitEditPage');
		Route::post('review/delete/{id}','AdminReviewController@deleteReview');

		Route::get('comments/list','AdminCommentController@showListPage');
		Route::get('comment/edit/{id}','AdminCommentController@showEditPage');
		Route::post('comment/edit/{id}','AdminCommentController@submitEditPage');
		Route::post('comment/delete/{id}','AdminCommentController@deleteComment');
		
	});
});