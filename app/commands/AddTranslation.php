<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AddTranslation extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'trans:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $key = $this->argument("key");
        $en = $this->argument("en");
        $fr = $this->argument("fr");
        $parts = explode(".", $key);
        $this->info($key);
        $this->info($en);
        $this->info($fr);
        $filename = $parts[0] . ".php";
        $key = $parts[1];
        if (empty($en)) {
            $en = $parts[1];
        }
        $this->info($filename);
        $en_file = app_path() . "/lang/en/" . $filename;
        $fr_file = app_path() . "/lang/fr/" . $filename;
        $this->createFile($en_file);
        $this->createFile($fr_file);
        $this->appendToFile($en_file, $key, $en);
        if (empty($fr)) {
            $fr = $en;
        }
        $this->appendToFile($fr_file, $key, $fr);
    }

    private function appendToFile($file, $key, $value)
    {
        $contents = file_get_contents($file);
        if($this->keyExists($contents, $key)){
            $this->error("Key already exists!");
            return;
        }
        $contents = substr($contents, 0, strpos($contents, "];"));
        $contents .= "\t" . '"' . $key . '" => "' . $value . '",' . PHP_EOL . "];";
        file_put_contents($file, $contents);
    }
    private function keyExists($content, $key){
        return strpos($content,"\t".'"'.$key.'"')>-1;
    }
    private function createFile($path)
    {
        if (file_exists($path)) {
            return;
        }
        $fp = fopen($path, "w+");
        $contents = "<?php " . PHP_EOL . "return [" . PHP_EOL . "];";
        fwrite($fp, $contents);
        fclose($fp);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            ['key', InputArgument::REQUIRED, 'key of the phrase.'],
            ['fr', InputArgument::REQUIRED, 'french translation of the phrase.'],
            ['en', InputArgument::OPTIONAL, 'english translation of the phrase.'],
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}
